/*******************************************************************************
 * Package: com.song.web
 * Type:    K8sWeb
 * Date:    2024-10-24 15:59
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能描述：
 *  http://localhost:10800/doc.html
 * @author Songxianyang
 * @date 2024-10-24 15:59
 */
@Api(tags = "k8s调试接口")
@RestController
@RequestMapping("k8s-web")
@Slf4j
public class K8sWeb {
    @GetMapping("str")
    @ApiOperation("获取String数据")
    public String getName(@RequestParam String name){
        log.info("接口获取到的参数是：：：：{}", name);
        return name;
    }
}
