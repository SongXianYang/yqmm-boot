package com.song.service;


import com.song.api.User;
import com.song.api.UserServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

@Service
public class UserClient {
    // yml里面指定访问服务端地址
    @GrpcClient("grpc-user")
    private UserServiceGrpc.UserServiceBlockingStub userServiceBlockingStub;



    public User.GetUserInfoResponse getUserInfo(String userId) {
        User.GetUserInfoRequest request = User.GetUserInfoRequest.newBuilder()
                .setUserId(userId)
                .build();
        return userServiceBlockingStub.getUserInfo(request);
    }
}
