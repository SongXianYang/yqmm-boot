package com.song;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * http://localhost:10001/order?userId=123
 * https://juejin.cn/post/7114126478459011085?searchId=2024090122483953037FDD0E259EBE06E8
 * https://blog.csdn.net/qq_31564573/article/details/139119789
 * https://blog.csdn.net/CYK_byte/article/details/135427954
 * https://www.opkfc.com/c/c18dddb6-c126-4409-9ca5-1c204b9aed79
 */
@SpringBootApplication
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }
}
