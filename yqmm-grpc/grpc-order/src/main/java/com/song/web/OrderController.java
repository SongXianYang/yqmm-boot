package com.song.web;

import com.song.api.User;
import com.song.service.UserClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    private final UserClient userClient;

    public OrderController(UserClient userClient) {
        this.userClient = userClient;
    }

    @GetMapping("/order")
    public String createOrder(@RequestParam String userId) {
        // 远程调用
        User.GetUserInfoResponse userInfo = userClient.getUserInfo(userId);
        // 处理订单逻辑
        return "为用户创建的订单：" + userInfo.getName()+userInfo.getAddress()+userInfo.getEmail();
    }
}
