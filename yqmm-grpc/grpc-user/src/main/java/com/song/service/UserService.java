package com.song.service;


import com.song.api.User;
import com.song.api.UserServiceGrpc;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;



/**
 * 自定义数据查询  提供给客户端数据
 */
@GrpcService
@Slf4j
public class UserService extends UserServiceGrpc.UserServiceImplBase{


    @Override
    public void getUserInfo(User.GetUserInfoRequest request,
                            StreamObserver<User.GetUserInfoResponse> responseObserver) {
        log.info("客户端发来的请求参数UserId>>>>>>>{}",request.getUserId());
        // todo 客户端发来的请求参数UserId 查询数据库db
        // todo 模拟db返回出来的用户数据
        User.GetUserInfoResponse response = User.GetUserInfoResponse.newBuilder()
                .setName("song-name")
                .setEmail("song.com")
                .setAddress("北京")
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
