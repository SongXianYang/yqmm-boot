package com.song.rocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
/**
 * 接口文档地址
 * http://localhost:12002/doc.html
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.song"})
public class ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

}
