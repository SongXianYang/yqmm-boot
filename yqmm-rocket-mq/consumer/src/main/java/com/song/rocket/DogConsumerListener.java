/*******************************************************************************
 * Package: com.song.rocket
 * Type:    UserConsumerListener
 * Date:    2024-03-27 14:06
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.rocket;

import com.song.common.constants.MQConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;


/**
 * 功能描述：获取DOG_TOPIC 里面的数据、并发消费消息
 * @author Songxianyang
 * @date 2024-03-27 14:06
 */
@RocketMQMessageListener(topic = MQConstant.DOG_TOPIC,consumerGroup = MQConstant.CONSUMER_GROUP_DOG)
@Component
@Slf4j
public class DogConsumerListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        log.info("接收到动物小狗的消息为：：：：：：{}", s);
    }
}
