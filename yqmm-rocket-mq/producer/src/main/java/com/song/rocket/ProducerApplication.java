package com.song.rocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 接口文档地址
 * http://localhost:12001/doc.html
 * https://blog.csdn.net/m0_63270436/article/details/132025694
 * https://blog.csdn.net/gzmyh/article/details/130222388
 * https://blog.csdn.net/weixin_45188218/article/details/135703873
 * https://www.bilibili.com/video/BV1tM4y1t7GE?p=8&vd_source=7edba4d7e4f510db6dfdcf1ff1081847
 */

@SpringBootApplication
@ComponentScan(basePackages = {"com.song"})
public class ProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }

}
