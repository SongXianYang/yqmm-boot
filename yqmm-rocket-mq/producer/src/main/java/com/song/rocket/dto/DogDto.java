package com.song.rocket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class DogDto implements Serializable {
    private String id;
    private String name;
}