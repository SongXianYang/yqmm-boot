报错1： Can't find datasource type!
> names: ds0,ds1 写正确

报错2：jdbcUrl is required with driverClassName.
> 将配置文件jdbc-url 改为 url
> 
报错3： Incorrect string value: '\xE6\xB5\x8B\xE8\xAF\x95...' for column 'name' at row 1
这种错误就是表的字符集与添加的字符集不匹配，要把表的字符集改一下，改为utf8。
> 针对字段修改字符集：
> 
>  ALTER TABLE `order_sharding_jdbc_0` CHANGE `name` `name` VARCHAR(100) CHARACTER SET utf8 NOT NULL;


```sql
修改
ALTER TABLE order_sharding_jdbc_0 CHARSET=utf8;
ALTER TABLE order_sharding_jdbc_1 CHARSET=utf8;
查看字符集
 show full columns from order_sharding_jdbc_0;
 针对字段修改字符集
 ALTER TABLE `order_sharding_jdbc_0` CHANGE `name` `name` VARCHAR(100) CHARACTER SET utf8 NOT NULL;
 ALTER TABLE `order_sharding_jdbc_1` CHANGE `name` `name` VARCHAR(100) CHARACTER SET utf8 NOT NULL;
```
# Sharding-JDBC 分片表简单的一套增删改查
###  _官网_：https://shardingsphere.apache.org/document/legacy/4.x/document/cn/manual/sharding-jdbc/
# maven
```java

```

```xml
<!--    Sharding-JDBC-->
    <!-- https://mvnrepository.com/artifact/org.apache.shardingsphere/sharding-jdbc-spring-boot-starter -->
    <dependency>
      <groupId>org.apache.shardingsphere</groupId>
      <artifactId>sharding-jdbc-spring-boot-starter</artifactId>
      <version>${sharding-sphere.version}</version>
    </dependency>
```
# 创建表：
```
CREATE TABLE `order_sharding_jdbc_1` (
  `order_id` int NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
分别在：mysql://127.0.0.1:3306/person表里面创建order_sharding_jdbc_1  order_sharding_jdbc_0
自己阿里云数据库：mysql://阿里云公网ip:3306/person表里面创建order_sharding_jdbc_1  order_sharding_jdbc_0

![img.png](img/img_3.png)
![img.png](img/img_4.png)

# yml配置：仔细阅读配置文件
```yaml
spring:
############################################################
#
# shardingsphere 配置
#
############################################################
  shardingsphere:
    datasource:
#      设置两个分片
      names: ds0,ds1
      ds0:
        type: com.zaxxer.hikari.HikariDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        jdbc-url: jdbc:mysql://127.0.0.1:3306/person-tmp?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai
        username: root
        password: root#123
      ds1:
        type: com.zaxxer.hikari.HikariDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
#      注意：  是jdbc-url 不是url 这是 规范
        jdbc-url: jdbc:mysql://127.0.0.1:3306/person?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai
        username: root
        password: root#123
    sharding:
      default-database-strategy:
        inline:
#    user_id来敲定库      举例  1除以2余数为1 来敲定数据落到那个库ds1：127.0.0.1:3306/person  依此类推
          sharding-column: user_id
          algorithm-expression: ds$->{user_id % 2}
      tables:
        order_sharding_jdbc:
#          举例：DELETE FROM 那个ds库（person）.order_sharding_jdbc_? WHERE order_id=?
          actual-data-nodes: ds$->{0..1}.order_sharding_jdbc_$->{0..1}
          table-strategy:
            inline:
              sharding-column: order_id
#     order_id来敲定表         来敲定表的逻辑  1除以2余数为1  依此类推
              algorithm-expression: order_sharding_jdbc_$->{order_id % 2}
```

# mapper.xml   表名选择在yml 里面配置的 tables:_order_sharding_jdbc_作为表名  注意
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE  mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.song.kkxxpoi.mapper.IOrderShardingJdbcMapper">
    <resultMap id="BaseResultMap" type="com.song.kkxxpoi.entity.OrderShardingJdbcDO">
        <result column="order_id" property="orderId" jdbcType="INTEGER"/>
        <result column="name" property="name" jdbcType="VARCHAR"/>
        <result column="user_id" property="userId" jdbcType="INTEGER"/>
    </resultMap>
    
<!--    其他省略-->
    
    <!-- 根据主键删除记录 -->
    <delete id="delete" parameterType="com.song.kkxxpoi.entity.OrderShardingJdbcDO">
        DELETE FROM order_sharding_jdbc WHERE order_id=#{orderId,jdbcType=INTEGER}
    </delete>
</mapper>
```
# web前端控制层一套增删改查
```java
/**
 * @author SongXianYang
 */
@Log4j
@Api(tags = "ShardingJdbc-分库分表")
@RestController
@RequestMapping("/orderShardingJdbc")
public class OrderShardingJdbcController {

    @Resource
    private IOrderShardingJdbcMapper orderShardingJdbcMapper;

    @GetMapping("insert")
    public void insert() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        for (Integer integer : list) {
            OrderShardingJdbcDO orderShardingJdbcDO = new OrderShardingJdbcDO();
            orderShardingJdbcDO.setOrderId(integer);
            orderShardingJdbcDO.setName("测试嘿嘿" + integer);
            orderShardingJdbcDO.setUserId(integer);
            orderShardingJdbcMapper.insert(orderShardingJdbcDO);
        }
    }

    @GetMapping("update")
    public void update() {
        OrderShardingJdbcDO orderShardingJdbcDO = new OrderShardingJdbcDO();
        orderShardingJdbcDO.setOrderId(1);
        orderShardingJdbcDO.setName("测试嘿嘿" + "查询");
        orderShardingJdbcDO.setUserId(88);
        orderShardingJdbcMapper.update(orderShardingJdbcDO);
    }

    @GetMapping("list")
    public List<OrderShardingJdbcDO> list(Integer page, Integer size) {
        return orderShardingJdbcMapper.listAll(page, size);
    }

    @GetMapping("delete")
    public void delete() {
        OrderShardingJdbcDO orderShardingJdbcDO = new OrderShardingJdbcDO();
        orderShardingJdbcDO.setOrderId(10);
        orderShardingJdbcMapper.delete(orderShardingJdbcDO);
    }
    
}
```

# 全局表（广播表配置）
# yml
![img.png](img/img_5.png)
# Controller
```java
 @GetMapping("/insert-complete/{subProjectName}")
    public String insert(@PathVariable("subProjectName") String subProjectName) {
        IntEndScaleForecastEntity entity = new IntEndScaleForecastEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setSubProjectName(subProjectName);
        iIntEndScaleForecastService.insert(entity);
        return "成功";
    }
```
# 测试
![img.png](img/img_6.png)
![img.png](img/img_7.png)
![img.png](img/img_8.png)

# 读写分离

```java
    /**
     * 读写分离(读）
     *
     * @param
     * @return
     */
    @ApiOperation("读写分离-分页查")
    @GetMapping("list-read-write")
    public IPage<IntEndScaleForecastEntity> listReadWrite(Long page, long size) {
        IPage<IntEndScaleForecastEntity> iPage = new Page<>(page, size);
        QueryWrapper<IntEndScaleForecastEntity> queryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<IntEndScaleForecastEntity> lambda = queryWrapper.lambda();
        return intEndScaleForecastMapper.selectPage(iPage, lambda);
    }

    /**
     * 读写分离(写）
     *
     * @param
     * @return
     */
    @ApiOperation("读写分离-写")
    @GetMapping("list-read-write-insert")
    public boolean listReadWriteInsert(String id) {
        IntEndScaleForecastEntity entity = new IntEndScaleForecastEntity();
        entity.setId(id);
        entity.setSubProjectName("项目" + id);
        intEndScaleForecastMapper.insert(entity);
        return true;
    }
```
# yml
```yaml
############################################################
#
# shardingsphere 读写分离配置
#  mdb 主
#  ,sdb0,sdb1 两从
#
############################################################
spring:
  shardingsphere:
    datasource:
#      设置两个分片
      names: mdb,sdb0,sdb1
      mdb:
        type: com.zaxxer.hikari.HikariDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        jdbc-url: jdbc:mysql://127.0.0.1:3306/person-mdb?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai
        username: root
        password: root#123
      sdb0:
        type: com.zaxxer.hikari.HikariDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        jdbc-url: jdbc:mysql://127.0.0.1:3306/person-tmp?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai
        username: root
        password: root#123
      sdb1:
        type: com.zaxxer.hikari.HikariDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
#      注意：  是jdbc-url 不是url 这是 规范
        jdbc-url: jdbc:mysql://127.0.0.1:3306/person?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai
        username: root
        password: root#123
    masterslave:
      # 配置slave节点的负载均衡均衡策略,采用轮询机制
      load-balance-algorithm-type: round_robin
      # 配置主库master,负责数据的写入
      master-data-source-name: mdb
      # 配置主从名称
      name: ms
      # 配置从库slave节点
      slave-data-source-names: sdb0,sdb1
    # 显示sql
    props:
      sql:
        show: true
    # 配置默认数据源ds1 默认数据源,主要用于写
    sharding:
      default-data-source-name: ds1
#监控功能Actuator：info
info:
  app:
    name: yqmm-sharding
    version: 1.1-SNAPSHOT
    description: 读写分离配置
```




