/*******************************************************************************
 * Package: com.song.kkxxpoi.mapper
 * Type:    IIntEndScaleForecastMapper
 * Date:    2022-03-24 14:44
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sharding.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.song.sharding.entity.IntEndScaleForecastEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-03-24 14:44
 */
@Mapper
public interface IIntEndScaleForecastMapper  extends BaseMapper<IntEndScaleForecastEntity> {

}
