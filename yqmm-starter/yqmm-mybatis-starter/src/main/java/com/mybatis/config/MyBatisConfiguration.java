/*******************************************************************************
 * Package: cn.surveyking.module.project.config
 * Type:    MyBatisPageConfiguration
 * Date:    2024-01-02 18:05
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.mybatis.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.mybatis.core.handler.ParentDOFieldHandler;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述： MyBaits 配置类
 *
 * @author Songxianyang
 * @date 2024-01-02 18:05
 */
@Configuration
// 扫描包写成活的 ，通过配置进行获取
@MapperScan(value = "${yqmm.mybatis.mapper-scan}",annotationClass = Mapper.class)
public class MyBatisConfiguration {

    /**
     * mybatisplus 分页拦截器
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor()); // 分页插件
        return mybatisPlusInterceptor;
    }

    @Bean
    public MetaObjectHandler parentDOFieldHandler() {
        return new ParentDOFieldHandler(); // 自动填充参数类
    }
}
