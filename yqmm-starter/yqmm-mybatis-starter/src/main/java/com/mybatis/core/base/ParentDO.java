/*******************************************************************************
 * Package: com.mybatis.core
 * Type:    ParentDO
 * Date:    2024-06-08 14:50
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.mybatis.core.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import org.apache.ibatis.type.JdbcType;

import java.util.Date;

/**
 * 功能描述： 通用参数
 *
 * @author Songxianyang
 * @date 2024-06-08 14:50
 */
@Data
public abstract class ParentDO {

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 最后更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 创建者，User  id 编号
     *
     */
    @TableField(fill = FieldFill.INSERT, jdbcType = JdbcType.VARCHAR)
    private String createUser;

    /**
     * 更新者，User id 编号
     *
     */
    @TableField(fill = FieldFill.INSERT_UPDATE, jdbcType = JdbcType.VARCHAR)
    private String updateUser;

    /**
     * TODO 是否删除  根据自己项目实际 来填写这个字段     （数据库设置deleted默认值为0）
     */
    @TableLogic
    private Boolean deleted;

}
