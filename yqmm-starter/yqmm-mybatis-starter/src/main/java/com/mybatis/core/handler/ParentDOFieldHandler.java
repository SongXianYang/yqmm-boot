package com.mybatis.core.handler;


import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.mybatis.core.base.ParentDO;
import org.apache.ibatis.reflection.MetaObject;

import java.util.Date;
import java.util.Objects;

/**
 * 功能描述： 对通用参数进行填充、赋值
 *
 * @author Songxianyang
 */
public class ParentDOFieldHandler implements MetaObjectHandler {

    public static final String LOGIN_USER_ID = "admin";

    @Override
    public void insertFill(MetaObject metaObject) {
        if (Objects.nonNull(metaObject) && metaObject.getOriginalObject() instanceof ParentDO) {
            ParentDO parentDO = (ParentDO) metaObject.getOriginalObject();

            Date current = new Date();
            // 创建时间为空，则以当前时间为插入时间
            if (Objects.isNull(parentDO.getCreateTime())) {
                parentDO.setCreateTime(current);
            }
            // 更新时间为空，则以当前时间为更新时间
            if (Objects.isNull(parentDO.getUpdateTime())) {
                parentDO.setUpdateTime(current);
            }
            // TODO 获取当前登录用户 (不同登录系统获取登录用户方式不一样）**注意**
            String userId = LOGIN_USER_ID;
            // 当前登录用户不为空，创建人为空，则当前登录用户为创建人
            if (Objects.nonNull(userId) && Objects.isNull(parentDO.getCreateUser())) {
                parentDO.setCreateUser(userId);
            }
            // 当前登录用户不为空，更新人为空，则当前登录用户为更新人
            if (Objects.nonNull(userId) && Objects.isNull(parentDO.getUpdateUser())) {
                parentDO.setUpdateUser(userId);
            }
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 更新时间为空，则以当前时间为更新时间
        Object modifyTime = getFieldValByName("updateTime", metaObject);
        if (Objects.isNull(modifyTime)) {
            setFieldValByName("updateTime", new Date(), metaObject);
        }

        // 当前登录用户不为空，更新人为空，则当前登录用户为更新人
        Object modifier = getFieldValByName("updateUser", metaObject);
        // TODO 获取当前登录用户 (不同登录系统获取登录用户方式不一样）**注意**
        String userId = LOGIN_USER_ID;
        if (Objects.nonNull(userId) && Objects.isNull(modifier)) {
            setFieldValByName("updateUser", userId, metaObject);
        }
    }

}
