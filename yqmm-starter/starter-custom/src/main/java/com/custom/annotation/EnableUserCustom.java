package com.custom.annotation;

import com.custom.config.UserAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;


/**
 * UserAutoConfiguration 把这个配置文件注入到这个EnableUserCustom
 * 如果使用EnableUserCustom这个注解将自动注入我们配置的bean （更灵活）
 * https://blog.csdn.net/weixin_54505261/article/details/132304910
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(UserAutoConfiguration.class)
// @Import(UserAutoConfiguration1.class)
// @Import(UserAutoConfiguration2.class)
public @interface EnableUserCustom {

}