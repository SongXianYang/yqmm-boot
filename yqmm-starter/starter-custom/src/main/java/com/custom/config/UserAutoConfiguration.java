/*******************************************************************************
 * Package: com.custom.config
 * Type:    UserConfig
 * Date:    2024-04-26 13:40
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.custom.config;

import com.custom.UserService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述： 自动配置类
 *
 * @author Songxianyang
 * @date 2024-04-26 13:40
 */
@Configuration
// yaml档中song.user.enabled 有没有  如果没有则无法自动配置改类
// ConditionalOnProperty中的属性 name  在UserProperties  没有enabled这个字段
// ConditionalOnProperty中的属性 value  在UserProperties  必须有enabled这个字段
@ConditionalOnProperty(prefix = "song.user",  name = "enabled", matchIfMissing = true)
// 把yml配置的属性注入进来 然后使用它。启动自动配置属性
@EnableConfigurationProperties(UserProperties.class)
public class UserAutoConfiguration {

    /*
    ConditionalOnMissingBean  如果系统存在userService  就使用系统存在的   如果系统没有 就自动配置当前userService的bean
     */
    @ConditionalOnMissingBean
    @Bean
    public UserService userService(UserProperties userProperties) {
        UserService userService = new UserService();
        userService.setId(userProperties.getId());
        userService.setName(userProperties.getName());
        userService.setAge(userProperties.getAge());
        return userService;
    }
}
