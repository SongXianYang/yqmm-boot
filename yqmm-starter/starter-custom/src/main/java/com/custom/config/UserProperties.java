/*******************************************************************************
 * Package: com.custom.config
 * Type:    UserProperties
 * Date:    2024-04-26 13:40
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.custom.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-04-26 13:40
 */
@ConfigurationProperties(prefix = "song.user")
public class UserProperties {
    private String name;
    private String age;
    private Long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
