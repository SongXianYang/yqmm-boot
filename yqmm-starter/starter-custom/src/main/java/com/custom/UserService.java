/*******************************************************************************
 * Package: com.custom
 * Type:    UserService
 * Date:    2024-04-26 11:49
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.custom;

import lombok.Data;

/**
 * 功能描述： 用户组件
 * 自动配置文档
 *  https://www.cnblogs.com/hjwublog/p/10335464.html
 *
 *  SpringBoot之自定义starter
 *  https://blog.csdn.net/qq_52445443/article/details/122479679
 * @author Songxianyang
 * @date 2024-04-26 11:49
 */
@Data
public class UserService {
    private String name;
    private String age;
    private Long id;
    public void pint() {
        System.out.println(String.format("获取的数据用户名：%s----密码：%s----id:%s", name, age, id));
    }
}
