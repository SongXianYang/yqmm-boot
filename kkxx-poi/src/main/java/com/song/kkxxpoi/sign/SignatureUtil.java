package com.song.kkxxpoi.sign;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Base64;


/**
 * 生成签名
 */
public class SignatureUtil {
    // 提供给前端对称加密密钥  （随便写）  保证前后端是同一把密钥
    public static final String SECRET_KEY="web—key-sxy-001";
    /**
     * 加密数据
     * @param data 数据
     * @return
     */
    public static String generateHmacSHA256(String data, String SECRET_KEY) {
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKey = new SecretKeySpec(SECRET_KEY.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            sha256_HMAC.init(secretKey);
            byte[] hash = sha256_HMAC.doFinal(data.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(hash);
        } catch (Exception e) {
            throw new RuntimeException("生成 HMAC SHA256 签名时出错", e);
        }
    }
}
