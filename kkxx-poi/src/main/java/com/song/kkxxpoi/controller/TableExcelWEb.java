/*******************************************************************************
 * Package: com.song.kkxxpoi.controller
 * Type:    TableExcelWEb
 * Date:    2024-04-19 16:04
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.controller;


import com.song.kkxxpoi.common.WordUtil;
import com.song.kkxxpoi.entity.TableExcel;
import com.song.kkxxpoi.mapper.TableExcelMapper;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能描述：  按模板导出功能
 *
 * @author Songxianyang
 * @date 2024-04-19 16:04
 */

@Api(tags = "excel")
@RestController
@RequestMapping("excel")
@Slf4j
public class TableExcelWEb {
    @Resource
    private WordUtil wordUtil;
    @Resource
    private TableExcelMapper tableExcelMapper;


    @GetMapping(value = "/exportWord")
    public String exportWord( HttpServletResponse response) throws IOException {


        List<TableExcel> tableExcels = tableExcelMapper.listAll();
        for (TableExcel tableExcel : tableExcels) {
            //存放数据，也就是填充在word里面的值
            Map<String, Object> params = new HashMap<>();

            //使用Map类型存储数据

            params.put("tableExcel", tableExcel);


            //模板路径  这里必须使用绝对路径，可以仿照我上面的模板，先练习
            String templatePath = "D:\\work\\project\\huatech\\yqmm-boot\\kkxx-poi\\src\\main\\resources\\templates\\new模板.docx";
            String t1 = tableExcel.getT1();
            String t3 = tableExcel.getT3();


            //生成文件名
            String fileName = t1+"_"+t3+"_"+ tableExcel.getT2() + ".docx";

            //保存路径
            String saveDir = "D:\\work\\project\\huatech\\yqmm-boot\\kkxx-poi\\src\\main\\resources\\templates";



            // 导出wold
            try {
                // 导出Word文档为文件
                String word = wordUtil.exportWord(templatePath, saveDir, fileName, params);
                // 将导出的Word文件转换为流
                File file = new File(word);
                InputStream inputStream = new FileInputStream(file);

                // 将流响应给前端回显
                //这里响应类型必须是二进制流（octet-stream），才可以让前端接收并解析
                response.setContentType("application/octet-stream");
                OutputStream outputStream = response.getOutputStream();
                byte[] buffer1 = new byte[1024];
                int len1;
                while ((len1 = inputStream.read(buffer1)) > 0) {
                    outputStream.write(buffer1, 0, len1);
                }
                outputStream.flush();
                outputStream.close();
                inputStream.close();
            } catch (Exception e) {
                System.out.println("导出Word文档时出现异常：" + e.getMessage());
            }
        }
        return "success";
    }


    @GetMapping("get")
    public TableExcel get(@RequestParam String t5) {
        return tableExcelMapper.getByt5(t5);
    }

}
