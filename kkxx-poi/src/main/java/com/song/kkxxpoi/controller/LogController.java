/*******************************************************************************
 * Package: com.song.kkxxpoi.controller
 * Type:    IndexController
 * Date:    2024-01-11 22:40
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.controller;

import com.custom.UserService;
import com.song.common.annotation.ResponseInfoSkin;
import com.song.common.log4j2.InputMDC;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 功能描述：
 *SpringBoot+Kafka+ELK 完成海量日志收集
 * or 自己封装的组件
 * https://blog.csdn.net/Qynwang/article/details/130684734
 * https://blog.csdn.net/z_ssyy/article/details/130366910
 * @author Songxianyang
 * @date 2024-01-11 22:40
 */
@Slf4j
@Api(tags = "log4j2-日志调试or自己封装的组件")
@RestController
@RequestMapping("/log4j2-web")
@ResponseInfoSkin
public class LogController {
    @Resource
    private UserService userService;

    @GetMapping(value = "/index")
    public String index() {
        InputMDC.putMDC();

        log.info("我是一条info日志");

        log.warn("我是一条warn日志");

        log.error("我是一条error日志");

        return "idx";
    }


    @GetMapping(value = "/err")
    public String err() {
        InputMDC.putMDC();
        try {
            int a = 1/0;
        } catch (Exception e) {
            log.error("算术异常", e);
        }
        return "err";
    }

    @GetMapping("get-starter-user")
    public String getStarter() {
        userService.pint();
        return "success";
    }
}
