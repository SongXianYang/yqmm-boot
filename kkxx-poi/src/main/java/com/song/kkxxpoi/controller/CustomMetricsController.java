/*******************************************************************************
 * Package: com.song.kkxxpoi.controller
 * Type:    CustomMetricsController
 * Date:    2024-08-13 10:00
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.controller;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 功能描述： prometheus：自定义指标
 * 功能需求：
 * 来监控接口请求次数
 * 实际项目项目中，使用AOP，或拦截器方式统计接口的请求信息次数更为优雅
 *
 * @author Songxianyang
 * @date 2024-08-13 10:00
 */
@Slf4j
@RestController
@RequestMapping("/custom-metrics")
@RequiredArgsConstructor
@Api(tags = "自定义指标")
public class CustomMetricsController {
    private final MeterRegistry meterRegistry;

    public static final String METRICS = "metrics.kkxx-poi.request.count";

    /**
     * 详情接口请求统计
     */
    @ApiOperation("详情接口")
    @GetMapping("/get/{id}")
    public String get(@PathVariable("id") String id) {
        Counter.builder(METRICS).tags("apiCode", "get").register(meterRegistry).increment();
        return "详情接口请求统计id：" + id;
    }

    /**
     * 列表接口请求统计
     */
    @ApiOperation("列表接口")
    @PostMapping("/list")
    public String list() {
        Counter.builder(METRICS).tags("apiCode", "list").register(meterRegistry).increment();
        return "列表接口请求统计";
    }
}
