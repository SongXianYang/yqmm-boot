/*******************************************************************************
 * Package: com.song.kkxxpoi.controller
 * Type:    Web
 * Date:    2022-05-02 16:08
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.controller;

import com.song.common.annotation.ResponseInfoSkin;
import com.song.kkxxpoi.entity.IntEndScaleForecastEntity;
import com.song.kkxxpoi.mapper.IIntEndScaleForecastMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-05-02 16:08
 */
@Api(tags = "vue对外暴露接口")
@RestController
@RequestMapping("/api/test")
@CrossOrigin
@Slf4j
@ResponseInfoSkin
public class VueWeb {
    @Resource
    private IIntEndScaleForecastMapper iIntEndScaleForecastMapper;


    @RequestMapping(value="/find", method={RequestMethod.GET}, produces = "application/json;charset=UTF-8")
    @ResponseBody
    @ApiOperation(httpMethod = "GET", value = "模拟测试对象查询")
    public List<IntEndScaleForecastEntity> find() {
        return iIntEndScaleForecastMapper.listAll();
    }


    @ApiOperation(httpMethod = "POST", value = "模拟测试对象保存")
    @PostMapping(value = "/insert", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Boolean insert(
            @RequestBody @ApiParam(name = "模拟测试对象", value = "json格式对象", required = true)IntEndScaleForecastEntity entity) {
        iIntEndScaleForecastMapper.insertIgnoreNull(entity);
        return Boolean.TRUE;
    }

    @ApiOperation(httpMethod = "POST", value = "模拟测试对象删除")
    @DeleteMapping(value = "/delete/{id}")
    @ResponseBody
    public Boolean delete(
            @PathVariable String id) {
        iIntEndScaleForecastMapper.delete(id);
        return  Boolean.TRUE;

    }


    @ApiOperation(httpMethod = "POST", value = "模拟测试对象更新")
    @PostMapping(value = "/update", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Boolean update(
            @RequestBody @ApiParam(name = "用户对象", value = "json格式对象", required = true)  IntEndScaleForecastEntity entity) {
        iIntEndScaleForecastMapper.update(entity);
        return Boolean.TRUE;
    }

    @GetMapping("/get")
    @ApiOperation("获取详情")
    public IntEndScaleForecastEntity get(@RequestParam String id) {
        return iIntEndScaleForecastMapper.getById(id);
    }
}
