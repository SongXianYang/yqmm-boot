package com.song.kkxxpoi.controller;

import com.song.common.annotation.ResponseInfoSkin;
import com.song.kkxxpoi.rsa.RSAUtil;
import com.song.kkxxpoi.sign.SignatureUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = "RSA非对称加解密")
@RestController
@RequestMapping("/rsa")
@ResponseInfoSkin
public class RSAController {

    /**
     * 加密请求
     *
     * @param data 具体数据
     * @return
     * @throws Exception
     */
    @GetMapping("/encrypt")
    @ApiOperation("加密请求")
    public String encrypt(@RequestParam String data) throws Exception {
        return RSAUtil.encrypt(data, RSAUtil.PUBLIC_KEY);
    }


    /**
     * 解密请求
     *
     * @param data 公钥加密后的明文
     * @return
     */
    @GetMapping("/decrypt")
    @ApiOperation("解密请求")
    public String decrypt(@RequestParam String data) throws Exception {
        return RSAUtil.decrypt(data, RSAUtil.PRIVATE_KEY);
    }


    /**
     * 接口签名
     *
     * @param signature 前端签名后的明文数据
     * @param amount    请求业务参数 金额
     * @param currency  请求业务参数 货币
     * @param timestamp 时间戳
     * @param nonce     随机数（保证请求接口唯一标识id）
     * @return
     */
    @ApiOperation("接口签名")
    @GetMapping("/signature")
    public String handlePayment(@RequestHeader("Authorization") String signature,
                                @RequestParam String amount,
                                @RequestParam String currency,
                                @RequestParam String timestamp,
                                @RequestParam String nonce) {

        // 拼接请求参数  /rsa/signature?amount=10&currency=USD&nonce=473897482&timestamp=20241113
        String data = "/rsa/signature?amount=" + amount + "&"+"currency=" + currency + "&"+"nonce=" + nonce +"&"+ "timestamp=" + timestamp;

        // 生成签名
        String calculatedSignature = SignatureUtil.generateHmacSHA256(data, SignatureUtil.SECRET_KEY);

        // 比较签名
        if (!calculatedSignature.equals(signature)) {
            return "签名无效！";
        }
        // 正常处理请求
        return "签名处理成功";
    }

    /**
     * 对请求接口路径签名
     *
     * @param path 接口路径
     * @return
     * @throws Exception
     */
    @GetMapping("/interface-path-signature")
    @ApiOperation("对请求接口路径签名")
    public String signThePathOfTheRequestInterface(@RequestParam String path) throws Exception {
        return SignatureUtil.generateHmacSHA256(path, SignatureUtil.SECRET_KEY);
    }

}
