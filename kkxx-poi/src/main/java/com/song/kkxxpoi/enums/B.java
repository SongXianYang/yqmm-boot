/*******************************************************************************
 * Package: com.song.kkxxpoi
 * Type:    B
 * Date:    2022-03-30 9:44
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.enums;

import com.song.kkxxpoi.enums.Roles;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-03-30 9:44
 */
public class B {
    public static void main(String[] args) {
        System.out.println(Roles.ADMIN.getRole());
        System.out.println(Roles.ADMIN.getName());
        System.out.println(Roles.getNameByRole(1));
    }
}
