/*******************************************************************************
 * Package: com.song.kkxxpoi.mapper
 * Type:    IIntEndScaleForecastMapper
 * Date:    2022-03-24 14:44
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.mapper;

import com.song.kkxxpoi.entity.IntEndScaleForecastEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-03-24 14:44
 */
@Mapper
public interface IIntEndScaleForecastMapper {
    /**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
    List<IntEndScaleForecastEntity> listAll();
    
    
    /**
     * 根据主键查询
     *
     * @param id 主键
     * @return 返回记录，没有返回null
     */
    IntEndScaleForecastEntity getById(String id);
    
    /**
     * 新增，插入所有字段
     *
     * @param intEndScaleForecast 新增的记录
     * @return 返回影响行数
     */
    int insert(IntEndScaleForecastEntity intEndScaleForecast);

    int batchSaveMainProject(@Param("list") List<IntEndScaleForecastEntity> list);
    
    /**
     * 新增，忽略null字段
     *
     * @param intEndScaleForecast 新增的记录
     * @return 返回影响行数
     */
    int insertIgnoreNull(IntEndScaleForecastEntity intEndScaleForecast);
    
    /**
     * 修改，修改所有字段
     *
     * @param intEndScaleForecast 修改的记录
     * @return 返回影响行数
     */
    int update(IntEndScaleForecastEntity intEndScaleForecast);
    
    /**
     * 修改，忽略null字段
     *
     * @param intEndScaleForecast 修改的记录
     * @return 返回影响行数
     */
    int updateIgnoreNull(IntEndScaleForecastEntity intEndScaleForecast);
    
    /**
     * 删除记录
     *
     * @param id 待删除的记录
     * @return 返回影响行数
     */
    int delete(@Param("id") String  id);
}
