package com.song.kkxxpoi.mapper;



import com.song.kkxxpoi.entity.TableExcel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author SongXianYang
 */
@Mapper
public interface TableExcelMapper {

	/**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
	List<TableExcel> listAll();


	/**
     * 根据主键查询
     *
     * @param id 主键
     * @return 返回记录，没有返回null
     */
	TableExcel getById(Integer id);
	TableExcel getByt5(String t5);

	
}