/*******************************************************************************
 * Package: com.song.kkxxpoi.interceptor
 * Type:    UserTokenInterceptor
 * Date:    2023-12-24 9:02
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.interceptor;

import cn.hutool.json.JSONUtil;
import com.song.common.response.ResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * 功能描述：  登录校验拦截器
 *
 * @author Songxianyang
 * @date 2023-12-24 9:02
 */
@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {
    /**
     *  拦截请求，在访问controller调用之前
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("该方法进入了登录校验拦截器...................");
        String userId = request.getHeader("userId");

        // todo 可以做登录校验、权限校验的拦截
        // 在这里我就简单写一个demo：判断等的用户是不是admin 如果不是就返回请登录

        if (!"admin".equals(userId)) {
            errorResponse(response, ResponseVO.error("用户不是管理员用户admin！请重新登录"));
            return false;
        }

        return true;
    }

    private void errorResponse(HttpServletResponse response, ResponseVO vo) {
        OutputStream out = null;
        try {
            response.setCharacterEncoding("utf-8");
            response.setContentType("text/json");
            out = response.getOutputStream();
            out.write(JSONUtil.toJsonStr(vo).getBytes(StandardCharsets.UTF_8));
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 请求访问controller之后，渲染视图之前
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    /**
     *
     *请求访问controller之后，渲染视图之后
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
