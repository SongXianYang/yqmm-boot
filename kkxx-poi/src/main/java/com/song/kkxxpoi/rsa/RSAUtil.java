package com.song.kkxxpoi.rsa;

import javax.crypto.Cipher;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * 功能描述：生成 RSA 密钥对
 * 你要给谁发信息 你就要用谁的公钥加密私钥解密
 *
 * @author Songxianyang
 */
public final class RSAUtil {

    public static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqZ7LDUAiM5GTR2JXEaEa+mNdVGjKW5RL9Om9yA5QL+Ym6q9LD3rHryz5B8O8rJsqaoNiQJQu/q/pcTGbjfkq2NiTTQSAJSXcLIToew95gilOs9SMzfiA/MrF5r1JULDbjQypZSs7YQd+yDVRu/tnevfKvOlRbM6R7g3zzsklUrk6jxfNp+Al6TqETFutmM+2fP/6TDkcY3Io7cjdUuGj9WLcYA2fNzwWrcnNOiLI+dzigZvGRHI+NFCnNyQIIwYGZjAQIyI/lzeatDNTHjzsHl+xN0E/Mdl+b5i4CNYIUhjriUgQFBcI4QZ9Ns3rZJ9sDozMA1jSvn3zXBQo6mdJ5wIDAQAB";
    public static final String PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCpnssNQCIzkZNHYlcRoRr6Y11UaMpblEv06b3IDlAv5ibqr0sPesevLPkHw7ysmypqg2JAlC7+r+lxMZuN+SrY2JNNBIAlJdwshOh7D3mCKU6z1IzN+ID8ysXmvUlQsNuNDKllKzthB37INVG7+2d698q86VFszpHuDfPOySVSuTqPF82n4CXpOoRMW62Yz7Z8//pMORxjcijtyN1S4aP1YtxgDZ83PBatyc06Isj53OKBm8ZEcj40UKc3JAgjBgZmMBAjIj+XN5q0M1MePOweX7E3QT8x2X5vmLgI1ghSGOuJSBAUFwjhBn02zetkn2wOjMwDWNK+ffNcFCjqZ0nnAgMBAAECggEAOmWMHMS+wahV31axCUXBBwFcqWnmeT8GI4yxpQWgf80qCtRM8EhiJSJDnnO6KmbFZKRVGEysHU0s/qaImol1QfDWTcbF44Q0WDNt4qhCOhjEms8LpwHuTpn1Jmp2qkwpWldInqcSq2YsNPt+Z0y3pLNY3k8plF9OCHpPPRstOVwyla5QrC/xqhbjV2x1NNGohdCcVKPZ9ooo/FamPt90GdwLtAcE2eqEgKES4VDCFBqtWCcJlbUnpNZDr8wFBkjUtu3EcRaa3eQVkA5L7U1xR/mYB5E2vyHHq8bnz5oWdD6pTTzsb7D37Wi8QNocaxTkh8nbFfizxED0U/Nm88w+YQKBgQDUQCA6tHy0GpiGjyxHdGgnExmu6DYKoHHLjYlLDJFmRIoCZ/sN4GNw/4oUavjfUV9x0LWEmAmLwLC9obozRYFvQFPIGoStnj5EP6o+UPgDQaNYHesSdNgMUUvwd5AtQZ9qtAvghRceMtgs/3d91uvEvy2wkexTzWKzcuFMJzGzMQKBgQDMlTBM0LsSS8uxt5rt58jKqDupA90k6MsoSEdB4dv6jYxIPXTe9UFMS44Guq1Lh8hALWrk9O+EiNHuWSh4G7UA/Hb64ecDcDiZEOhjTzX4ve7H77GqWJZl/pe2mXRZ3RNtqMpMWQiRE6Egn9SJJ/s0tSb1DNUe0aWqE5z1wWoYlwKBgBmdR4ZuI6KElyWd/6T76lnU10OhaGwt4q3C/e+dq6pg2RrjO4v4KWgInrxKEJC6vD77BfzjcDt6XJn1+4eWesFXTQUYFizxrOgFmuD8WoHiGPlYWvcWPMB3yydQ9ohPpBJ3eft1dE3Dqwz7I4t/NWLAA56Mba3LWn73fTL4g+nxAoGBAKs7L1sL1ZfhstLDjzwSpT/iAVVcc4sCAZBstp+Q8DLMqJ6KObinZAj8sBo3wvYV5ui4WuDZlWPDtJofK70jy6LQrLEdxOJe2jBbM3ZzhQP1zNaxa23dibxClC70T6srW6iL/w5Z8s6itFj/Db/io+IgwUOXgwWxiUxJ8DTNtwRvAoGAFw0dPwOAM4TdYkFcNs2u7pBj9C328IadhmV1dSTaTTgzFBaBNHasaWOyGQXZjBFOuNhKKkMDsmBFk8wvIPdLs6PlBDXwLFntZCPeCIgUtpNKfMyAOfvR8ZjgLjnfWRy/9yxdf9oiuhr5kVaCzo7Li9SQjkMaoFlXomPZ4Kaf24w=";

    /**
     * 使用公钥加密
     *
     * @param plainText 需要加密的数据
     * @param publicStr 公钥
     * @return 明文
     * @throws Exception
     */
    public static String encrypt(String plainText, String publicStr) throws Exception {
        PublicKey publicKey = getPublicKey(publicStr);
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encryptedBytes = cipher.doFinal(plainText.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    /**
     * 使用私钥解密
     *
     * @param encryptedText 公钥加密后的明文
     * @param privateStr    私钥
     * @return 解密后的具体数据
     * @throws Exception
     */
    public static String decrypt(String encryptedText, String privateStr) throws Exception {
        PrivateKey privateKey = getPrivateKey(privateStr);
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedText));
        return new String(decryptedBytes);
    }


    // 从 Base64 字符串获取 PublicKey
    public static PublicKey getPublicKey(String key) throws Exception {
        byte[] byteKey = Base64.getDecoder().decode(key);
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(byteKey));
    }

    // 从 Base64 字符串获取 PrivateKey
    public static PrivateKey getPrivateKey(String key) throws Exception {
        byte[] byteKey = Base64.getDecoder().decode(key);
        return KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(byteKey));
    }
}
