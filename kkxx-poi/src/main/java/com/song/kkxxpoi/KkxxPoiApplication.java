package com.song.kkxxpoi;

import com.custom.annotation.EnableUserCustom;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * -javaagent:D:/work/DevelopUtils/skywalking/skywalking-agent/skywalking-agent.jar
 * -Dserver.port=9092
 * -Dskywalking.agent.service_name=KkxxPoiApplication-9092
 * -Dskywalking.agent.sample_n_per_3_secs=2
 * -Dskywalking.plugin.jdbc.trace_sql_parameters=true
 * -Dskywalking.plugin.jdbc.sql_parameters_max_length=100
 */
@SpringBootApplication
@MapperScan("com.song.kkxxpoi.mapper")
@ComponentScan(basePackages = {"com.song.common","com.song.kkxxpoi"})
@EnableScheduling
// 开启自定义组件
@EnableUserCustom
public class KkxxPoiApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(KkxxPoiApplication.class, args);
    }
    
}
