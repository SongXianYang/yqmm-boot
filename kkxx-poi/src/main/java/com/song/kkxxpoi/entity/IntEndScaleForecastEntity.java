/*******************************************************************************
 * Package: com.song.kkxxpoi.entity
 * Type:    A
 * Date:    2022-03-24 14:37
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.entity;

import com.song.kkxxpoi.common.CreateTime;
import com.song.kkxxpoi.common.CreateUser;
import com.song.kkxxpoi.common.UpdateTime;
import com.song.kkxxpoi.common.UpdateUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-03-24 14:37
 */
@ApiModel("测试着玩的")
@Data
public class IntEndScaleForecastEntity {
    /**
     * 主键id
     */
    @ApiModelProperty("id")
    private String id;
    /**
     * 子项目编号
     */
    @ApiModelProperty("code")
    private String subProjectCode;
    /**
     * 子项目名称
     */
    private String subProjectName;
    /**
     * 开始时间
     */
    private Date startDate;
    /**
     * 金额
     */
    private BigDecimal money;
    /**
     * 结束时间
     */
    private Date endDate;
    /**
     * 创建人
     */
    @CreateUser
    private String createBy;
    /**
     * 创建时间
     */
    @CreateTime
    private Date createTime;
    /**
     * 更新人
     */
    @UpdateUser
    private String updateBy;
    /**
     * 更新时间
     */
    @UpdateTime
    private Date updateTime;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 乐观锁
     */
    private Integer updateCount;
}
