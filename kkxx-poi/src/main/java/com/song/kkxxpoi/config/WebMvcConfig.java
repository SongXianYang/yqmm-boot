/*******************************************************************************
 * Package: com.song.kkxxpoi.config
 * Type:    WebMvcConfig
 * Date:    2023-12-24 9:47
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.config;

import com.song.kkxxpoi.interceptor.LoginInterceptor;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 功能描述：配置web拦截器
 *
 * @author Songxianyang
 * @date 2023-12-24 9:47
 */
@SpringBootConfiguration
public class WebMvcConfig implements WebMvcConfigurer {

    @Bean
    public LoginInterceptor getLoginInterceptor() {
        return new LoginInterceptor();
    }

    /**
     * 注册拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(getLoginInterceptor())
                // 你要拦截的路径
                .addPathPatterns("/my/yes-Interceptors")
                // 可以写多个
                //.addPathPatterns("/Interceptors/add")

                // 不拦截的路径
                .excludePathPatterns("/my/no-Interceptors");
                //.excludePathPatterns("/test/test---1");

        WebMvcConfigurer.super.addInterceptors(registry);
    }
}
