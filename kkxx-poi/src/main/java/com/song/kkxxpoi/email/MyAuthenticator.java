/*******************************************************************************
 * Package: com.DocSystem.email
 * Type:    MyAuthenticator
 * Date:    2023-10-07 18:30
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.email;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-10-07 18:30
 */
public class MyAuthenticator extends Authenticator{
    String userName = null;

    String password = null;

    public MyAuthenticator() {
    }

    public MyAuthenticator(String username, String password) {
        this.userName = username;
        this.password = password;
    }

    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(userName, password);
    }
}
