// /*******************************************************************************
//  * Package: com.song.kkxxpoi.service
//  * Type:    WordToPDF
//  * Date:    2023-08-19 14:57
//  *
//  * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
//  *
//  * You may not use this file except in compliance with the License.
//  *******************************************************************************/
// package com.song.kkxxpoi.word;
//
//
// import com.jacob.activeX.ActiveXComponent;
// import com.jacob.com.Dispatch;
//
//
// import java.io.*;
// import java.nio.file.Files;
// import java.nio.file.Path;
// import java.nio.file.Paths;
// import java.time.Duration;
// import java.time.Instant;
// import java.util.List;
// import java.util.stream.Collectors;
//
// /**
//  * 功能描述：
//  *
//  * @author Songxianyang
//  * @date 2023-08-19 14:57
//  */
// public class WordToPDF {
//
//     public static final String DOCX =".docx";
//     public static final String DOC=".doc";
//
//     public static final String extPdf=".pdf";
//     public static final String name="D:\\下载\\定稿制度\\合规及反洗钱类\\";
//     public static final String fileName="4、华创证券有限责任公司上海分公司合规工作档案管理指引（2023年8月修订）";
//
//     public static void main(String[] args) throws IOException {
//         //ActiveXComponent activeXComponent = null;
//         //try {
//         //    activeXComponent = new ActiveXComponent("Word.Application");
//         //    activeXComponent.setProperty("Visible", false);
//         //    Dispatch docs = activeXComponent.getProperty("Documents").toDispatch();
//         //    Dispatch doc = Dispatch.call(docs, "Open", name + fileName + DOC).toDispatch();
//         //    Dispatch.call(doc, "SaveAs", name + fileName + extPdf,17);
//         //    Dispatch.call(doc, "Close");
//         //} catch (Exception e) {
//         //    e.printStackTrace();
//         //}finally {
//         //    activeXComponent.invoke("Quit",0);
//         //}
//         testWordToPDF();
//     }
//
//     public static void testWordToPDF() throws IOException {
//         List<Path> collect = Files.walk(Paths.get("D:\\下载\\定稿制度")).filter(Files::isRegularFile).collect(Collectors.toList());
//         System.out.println("--------------------------------------------------");
//         Instant time1 = Instant.now();
//
//         for (Path path : collect) {
//             Path parent = path.getParent();// 父级路径:D:\下载\定稿制度\信息技术管理类
//             File file = new File(path.toUri());
//             String fileName = file.getName();
//             String suffixLast = fileName.substring(fileName.lastIndexOf("."));// .doc
//
//             String suffixFirst = fileName.substring(0, fileName.lastIndexOf('.'));//华创证券有限责任公司金融产品代销业务应急处理措施
//             suffixFirst = "/" + suffixFirst;
//
//             if (suffixLast.equals(DOCX)||suffixLast.equals(DOC)) {
//
//                 ActiveXComponent activeXComponent = null;
//                 try {
//                     activeXComponent = new ActiveXComponent("Word.Application");
//                     activeXComponent.setProperty("Visible", false);
//                     Dispatch docs = activeXComponent.getProperty("Documents").toDispatch();
//                     Dispatch doc = Dispatch.call(docs, "Open", path.toString()).toDispatch();
//                     Dispatch.call(doc, "SaveAs", parent+suffixFirst+extPdf,17);
//                     Dispatch.call(doc, "Close");
//                 } catch (Exception e) {
//                     e.printStackTrace();
//                 }finally {
//                     activeXComponent.invoke("Quit",0);
//                 }
//             }
//         }
//
//         Instant time2 = Instant.now();
//         System.out.println(">>>查询data耗时:"+Duration.between(time1,time2).toMillis());
//     }
// }
