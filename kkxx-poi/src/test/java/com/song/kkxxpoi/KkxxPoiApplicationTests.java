//package com.song.kkxxpoi;
//
//import com.song.kkxxpoi.service.TransactionalEnd;
//import org.junit.jupiter.api.Test;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.transaction.support.TransactionTemplate;
//
//import javax.annotation.Resource;
//
//@SpringBootTest
//class KkxxPoiApplicationTests {
//    @Resource
//    private TransactionTemplate transactionTemplate;
//
//    @Resource
//    private TransactionalEnd transactionalEnd;
//
//    @Test
//    void transactional1() {
//        transactionalEnd.test1();
//    }
//
//    @Test
//    void setTransactionTemplate() {
//        // 查用户
//        // 查积分
//        // 发短信
//        // 发邮件
//
//        transactionTemplate.execute(status -> {
//            // 用户新增
//            transactionalEnd.test2();
//            // 角色新增
//            transactionalEnd.test3();
//            return true;
//        });
//    }
//}
