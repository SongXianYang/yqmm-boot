/*******************************************************************************
 * Package: com.song.visit
 * Type:    UserVisitDto
 * Date:    2024-11-28 11:15
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.visit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-11-28 11:15
 */
@Data
@ToString
@AllArgsConstructor
public class UserVisitDto implements Serializable ,Comparable<UserVisitDto>{
    private static final long serialVersionUID = -3325020540705384997L;
    // 商品id
    // productId
    private String productId;
    // 点击数
    // clicks
    private Long clicks;
    // 下单数
    // numberOrders
    private Long numberOrders;
    // 支付数
    // numberPayments
    private Long numberPayments;


    @Override
    public int compareTo(UserVisitDto other) {


        if ( this.clicks > other.clicks ) {
            return -1;
        } else if ( this.clicks < other.clicks ) {
            return 1;
        } else {
            if ( this.numberOrders > other.numberOrders ) {
                return -1;
            } else if ( this.numberOrders < other.numberOrders ) {
                return 1;
            } else {
                return (int)(other.numberPayments - this.numberPayments);
            }
        }
    }
}

