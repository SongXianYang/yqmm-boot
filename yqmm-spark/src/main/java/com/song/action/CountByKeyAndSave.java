/*******************************************************************************
 * Package: com.song.main
 * Type:    TestMap
 * Date:    2024-11-21 19:00
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述： countByKey()统计每种key的个数
 * save相关算子
 *
 * @author Songxianyang
 * @date 2024-11-21 19:00
 */
public class CountByKeyAndSave {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("sparkCore");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);
        List<Tuple2<String, String>> list = new ArrayList<>();

        list.add(new Tuple2<>("k1", "v1"));
        list.add(new Tuple2<>("k1", "v2"));
        list.add(new Tuple2<>("k3", "v3"));
        list.add(new Tuple2<>("k3", "v4"));

        JavaPairRDD<String, String> pairRDD = sc.parallelizePairs(list);
        //countByKey()统计每种key的个数
        System.out.println(pairRDD.countByKey());
        //saveAsTextFile(path)保存成Text文件
        pairRDD.saveAsTextFile("yqmm-spark/file/output");
        sc.close();
    }
}
