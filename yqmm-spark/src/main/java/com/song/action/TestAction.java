/*******************************************************************************
 * Package: com.song.action
 * Type:    TestAction
 * Date:    2024-11-22 17:04
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.action;

import com.google.common.collect.Lists;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.List;

/**
 * 功能描述： 行动算子
 *
 * @author Songxianyang
 * @date 2024-11-22 17:04
 */
public class TestAction {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf().setMaster("local").setAppName("sparkCore");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        List<Integer> data = Lists.newArrayList(1, 2, 3, 4, 5, 6, 7);
        // 获取内存数据
        JavaRDD<Integer> map = sc.parallelize(data);

        // RDD中元素个数
        System.out.println("------------------count-----------");
        System.out.println(map.count());
        //返回RDD中的第一个元素
        System.out.println("------------------first-----------");
        System.out.println(map.first());
        // 返回由RDD前n个元素组成的数组
        System.out.println("------------------take-----------");
        map.take(3).forEach(System.out::println);
        // 收集数据
        System.out.println("------------------collect----forEach-------");
        map.collect().forEach(System.out::println);
        sc.close();
    }
}
