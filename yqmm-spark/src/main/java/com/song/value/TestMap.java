/*******************************************************************************
 * Package: com.song.main
 * Type:    TestMap
 * Date:    2024-11-21 19:00
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.value;

import com.google.common.collect.Lists;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.List;

/**
 * 功能描述： 新RDD中的每一个元素都是原来RDD中每一个元素依次应用f函数而得到的
 *
 * @author Songxianyang
 * @date 2024-11-21 19:00
 */
public class TestMap {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("sparkCore");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        List<Integer> data = Lists.newArrayList(1, 2, 3, 4);
        // 获取内存数据
        JavaRDD<Integer> mapRdd = sc.parallelize(data);
        JavaRDD<Integer> map = mapRdd.map(value -> value * 2);
        // 收集数据
        map.collect().forEach(System.out::println);
        sc.close();
    }
}
