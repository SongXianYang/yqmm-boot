/*******************************************************************************
 * Package: com.song.main
 * Type:    GroupBy
 * Date:    2024-11-21 20:03
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.value;

import com.google.common.collect.Lists;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.List;

/**
 * 功能描述：在排序之前，可以将数据通过f函数进行处理，之后按照f函数处理的结果进行排序，默认为正序排列
 *
 * @author Songxianyang
 * @date 2024-11-21 20:03
 */
public class SortBy {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("sparkCore");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        List<Integer> data = Lists.newArrayList(1, 2, 8, 3, 7, 4);
        // 获取内存数据
        JavaRDD<Integer> rdd = sc.parallelize(data);
        rdd.sortBy(s->s,false,2).collect().forEach(System.out::println);
        sc.close();
    }
}
