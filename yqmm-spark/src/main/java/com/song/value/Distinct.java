/*******************************************************************************
 * Package: com.song.main
 * Type:    GroupBy
 * Date:    2024-11-21 20:03
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.value;

import com.google.common.collect.Lists;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.List;

/**
 * 功能描述：
 *对内部的元素去重，并将去重后的元素放到新的RDD中
 * @author Songxianyang
 * @date 2024-11-21 20:03
 */
public class Distinct {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("sparkCore");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        List<Integer> data = Lists.newArrayList(1, 2, 3, 4,2,5);
        // 获取内存数据
        JavaRDD<Integer> rdd = sc.parallelize(data);
        rdd.distinct().collect().forEach(System.out::println);
        sc.close();
    }
}
