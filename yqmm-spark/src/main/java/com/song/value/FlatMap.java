/*******************************************************************************
 * Package: com.song.main
 * Type:    FlatMap
 * Date:    2024-11-21 19:09
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;

import java.util.Arrays;
import java.util.Iterator;

/**
 * 功能描述： 扁平化  把数据拆平
 *
 * @author Songxianyang
 * @date 2024-11-21 19:09
 */
public class FlatMap {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("sparkCore");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);
        // 获取内存数据
        JavaRDD<String> stringJavaRDD = sc.textFile("yqmm-spark/file/file.txt", 2);

        // 扁平
        JavaRDD<String> flatMap = stringJavaRDD.flatMap(new FlatMapFunction<String, String>() {
            private static final long serialVersionUID = -8518025948072874182L;

            @Override
            public Iterator<String> call(String s) throws Exception {
                String[] split = s.split(" ");
                return Arrays.stream(split).iterator();
            }
        });
        // 映射
        flatMap.map(value-> value+"sssss").collect().forEach(s -> System.out.println(s));
        // 收集数据
        // flatMap.collect().forEach(System.out::println);
        sc.close();
    }
}
