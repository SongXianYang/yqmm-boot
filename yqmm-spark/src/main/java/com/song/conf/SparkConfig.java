/*******************************************************************************
 * Package: com.song.conf
 * Type:    SparkConfig
 * Date:    2024-11-04 16:04
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.conf;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述： 配置文件
 *
 * @author Songxianyang
 * @date 2024-11-04 16:04
 */
@Configuration
public class SparkConfig {
    public static final String APP_NAME = "Spring-Boot-Spark";
    public static final String MASTER = "local[*]";


    @Bean
    public SparkConf sparkConf() {
        SparkConf conf = new SparkConf().setAppName(APP_NAME).setMaster(MASTER);
        return conf;
    }



    @Bean
    public SparkSession sparkSession(SparkConf conf) {
        return SparkSession.builder().config(conf).getOrCreate();
    }

    @Bean
    public JavaSparkContext javaSparkContext(SparkConf conf) {
        return new JavaSparkContext(conf);
    }

}
