/*******************************************************************************
 * Package: com.song.keyvalue
 * Type:    MapValue
 * Date:    2024-11-22 16:11
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.keyvalue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述： 针对于(K,V)形式的类型只对 V 进行操作
 *
 * @author Songxianyang
 * @date 2024-11-22 16:11
 */
public class MapValue {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("sparkCore");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);
        List<Tuple2<String,String>> list = new ArrayList<>();

        list.add(new Tuple2<>("k1","v1"));
        list.add(new Tuple2<>("k2","v2"));
        list.add(new Tuple2<>("k3","v3"));
        list.add(new Tuple2<>("k4","v4"));

        JavaPairRDD<String, String> pairRDD = sc.parallelizePairs(list);
        pairRDD.mapValues( value -> value+"song").collect().forEach(System.out::println);
        sc.close();
    }
}
