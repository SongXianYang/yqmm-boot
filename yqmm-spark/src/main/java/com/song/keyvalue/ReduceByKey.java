/*******************************************************************************
 * Package: com.song.keyvalue
 * Type:    MapValue
 * Date:    2024-11-22 16:11
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.keyvalue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述： 功能说明：该操作可以将RDD[K,V]中的元素按照相同的K对V进行聚合
 *
 * @author Songxianyang
 * @date 2024-11-22 16:11
 */
public class ReduceByKey {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("sparkCore");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);
        List<Tuple2<String,Integer>> list = new ArrayList<>();

        list.add(new Tuple2<>("k1",1));
        list.add(new Tuple2<>("k1",1));
        list.add(new Tuple2<>("k3",1));
        list.add(new Tuple2<>("k3",1));
        list.add(new Tuple2<>("k4",7));
        list.add(new Tuple2<>("k3",9));

        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(list);
        pairRDD.reduceByKey(new Function2<Integer, Integer, Integer>() {
            private static final long serialVersionUID = 7962788610923032376L;

            @Override
            public Integer call(Integer v1, Integer v2) throws Exception {
                return v1+v2;
            }
        }).collect().forEach(System.out::println);
        sc.close();
    }
}
