/*******************************************************************************
 * Package: com.song.keyvalue
 * Type:    MapValue
 * Date:    2024-11-22 16:11
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.keyvalue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述： groupByKey对每个key进行操作，但只生成一个seq，并不进行聚合。
 * 该操作可以指定分区器或者分区数
 *
 * @author Songxianyang
 * @date 2024-11-22 16:11
 */
public class GroupByKey {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("sparkCore");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);
        List<Tuple2<String,Integer>> list = new ArrayList<>();

        list.add(new Tuple2<>("k1",1));
        list.add(new Tuple2<>("k1",1));
        list.add(new Tuple2<>("k3",1));
        list.add(new Tuple2<>("k3",1));
        list.add(new Tuple2<>("k4",7));
        list.add(new Tuple2<>("k4",9));

        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(list);
        pairRDD.groupByKey().collect().forEach(System.out::println);
        sc.close();
    }
}
