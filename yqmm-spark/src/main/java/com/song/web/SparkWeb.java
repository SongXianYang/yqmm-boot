/*******************************************************************************
 * Package: com.song.web
 * Type:    SparkWeb
 * Date:    2024-11-04 16:14
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.web;

import com.song.service.WordCountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-11-04 16:14
 */


@Api(tags = "Spark接口实战详情")
@RestController
@RequestMapping("spark-web")
@RequiredArgsConstructor
public class SparkWeb {

    private final WordCountService wordCountService;

    @GetMapping("/word—count")
    @ApiOperation("批处理word—count小程序")
    public Map<Object, Long> getWordCount() {
        return wordCountService.countWords();
    }

}
