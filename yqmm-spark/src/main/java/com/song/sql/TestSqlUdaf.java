/*******************************************************************************
 * Package: com.song.sql
 * Type:    TestSql001
 * Date:    2024-12-02 10:59
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.expressions.UserDefinedFunction;

import static org.apache.spark.sql.functions.udaf;
/**
 * 功能描述： 获取一列
 * 求平均值
 * 输入多行，返回一行。通常和groupBy一起使用，如果直接使用UDAF函数，默认将所有的数据合并在一起
 *
 * @author Songxianyang
 * @date 2024-12-02 10:59
 */
public class TestSqlUdaf {

    public static void main(String[] args) {
        // 创建配置对象
        SparkSession session = SparkSession.builder()
                .appName("SparkSql")
                .master("local[1]")
                .getOrCreate();

        // 获取文件数据
        Dataset<Row> dataset = session.read().json("D:/work/project/huatech/yqmm-boot/yqmm-spark/file/user.json").repartition(2);


        // 转换对象dto
        // orReplace表示覆盖之前相同名称的视图
        dataset.createOrReplaceTempView("user1");

        // UserDefinedFunction myAvg = ;

        // 把自己的utf 注册进来
        session.udf().register("myAvg",udaf(new MyAvg(), Encoders.LONG()));


        String sql = "select myAvg(age) newAge  from user1";
        // String sql = "select avg(age) newage  from user1";
        // 通过sql文得方式来查
        Dataset<Row> rowDataset = session.sql(sql);
        rowDataset.show();


        session.close();
    }
}
