/*******************************************************************************
 * Package: com.song.sql
 * Type:    UserDto
 * Date:    2024-12-02 11:00
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-12-02 11:00
 */
@Data
@AllArgsConstructor
public class UserDto implements Serializable {
    private static final long serialVersionUID = -482564324206229081L;
    private Long age;
    private String name;
}
