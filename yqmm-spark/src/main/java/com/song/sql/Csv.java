/*******************************************************************************
 * Package: com.song.sql
 * Type:    Csv
 * Date:    2024-12-16 10:23
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

/**
 * 功能描述：
 *
 * 读取的文件模式可以是csv、json、mysql、Hive、Parquet
 * 写入的文件模式也可以是csv、json、mysql、Hive、Parquet
 * 任意两者都可以
 * @author Songxianyang
 * @date 2024-12-16 10:23
 */
public class Csv {
    public static void main(String[] args) {
        //
        SparkSession session = SparkSession.builder()
                .appName("SparkSql")
                .master("local[1]")
                .getOrCreate();
        // 读取
        Dataset<Row> csv =
                session.read()
                        .option("header", "true") // 配置 id,name,age 表头
                        .csv("yqmm-spark/file/userinfo.csv");
        // 写入
        //  如果输出目的地已经存在，那么SparkSQL默认会发生错误，如果不希望发生错误，那么就需要修改配置：保存模式   append : 追加   重写Overwrite
        csv.write()
                .mode(SaveMode.Overwrite)
                .option("header", "true") // 配置
                .csv("yqmm-spark/file/output");

        csv.show();

        session.close();

    }
}
