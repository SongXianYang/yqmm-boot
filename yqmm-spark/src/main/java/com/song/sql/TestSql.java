/*******************************************************************************
 * Package: com.song.sql
 * Type:    TestSql001
 * Date:    2024-12-02 10:59
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-12-02 10:59
 */
public class TestSql {

    public static void main(String[] args) {
        // 创建配置对象
        SparkSession session = SparkSession.builder()
                .appName("SparkSql")
                .master("local[*]")
                .getOrCreate();
        // 获取文件数据
        Dataset<Row> dataset = session.read().json("D:/work/project/huatech/yqmm-boot/yqmm-spark/file/user.json").repartition(2);
        System.out.println(dataset.count());
        // 转换对象dto
        // Dataset<UserDto> userDtoDataset = dataset.as(Encoders.bean(UserDto.class));
        // orReplace表示覆盖之前相同名称的视图
        dataset.createOrReplaceTempView("user1");
        String sql = "select * from user1";
        // 通过sql文得方式来查
        Dataset<Row> rowDataset = session.sql(sql);
        rowDataset.show();


        session.close();
    }
}
