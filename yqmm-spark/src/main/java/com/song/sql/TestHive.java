/*******************************************************************************
 * Package: com.song.sql
 * Type:    TestSql001
 * Date:    2024-12-02 10:59
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import org.apache.spark.sql.SparkSession;

/**
 * 功能描述：
 * hive
 * @author Songxianyang
 * @date 2024-12-02 10:59
 */
public class TestHive {

    public static void main(String[] args) {

        //设定Hadoop的访问用户
        System.setProperty("HADOOP_USER_NAME","hivename");

        // 创建配置对象
        SparkSession session = SparkSession.builder()
                .appName("SparkSql")
                .enableHiveSupport() // 启用Hive的支持
                .master("local[*]")
                .getOrCreate();

        session.sql("use spark_user_visit_action");
        session.sql("show tables").show();


        session.close();
    }
}
