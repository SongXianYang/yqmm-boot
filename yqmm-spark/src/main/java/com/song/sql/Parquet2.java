/*******************************************************************************
 * Package: com.song.sql
 * Type:    Parquet
 * Date:    2024-12-16 10:48
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

/**
 * 功能描述：
 *列式存储的数据自带列分割
 * @author Songxianyang
 * @date 2024-12-16 10:48
 */
public class Parquet2 {
    public static void main(String[] args) {
        //
        SparkSession session = SparkSession.builder()
                .appName("SparkSql")
                .master("local[1]")
                .getOrCreate();
        // 读取
        Dataset<Row> csv =
                session.read()
                        .option("header", "true") // 配置 id,name,age 表头
                        .csv("yqmm-spark/file/userinfo.csv");
        // 写入
        //  如果输出目的地已经存在，那么SparkSQL默认会发生错误，如果不希望发生错误，那么就需要修改配置：保存模式   append : 追加   重写Overwrite
        csv.write()
                .mode(SaveMode.Overwrite)
                .option("header", "true") // 配置
                .parquet("yqmm-spark/file/output");


        csv.show();
        session.close();
    }
}
