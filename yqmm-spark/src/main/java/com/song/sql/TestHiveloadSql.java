/*******************************************************************************
 * Package: com.song.sql
 * Type:    TestSql001
 * Date:    2024-12-02 10:59
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import org.apache.spark.sql.SparkSession;

/**
 * 功能描述：
 * 把文件里面的数据加载到 spark_user_visit_action 对应的表里面
 *
 * @author Songxianyang
 * @date 2024-12-02 10:59
 */
public class TestHiveloadSql {

    public static void main(String[] args) {

        //设定Hadoop的访问用户
        System.setProperty("HADOOP_USER_NAME", "SongXianYang");

        // 创建配置对象
        SparkSession session = SparkSession.builder()
                .appName("SparkSql")
                .enableHiveSupport() // 启用Hive的支持
                .master("local[*]")
                .getOrCreate();

        session.sql("use spark_user_visit_action");
        // session.sql("show tables").show();
        session.sql("load data local inpath 'yqmm-spark/data/user_visit_action.txt' into table user_visit_action;");
        session.sql("load data local inpath 'yqmm-spark/data/city_info.txt' into table city_info;");
        session.sql("load data local inpath 'yqmm-spark/data/product_info.txt' into table product_info;");

        session.sql("select * from user_visit_action limit 5").show();
        session.sql("select * from city_info limit 5").show();
        session.sql("select * from product_info limit 5").show();
        session.close();
    }
}
