/*******************************************************************************
 * Package: com.song.sql
 * Type:    TestSql001
 * Date:    2024-12-02 10:59
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.expressions.UserDefinedFunction;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.udf;

/**
 * 功能描述： 获取一列
 * 一行进入，一行出
 *
 * @author Songxianyang
 * @date 2024-12-02 10:59
 */
public class TestSqlUdf01 {

    public static void main(String[] args) {
        // 创建配置对象
        SparkSession session = SparkSession.builder()
                .appName("SparkSql")
                .master("local[*]")
                .getOrCreate();

        // 获取文件数据
        Dataset<Row> dataset = session.read().json("D:/work/project/huatech/yqmm-boot/yqmm-spark/file/user.json").repartition(2);

        // 转换对象dto
        // orReplace表示覆盖之前相同名称的视图
        dataset.createOrReplaceTempView("user1");
        // 优化
        session.udf().register("addNameUdf", (UDF1<String,String>) name -> "西瓜视频" + name,DataTypes.StringType);

        String sql = "select addNameUdf(name) newName from user1";
        // 通过sql文得方式来查
        Dataset<Row> rowDataset = session.sql(sql);
        rowDataset.show();


        session.close();
    }
}
