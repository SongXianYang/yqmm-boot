/*******************************************************************************
 * Package: com.song.sql
 * Type:    BufferObj
 * Date:    2024-12-15 19:53
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-12-15 19:53
 */
@Data
@AllArgsConstructor
public class BufferObj  implements Serializable {
    private static final long serialVersionUID = 4054713614823828380L;
    private Long sum;
    private Long count;
    //解决 File 'generated.java', Line 41, Column 11: No applicable constructor/method found for zero actual parameters; candidates are: "com.song.sql.BufferObj(java.lang.Long, java.lang.Long)"
    public BufferObj() {
    }
}
