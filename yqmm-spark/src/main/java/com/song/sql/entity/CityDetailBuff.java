/*******************************************************************************
 * Package: com.song.sql.entity
 * Type:    CityDetailBuff
 * Date:    2024-12-25 16:56
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-12-25 16:56
 */
@Data
public class CityDetailBuff implements Serializable {
    private static final long serialVersionUID = 900106356422415508L;
    // 总个数
    private Long totalNumber;
    // key ：城市  value：地市个数
    private Map<String, Long> map = new HashMap<>();

    public CityDetailBuff(Long totalNumber, Map<String, Long> map) {
        this.totalNumber = totalNumber;
        this.map = map;
    }

    public CityDetailBuff() {
    }
}
