/*******************************************************************************
 * Package: com.song.sql
 * Type:    TestSql001
 * Date:    2024-12-02 10:59
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import com.sun.org.apache.regexp.internal.RE;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.api.java.function.ReduceFunction;
import org.apache.spark.sql.*;
import scala.Tuple2;

/**
 * 功能描述： 转换对象得方式
 *
 * @author Songxianyang
 * @date 2024-12-02 10:59
 */
public class TestSqlToObject {

    public static void main(String[] args) {
        // 创建配置对象
        SparkSession session = SparkSession.builder()
                .appName("SparkSql")
                .master("local[*]")
                .getOrCreate();
        // 获取文件数据
        Dataset<Row> dataset = session.read().json("D:/work/project/huatech/yqmm-boot/yqmm-spark/file/user.json").repartition(2);

        // 转换对象dto
        Dataset<UserDto> userDtoDataset = dataset.as(Encoders.bean(UserDto.class));
        // userDtoDataset.show();
        // 映射对象
        Dataset<UserDto> toObject = dataset.map(new MapFunction<Row, UserDto>() {
                                               private static final long serialVersionUID = -1949484293910599855L;

                                               @Override
                                               public UserDto call(Row row) throws Exception {
                                                   return new UserDto(row.getLong(0), row.getString(1));
                                               }
                                           },
                Encoders.bean(UserDto.class)
        );
        // toObject.show();
        // 排序
        // toObject.sort(new Column("age")).show();
        // 分组 区分
        RelationalGroupedDataset groupDS = toObject.groupBy(new Column("name"));
         // groupDS.count().show();


        KeyValueGroupedDataset<String, UserDto> stringUserDtoKeyValueGroupedDataset = toObject.groupByKey(new MapFunction<UserDto, String>() {
            @Override
            public String call(UserDto userDto) throws Exception {
                return userDto.getName();
            }

            private static final long serialVersionUID = -7214559515977811001L;
        }, Encoders.STRING());
        stringUserDtoKeyValueGroupedDataset.count().show();


        session.close();
    }
}
