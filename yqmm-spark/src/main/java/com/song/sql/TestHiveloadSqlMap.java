package com.song.sql;

import com.song.sql.entity.CityDetailAgg;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.apache.spark.sql.functions.udaf;


/**
 * udaf
 *
 */
public class TestHiveloadSqlMap {
    public static void main(String[] args) {
        Map<String, Long> b1Map = new HashMap<>();
        b1Map.put("song", 1L);
        Map<String, Long> b2Map = new HashMap<>();
        b2Map.put("song", 2L);
        b2Map.put("xian", 5L);


        Map<String, Long> allMap = new HashMap<>();
        allMap.putAll(b1Map);
        allMap.putAll(b2Map);

        // 2不变 1遍历
        for (Map.Entry<String, Long> entry : b1Map.entrySet()) {
            if (Objects.nonNull(b2Map.get(entry.getKey()))) {
                allMap.put(entry.getKey(), entry.getValue() + b2Map.get(entry.getKey()));
            }
        }

        System.out.println(allMap);


    }
}
