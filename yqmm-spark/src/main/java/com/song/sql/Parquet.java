/*******************************************************************************
 * Package: com.song.sql
 * Type:    Parquet
 * Date:    2024-12-16 10:48
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * 功能描述：
 *列式存储的数据自带列分割
 * @author Songxianyang
 * @date 2024-12-16 10:48
 */
public class Parquet {
    public static void main(String[] args) {

        //
        SparkSession session = SparkSession.builder()
                .appName("SparkSql")
                .master("local[1]")
                .getOrCreate();
        // 读取
        Dataset<Row> parquet =
                session.read()
                        .parquet("yqmm-spark/file/users.parquet");


        parquet.show();
        session.close();
    }
}
