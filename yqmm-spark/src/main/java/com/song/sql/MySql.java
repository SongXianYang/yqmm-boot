/*******************************************************************************
 * Package: com.song.sql
 * Type:    MySql
 * Date:    2024-12-16 11:03
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Properties;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-12-16 11:03
 */
public class MySql {

    public static void main(String[] args) {
        //
        SparkSession session = SparkSession.builder()
                .appName("SparkSql")
                .master("local[1]")
                .getOrCreate();

        Properties properties = new Properties();
        properties.setProperty("user","root");
        properties.setProperty("password","root#123");


        Dataset<Row> jdbc = session.read().jdbc("jdbc:mysql://127.0.0.1:3306/person?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai"
                , "t_user", properties
        );

        jdbc.write().jdbc("jdbc:mysql://127.0.0.1:3306/person?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai"
                , "t_user_spark_test", properties
        );

        jdbc.show();
        session.close();
    }

}
