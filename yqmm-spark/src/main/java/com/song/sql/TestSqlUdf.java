/*******************************************************************************
 * Package: com.song.sql
 * Type:    TestSql001
 * Date:    2024-12-02 10:59
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.expressions.UserDefinedFunction;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.udf;

/**
 * 功能描述： 获取一列
 * 一行进入，一行出
 *
 * @author Songxianyang
 * @date 2024-12-02 10:59
 */
public class TestSqlUdf {

    public static void main(String[] args) {
        // 创建配置对象
        SparkSession session = SparkSession.builder()
                .appName("SparkSql")
                .master("local[*]")
                .getOrCreate();

        // 获取文件数据
        Dataset<Row> dataset = session.read().json("D:/work/project/huatech/yqmm-boot/yqmm-spark/file/user.json").repartition(2);

        // 转换对象dto
        // orReplace表示覆盖之前相同名称的视图
        dataset.createOrReplaceTempView("user1");

        UserDefinedFunction addNameUdf = udf(new UDF1<String, String>() {
            @Override
            public String call(String s) throws Exception {
                return "西瓜视频" + s;
            }

            private static final long serialVersionUID = -5286177773463154038L;
        }, DataTypes.StringType);
        // 把自己的utf 注册进来
        session.udf().register("addNameUdf",addNameUdf);


        String sql = "select addNameUdf(name) newName from user1";
        // 通过sql文得方式来查
        Dataset<Row> rowDataset = session.sql(sql);
        rowDataset.show();


        session.close();
    }
}
