/*******************************************************************************
 * Package: com.song.boot.springstudy.myspring.event
 * Type:    PersonEvent
 * Date:    2024-09-27 14:20
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.myspring.event;

import lombok.Data;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-09-27 14:20
 */
@Data
public class PersonEvent {
    private Person person;
    private String addOrUpdate;
    public PersonEvent(Person person, String addOrUpdate) {
        this.person = person;
        this.addOrUpdate = addOrUpdate;
    }

}
