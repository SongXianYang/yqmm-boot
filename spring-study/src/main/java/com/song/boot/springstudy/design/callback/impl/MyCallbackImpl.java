/*******************************************************************************
 * Package: com.song.boot.springstudy.design.callback.impl
 * Type:    MyCallbackImpl
 * Date:    2024-04-18 11:43
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.callback.impl;

import com.song.boot.springstudy.design.callback.MyCallback;

/**
 * 功能描述： 可以实现多种不同业务的回调函数
 *  这里简单处理
 *  模拟事务
 * @author Songxianyang
 * @date 2024-04-18 11:43
 */
public class MyCallbackImpl implements MyCallback {
    @Override
    public void onSuccess() {
        // 成功之后的后续处理
        System.out.println("commit事务");
    }

    @Override
    public void onFailure() {
        // 失败之后的后续处理
        System.out.println("Rollback事务");
    }
}
