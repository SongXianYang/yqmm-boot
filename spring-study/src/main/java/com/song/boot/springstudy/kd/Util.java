/*******************************************************************************
 * Package: com.song.boot.springstudy.kd
 * Type:    Util
 * Date:    2023-06-18 18:38
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.kd;

import net.sf.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-06-18 18:38
 */
public class Util {

    public String getToken(String xh, String pwd) throws URISyntaxException, IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        URIBuilder uriBuilder = new URIBuilder("http://kdcnujw.yinghuaonline.com/sdsfdxkdxy/");
        uriBuilder.setParameter("method", "authUser").setParameter("xh", xh).setParameter("pwd", pwd);
        HttpGet httpGet = new HttpGet(uriBuilder.build());
        CloseableHttpResponse response = httpClient.execute(httpGet);
        String s = null;
        if (response.getStatusLine().getStatusCode() == 200) {
            HttpEntity entity = (HttpEntity) response.getEntity();
            s = EntityUtils.toString((org.apache.http.HttpEntity) entity, "utf-8");
        }
        JSONObject obj = new JSONObject();
        obj = obj.fromObject(s);
        Object token = obj.get("token");
        return token.toString();
    }

    public JSONObject execute(List<BasicNameValuePair> list, String token)    {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        URIBuilder uriBuilder = null;
        try {
            uriBuilder = new URIBuilder("http:*****app.do");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.out.println("URL错误！");
        }
        //添加参数
        for (BasicNameValuePair i:list) {
            uriBuilder.setParameter(i.getName(),i.getValue());
        }

        HttpGet httpGet = null;
        try {
            //创建get请求
            httpGet = new HttpGet(uriBuilder.build());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.out.println("util中httpget错误");
        }
        httpGet.setHeader("token",token);
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("util中execute错误");
        }

        HttpEntity entity = (HttpEntity) response.getEntity();
        String s = null;
        try {
            s = EntityUtils.toString((org.apache.http.HttpEntity) entity, "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("字符串转换失败！");
        }

        JSONObject obj = new JSONObject();

        try {
            return obj.fromObject(s);
        }catch (Exception e) {
            s = "{\"result\":"+s+"}";
            return obj.fromObject(s);
        }
    }
}
