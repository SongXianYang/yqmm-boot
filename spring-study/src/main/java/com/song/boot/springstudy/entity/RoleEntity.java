/*******************************************************************************
 * Package: com.song.boot.springstudy.entity
 * Type:    RoleEntity
 * Date:    2022-01-20 18:18
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-01-20 18:18
 */
@Data
public class RoleEntity implements Serializable,Cloneable {

    private Integer uId;
    private String roleName;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
