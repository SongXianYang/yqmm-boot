/*******************************************************************************
 * Package: com.example.design.decorator
 * Type:    package-info
 * Date:    2025/3/11 10:29
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.decorator;/**
 * 功能描述：
 *假设我们有一个电商系统需要支持商品基础信息展示，并允许用户动态添加多种增值服务（如礼品包装、加急配送、延长保修），这些附加服务需要叠加计算总价和更新商品描述。
 * @author Songxianyang
 * @date 2025/3/11 10:29
 */