/*******************************************************************************
 * Package: com.example.design.chain
 * Type:    OAMain
 * Date:    2025/3/10 16:43
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.chain;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2025/3/10 16:43
 */
public class OAMain {
    public static void main(String[] args) {
        // 构建责任链
        ApproveHandler manager = new DepartmentManager();
        manager.setNext(new FinancialDirector()).setNext(new GeneralManager());
        // 模拟不同金额的报销申请
        manager.result("张三", 800);
        manager.result("李四", 3500);
        manager.result("王五", 12000);
    }
}
