package com.song.boot.springstudy.design.adapter;

//

/**
 * 第三方银行支付服务（不兼容的接口）
 * 被适配者（BankPaymentService）：已存在的第三方银行支付接口
 */
public class BankPaymentService {
    /**
     * 银行专用支付方法
     * @param fromAccount 付款方账户
     * @param toAccount   收款方账户
     * @param amountInCents 金额（分）
     * @return 支付结果
     */
    public boolean transfer(String fromAccount, String toAccount, long amountInCents) {
        System.out.printf("[银行支付] %s -> %s 转账 %d 分%n", fromAccount, toAccount, amountInCents);
        return true;
    }
}