/*******************************************************************************
 * Package: com.song.boot.springstudy.design.callback
 * Type:    User
 * Date:    2024-04-18 11:48
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.callback;

import com.song.boot.springstudy.design.builder.Cat;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-04-18 11:48
 */
public interface User {
    // 模拟新增
    void insert(Cat cat, MyCallback callback);

}
