/*******************************************************************************
 * Package: com.song.boot.springstudy.design.duty
 * Type:    NullCheck
 * Date:    2022-12-29 23:16
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.duty;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-12-29 23:16
 */
@Component
public class NullCheck extends AbstractCheck {
    @Override
    protected void check(LeaveVO vo) {
        Assert.notNull(vo.getAge(),"年龄不能为null");
        Assert.notNull(vo.getDays(),"天数不能为null");
        Assert.notNull(vo.getReason(),"原因不能为null");
        Assert.notNull(vo.getName(),"名字不能为null");
        System.out.println("null字段校验");
    }
}
