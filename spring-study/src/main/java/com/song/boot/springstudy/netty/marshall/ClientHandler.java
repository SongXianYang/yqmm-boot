/*******************************************************************************
 * Package: com.song.boot.springstudy.marshall
 * Type:    ClientHandler
 * Date:    2024-01-28 16:30
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.marshall;

import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import io.netty.util.ReferenceCountUtil;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-01-28 16:30
 */
public class ClientHandler extends ChannelInboundHandlerAdapter {


    // 当通道就绪时，会触发  往服务器发送数据
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("client ctx=-------------"+ctx);
        // 往服务器发送数据
        for (int i = 0, size = 50; i < size; i++) {
            ReqData rd = new ReqData();
            rd.setId(" "+i);
            rd.setName("我是消息"+i);
            rd.setRequestMessage("内容"+i);
            ctx.writeAndFlush(rd);
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        try{

            RespData data = (RespData) msg;
            System.out.println("输出服务器端响应内容：" + data.getId());
            System.out.println("输出服务器端响应内容：" + data.getName());
            System.out.println("输出服务器端响应内容：" + data.getRequestMessage());
        }finally {
            ReferenceCountUtil.release(msg);
        }
    }
}
