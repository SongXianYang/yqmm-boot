/*******************************************************************************
 * Package: com.song.boot.springstudy.design.observer
 * Type:    Test
 * Date:    2023-01-05 22:04
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.observer;

/**
 * 功能描述：
 * 需求：公众号群发文章，选择要群发的用户，发送文章，用户接受文章。
 * 选择观察者模式
 *
 * @author Songxianyang
 * @date 2023-01-05 22:04
 */

public class Test {
    public static void main(String[] args) {
        // 观察者：订阅者
        WxUser user1 = new WxUser("张一");
        WxUser user2 = new WxUser("张二");
        WxUser user3 = new WxUser("张三");
        WxUser user4 = new WxUser("张四");
        // 被观察者：发布者
        WxOfficialAccount officialAccount = new WxOfficialAccount();
        officialAccount.setWxOfficialAccountName("SteveCode");
        officialAccount.setText("设计模式-观察者模式开发及优化优质文章");
        // 选择用户
        officialAccount.add(user1);
        officialAccount.add(user2);
        officialAccount.add(user3);
        officialAccount.add(user4);
        // 删除所选用户
        officialAccount.delete(user2);
        // 发布文章
        officialAccount.send();
    }
}
