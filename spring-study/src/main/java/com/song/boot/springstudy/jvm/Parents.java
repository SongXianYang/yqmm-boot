/*******************************************************************************
 * Package: com.song.boot.springstudy.jvm
 * Type:    Parents
 * Date:    2022-08-17 22:29
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.jvm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * 功能描述：双亲委派
 *src/main/java/com/song/boot/springstudy/jvm/Parents.java
 * @author Songxianyang
 * @date 2022-08-17 22:29
 */
public class Parents {
    public static void main(String[] args) throws ClassNotFoundException, ParseException {
        Class<?> aClass = Parents.class.getClassLoader().loadClass("com.song.boot.springstudy.entity.UserEntity");
        System.out.println(aClass.getClassLoader());
        System.out.println(aClass.getDeclaredFields().length);
        System.out.println(String.class.getClassLoader());
        //System.out.println(System.);
        System.out.println(Parents.class.getClassLoader());
        System.out.println(Parents.class.getClassLoader().getParent().getParent());
    
        //String startDate = "2025-2";
        //
        //String yearMonth= startDate.substring(0, 4)+String.format("%02d",Integer.parseInt(startDate.substring(5, 7)))+"01";
        //
        //System.out.println(yearMonth);
    
    
        String s = "2022-1-21";
    
        System.out.println(s.length());
        if (s.length()==6){
            // 定义一个0
            String l = "0";
            // 补0
            System.out.println(s.substring(4, 5));
            System.out.println(s.substring(5, 6));
            //System.out.println(s.substring(0,4)+s.substring(4, 5)+l + s.substring(5, 6));
        }
    
        System.out.println("---------------------------");
    
        System.out.println(getTransTime(s));
    
        //String yearMonth= s.substring(0, 4)+String.format("%02d",Integer.parseInt(s.substring(5, 7)))+"01";
    
        //System.out.println(yearMonth);
        
        //String transTime = getTransTime(s);
        //System.out.println(transTime);
        
    }
    
    
    
    public static String getTransTime(String timeStr) throws ParseException {
    
        //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-M");
        //LocalDate localDate = LocalDate.parse(timeStr, dtf);
        //
    //M-dd
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-M") ;
        Date date = simpleDateFormat.parse(timeStr);
        
        
    
    
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        LocalDate localDate = localDateTime.toLocalDate();
        System.out.println(localDate.getYear());
        System.out.println(localDate.getMonth().getValue());
        String yearMonth= localDate.getYear()+"-"+String.format("%02d",localDate.getMonth().getValue());
        System.out.println("yearMonth = " + yearMonth);
        //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-M");
        //LocalDate parse = LocalDate.parse(timeStr, dtf);
        
        //LocalDateTime date = LocalDateTime.parse(timeStr,DateTimeFormatter.ofPattern("yyyy-MM"));
    
        //DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DateFormatTypeEnum.LONG_DATE.getCode());
        //System.out.println(parse.getYear());
        //System.out.println(parse.getMonth());
        return "假数据";
    
    }
    
   
}
