/*******************************************************************************
 * Package: com.hngtrust.interest.report.entity
 * Type:    RewardCountSale
 * Date:    2022-11-11 14:52
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.entity;


import lombok.Data;

/**
 * 功能描述：代销
 *
 * @author Songxianyang
 * @date 2022-11-11 14:52
 */
@Data
public class RewardCountSale extends RewardCountReportBase{
    /**
     * 代销机构
     */
    private String saleOrg;


}
