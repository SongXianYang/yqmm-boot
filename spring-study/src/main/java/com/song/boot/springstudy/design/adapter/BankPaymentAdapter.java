/*******************************************************************************
 * Package: com.example.design.adapter
 * Type:    BankPaymentAdapter
 * Date:    2025/3/10 15:02
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.adapter;

/**
 * 功能描述：
 *银行支付适配器（对象适配器）
 * 目标接口（PaymentProcessor）：定义统一的支付方法规范
 * @author Songxianyang
 * @date 2025/3/10 15:02
 */
public class BankPaymentAdapter implements PaymentProcessor{
    /**
     * 收款方账户
     */
    private String toAccount;
    /**
     * 第三方银行支付服务
     */
    private BankPaymentService service;

    public BankPaymentAdapter(String toAccount, BankPaymentService service) {
        this.toAccount = toAccount;
        this.service = service;
    }

    /**
     * 执行支付操作
     *
     * @param account 收款账户
     * @param amount  金额（元）
     * @return 支付是否成功
     */
    @Override
    public boolean processPayment(String account, double amount) {
        /**
         * 银行专用支付方法
         * @param fromAccount 付款方账户
         * @param toAccount   收款方账户
         * @param amountInCents 金额（分）
         * @return 支付结果
         */
        // 金额转换：元 -> 分
        long amountInCents = (long) (amount * 100);
        return service.transfer(account,toAccount,amountInCents);
    }
}
