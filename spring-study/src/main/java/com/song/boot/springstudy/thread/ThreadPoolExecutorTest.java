/*******************************************************************************
 * Package: com.song.boot.springstudy
 * Type:    ThreadPoolExecutorTest
 * Date:    2022-02-05 14:15
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.thread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述：线程池学习
 *
 * @author Songxianyang
 * @date 2022-02-05 14:15
 */
public class ThreadPoolExecutorTest {
    synchronized
    public static void main(String[] args) {
        /**
         当线程池中的线程数量超过核心线程数时，多余的线程会在等待一定时间后被销毁。
         这个等待时间就是 keepAliveTime 参数所指定的值。
         如果在这个时间内有新的任务提交到线程池中，那么这些空闲线程就会被重新利用，否则它们会被销毁
         */
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(3, 5, 5, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(5));
        // 向线程池提交任务
        for (int i = 0; i < threadPool.getCorePoolSize(); i++) {
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    for (int x = 0; x < 2; x++) {
                        System.out.println(Thread.currentThread().getName() + ":" + x);
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    
        // 关闭线程池
        threadPool.shutdown(); // 设置线程池的状态为SHUTDOWN，然后中断所有没有正在执行任务的线程
        // threadPool.shutdownNow(); // 设置线程池的状态为STOP，然后尝试停止所有的正在执行或暂停任务的线程，并返回等待执行任务的列表，该方法要慎用，容易造成不可控的后果
    }
}
