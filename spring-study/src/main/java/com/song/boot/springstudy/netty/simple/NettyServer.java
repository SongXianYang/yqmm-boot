/*******************************************************************************
 * Package: com.song.boot.springstudy.netty.simple
 * Type:    NettyServer
 * Date:    2023-06-04 17:38
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.simple;

import com.song.boot.springstudy.netty.simple.handler.NettyServerHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * 功能描述： 服务器
 *
 * @author Songxianyang
 * @date 2023-06-04 17:38
 */
public class NettyServer {
    public static void main(String[] args) throws InterruptedException {
        /*
        创建 bossEvent、workerEvent线程组
        bossEvent:处理连接请求
        workerEvent：处理业务
        // 创建BossGroup和WorkerGroup
        // bossGroup只处理连接请求，真正与客户端进行的业务处理，交给workerGroup完成。这两个都是无限循环
        // bossGroup和workerGroup默认含有的子线程（NioEventLoop）个数为 cpu核数*2
        // 可以自定义设置bossGroup和workerGroup的NioEventLoop线程数，如果workerGroup设置子线程数为8，则如果有超过8个客户端连接，会按照1，2，3...，8，1，2...的方式循环分配
         */

        EventLoopGroup bossEvent = new NioEventLoopGroup();
        EventLoopGroup workerEvent = new NioEventLoopGroup();

        try {
            // 服务器端 配置参数
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossEvent,workerEvent) // 设置两个线程组
                    .channel(NioServerSocketChannel.class) // nio实现类通道
                    .option(ChannelOption.SO_BACKLOG,128) // 设置线程队列
                    .childHandler(new ChannelInitializer<SocketChannel>() {  // 创建一个通道初始化对象（匿名对象）
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            socketChannel.pipeline().addLast(new NettyServerHandler());// 向管道追加一个处理器
                        }
                    });// 给workerGroup的EventLoop对应的管道设置处理器
            System.out.println("服务器端启动完成！");
            ChannelFuture cf = serverBootstrap.bind(6668).sync(); // 绑定端口，并且同步，生成了一个ChannelFuture对象

            // 注册监听器 到 ChannelFuture。从而获取 异步回调
            cf.addListener((ChannelFutureListener) future -> {
                if (future.isSuccess()) {
                    System.out.println("监听成功：：：》》》》6668");
                }else {
                    System.out.println("监听失败");
                }
            });

            cf.channel().closeFuture().sync(); // 对关闭通道进行见监听
        } finally {
            bossEvent.shutdownGracefully();  // 关闭
            workerEvent.shutdownGracefully();
        }
    }
}
