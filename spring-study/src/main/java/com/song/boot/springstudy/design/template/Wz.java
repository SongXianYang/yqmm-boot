/*******************************************************************************
 * Package: com.song.boot.springstudy.design.template
 * Type:    Wz
 * Date:    2023-01-02 16:50
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.template;

/**
 * 功能描述： 王者
 *
 * @author Songxianyang
 * @date 2023-01-02 16:50
 */
public class Wz extends BusiTemplate{
    @Override
    public void initialize() {
        System.out.println("王者-初始化游戏数据");
    }

    @Override
    void startPlay() {
        System.out.println("王者-开始游戏");
    }

    @Override
    void endPlay() {
        System.out.println("王者-结束游戏");
    }
}
