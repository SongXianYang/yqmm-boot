# 1.7和1.8 有什么区别
> 1.7 采用分段处理的思想，Segment内部继承了ReentrantLock。每一个Segment 就是一个Hashmap 。再读写的时候都是Lock来操作的。
> 
> 1.8 采用CAS与synchronized来保并发安全。数据结构是数组+链表+红黑树。
> 
[帮助文档](https://blog.csdn.net/g_ood_good_study/article/details/1204)
[帮助文档](https://blog.csdn.net/my_momo_csdn/article/details/101238097)