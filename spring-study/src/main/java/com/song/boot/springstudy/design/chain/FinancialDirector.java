/*******************************************************************************
 * Package: com.example.design.chain
 * Type:    DepartmentManager
 * Date:    2025/3/10 16:36
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.chain;

/**
 * 功能描述：具体处理者：财务总监
 *
 * @author Songxianyang
 * @date 2025/3/10 16:36
 */
public class FinancialDirector extends ApproveHandler{
    /**
     * 具体的处理逻辑方法
     * @param name 那个用户
     * @param amount 报销金额
     */
    @Override
    public void result(String name, int amount) {
        if (amount <= 5000) {
            System.out.printf("[财务总监] 审批通过 %s 的 %d 元报销%n",
                    name,amount);
        } else if (next  != null) {
            next.result(name, amount);
        }
    }
}
