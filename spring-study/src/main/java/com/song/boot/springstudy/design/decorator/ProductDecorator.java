/*******************************************************************************
 * Package: com.example.design.decorator
 * Type:    ProductDecorator
 * Date:    2025/3/11 10:31
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.decorator;

/**
 * 功能描述：抽象装饰器
 *
 * @author Songxianyang
 * @date 2025/3/11 10:31
 */
public abstract class ProductDecorator implements Product {
    protected Product product;

    public ProductDecorator(Product product) {
        this.product = product;
    }

    /**
     * 获取商品价格
     */

    public abstract double getPrice();

    /**
     * 获取商品描述
     */
    public abstract String getDescription();
}
