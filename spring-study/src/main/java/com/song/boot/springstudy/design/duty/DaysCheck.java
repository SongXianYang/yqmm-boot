/*******************************************************************************
 * Package: com.song.boot.springstudy.design.duty
 * Type:    Money
 * Date:    2022-12-29 23:16
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.duty;


import org.springframework.stereotype.Component;

/**
 * 功能描述： 天数校验
 *
 * @author Songxianyang
 * @date 2022-12-29 23:16
 */
@Component

public class DaysCheck extends AbstractCheck {
    @Override
    protected void check(LeaveVO vo) {
        if (vo.getDays()<=3) System.out.println("组长审批");
        if (vo.getDays()>6) System.out.println("经理审批");
        if (vo.getDays()>10) System.out.println("老板审批");
    }
}
