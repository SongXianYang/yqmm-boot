/*******************************************************************************
 * Package: com.song.boot.springstudy.nio
 * Type:    C
 * Date:    2022-09-03 22:40
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.io.nio.v1;

import lombok.SneakyThrows;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

/**
 * 功能描述： 客户端
 *公众号：SteveCode
 * @author Songxianyang
 * @date 2022-09-03 22:40
 */
public class C {
    @SneakyThrows
    public static void main(String[] args) {
        SocketChannel socketChannel = SocketChannel.open();
        InetSocketAddress inetSocketAddress = new InetSocketAddress("127.0.0.1", 9999);
        socketChannel.connect(inetSocketAddress);
        socketChannel.configureBlocking(false);
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        while (true){
            Scanner scanner = new Scanner(System.in);
            System.out.print("请输入:");
            String msg = scanner.nextLine();
            // 往buffer 放数据
            buffer.put(msg.getBytes());
            buffer.flip();
            // socketChannel 放buffer
            socketChannel.write(buffer);
            buffer.clear();
        }
    }
}
