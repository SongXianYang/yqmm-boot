/*******************************************************************************
 * Package: com.hngtrust.interest.report.entity
 * Type:    RewardCountReportBase
 * Date:    2022-11-08 15:20
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 功能描述： 基类
 *
 * @author Songxianyang
 * @date 2022-11-08 15:20
 */
@Data
public class RewardCountReportBase {
    /**
     * 项目编号
     */
    private String projectId;
    /**
     * 项目名称
     */
    private String projectName;

    //public RewardCountReportBase(String projectId, String projectName) {
    //    this.projectId = projectId;
    //    this.projectName = projectName;
    //}
}
