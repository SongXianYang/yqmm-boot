/*******************************************************************************
 * Package: com.song.boot.springstudy.netty.simple.handler
 * Type:    NettyServerHandler
 * Date:    2023-06-04 17:48
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.simple.handler;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

/**
 * 功能描述：  管道内处理器
 *
 * @author Songxianyang
 * @date 2023-06-04 17:48
 * 自定义一个处理器，需要继承netty规定好的某个HandlerAdapter
 */
public class NettyServerHandler extends ChannelInboundHandlerAdapter {
    // 读事件（读取客户端发送的数据）
    // ctx 上下文对象，含有 管道pipeline，一个管道里会有很多个业务处理的handler，通道channel，地址
    // msg 客户端发送的数据，是Object格式的
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("ChannelHandlerContext 他到底是啥："+ctx);
        System.out.println("通道："+ctx.channel());
        System.out.println("管道："+ctx.pipeline());// 本质是一个双向链表
        // 将msg转成一个ByteBuf
        ByteBuf byteBuf = (ByteBuf) msg;
        System.out.println("客户端发送过来的消息为：：："+byteBuf.toString(CharsetUtil.UTF_8));
        System.out.println("客户端地址为：：："+ctx.channel().remoteAddress());
    }
    // 读事件完毕，发送数据回复给客户端
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.writeAndFlush(Unpooled.copiedBuffer("你好！客户端！",CharsetUtil.UTF_8));
    }
    // 发生异常，则关闭通道
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}
