/*******************************************************************************
 * Package: com.song.boot.springstudy.jvm
 * Type:    PCText
 * Date:    2022-11-16 21:53
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.jvm;

/**
 * 功能描述：程序计数器
 *
 * @author Songxianyang
 * @date 2022-11-16 21:53
 */
public class PCTest {
    public static final String SSS = "宋先阳";
    public static void main(String[] args) {
        int i = 99;
        int d = 11;
        int c = i+d;

        String name = "song";
        System.out.println(name);
        System.out.println(c);
        System.out.println(SSS);

    }

    public void test1() {
        int test = test();
        System.out.println(test);
    }

    public int test() {
        int i=2;
        int j=4;
        int n= i+j;
        return n;
    }
}
