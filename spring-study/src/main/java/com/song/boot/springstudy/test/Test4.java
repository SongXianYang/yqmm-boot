/*******************************************************************************
 * Package: com.song.boot.springstudy
 * Type:    Test4
 * Date:    2022-09-13 17:30
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

import com.song.boot.springstudy.entity.One;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-09-13 17:30
 */
@Slf4j
public class Test4 {
    public static void main(String[] args) {
        Instant stime1 = Instant.now();
        //List<One> ones = new ArrayList<>(20433098);
        Map<String, String> hashMap = new HashMap<>(20433098);
        One one;
        for (int i = 0, size = 20433098; i < size; i++) {
            one = new One();
            one.setCount("value" + i);
            hashMap.put("key"+i, one.getCount());
        }
        Instant stime2 = Instant.now();
        log.info(">>>转换耗时={}...", Duration.between(stime1, stime2).toMillis());

    }
}
