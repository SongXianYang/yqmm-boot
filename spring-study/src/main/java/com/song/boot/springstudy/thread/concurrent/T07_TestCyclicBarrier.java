package com.song.boot.springstudy.thread.concurrent;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 *
 * 需求 parties 值必须先确定 然后在进行发车（发车的这个线程是用指定数据的parties这个线程。
 *
 * 例如：
 * 数据、网络、文件 =parties
 * 以上3三个操作完成之后在执行：barrierAction
 *
 */
public class T07_TestCyclicBarrier {
    public static void main(String[] args) {
        //CyclicBarrier barrier = new CyclicBarrier(20);

        CyclicBarrier barrier = new CyclicBarrier(20, () -> System.out.println(Thread.currentThread().getName() + "---------------------满人"));

        /*CyclicBarrier barrier = new CyclicBarrier(20, new Runnable() {
            @Override
            public void run() {
                System.out.println("满人，发车");
            }
        });*/

        for(int i=0; i<40; i++) {

            int finalI = i;
            new Thread(()->{
                    try {
                        barrier.await();
                        System.out.println(Thread.currentThread().getName() + "人数:"+ finalI);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                }).start();
            
        }
    }
}
