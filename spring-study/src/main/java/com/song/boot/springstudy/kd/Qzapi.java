/*******************************************************************************
 * Package: com.song.boot.springstudy.kd
 * Type:    Qzapi
 * Date:    2023-06-18 18:35
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.kd;


import net.sf.json.JSONObject;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-06-18 18:35
 */
public interface Qzapi {

    /**
     * 相当于登录
     * @param xh  学号
     * @param pwd  密码
     * @return
     */
    JSONObject authUser(String xh, String pwd);

    /**
     *获取所提交的日期的时间、周次、学年等信息
     * @param currDate //格式为"YYYY-MM-DD"，必填，留空调用成功，但返回值均为null
     * @return
     */
    JSONObject getCurrentTime(String currDate);

    /**
     * 获取一周的课程信息
     * @param xh 必填，使用与获取token时不同的学号，则可以获取到新输入的学号的课表
     * @param xnxqid 格式为"YYYY-YYYY-X"，非必填，不包含时返回当前日期所在学期课表
     * @param zc //必填
     * @return
     */
    JSONObject getKbcxAzc(String xh,String xnxqid,String zc);

    /**
     *获取成绩信息
     * @param xh //必填，可以添加非本token学号查询他人成绩
     * @param xnxqid //非必填，不填输出全部成绩
     * @return
     */
    JSONObject getCjcx(String xh,String xnxqid);

    /**
     * 获取考试信息
     * @param xh
     * @return
     */
    JSONObject getKscx(String xh);

    /**
     * 获取某个校区教学楼信息
     * @param xqid //校区ID
     * @return
     */
    JSONObject getJxlcx(String xqid);

    /**
     * 获取空教室信息
     * @param time  //格式"YYYY-MM-DD",非必填，默认返回当前日期空闲教室
     * @param idleTime //有allday,am,pm,night四种取值，非必填，默认值疑似allday
     * @param xqid  //校区ID，非必填
     * @param jxlid //教学楼ID，非必填
     * @param classroomNumber  //可选项 30,30-40,40-50,60(分别意为30人以下，30-40人,···,60人以上)
     * @return
     */
    JSONObject getKxJscx(String time,String idleTime,String xqid,String jxlid,String classroomNumber);

    /**
     *  获取当前token的用户信息 注意：此API测试与参考文档不同，待修改/补充
     * @param xh 疑似非必填，添加或不添加本参数返回相同值
     * @return
     */
    JSONObject getStudentIdInfo(String xh);

    /**
     *
     * @param xh 学号
     * @return
     */
    JSONObject getUserInfo(String xh);

    /**
     * 获取学年学期信息
     * @param xh 2017xxxxxx'  //非必填
     * @return
     */
    JSONObject getXnxq(String xh);

    /**
     * 获取校区信息
     * @return
     */
    JSONObject getXqcx();
}
