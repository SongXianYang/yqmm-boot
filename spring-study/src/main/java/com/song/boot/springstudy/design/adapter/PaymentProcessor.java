package com.song.boot.springstudy.design.adapter;


/**
 * 目标接口（PaymentProcessor）：定义统一的支付方法规范
 */
public interface PaymentProcessor {
    /**
     * 执行支付操作
     * @param account 收款账户
     * @param amount 金额（元）
     * @return 支付是否成功
     */
    boolean processPayment(String account, double amount);
}