/*******************************************************************************
 * Package: com.example.design.chain
 * Type:    DepartmentManager
 * Date:    2025/3/10 16:36
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.chain;

/**
 * 功能描述：具体处理者：总经理
 *
 * @author Songxianyang
 * @date 2025/3/10 16:36
 */
public class GeneralManager extends ApproveHandler {
    /**
     * 具体的处理逻辑方法
     *
     * @param name   那个用户
     * @param amount 报销金额
     */
    @Override
    public void result(String name, int amount) {

        System.out.printf("[总经理] 审批通过 %s 的 %d 元报销%n",
                name, amount);

        // todo 注意：没有下级
    }
}

