/*******************************************************************************
 * Package: com.song.boot.springstudy.io
 * Type:    package-info
 * Date:    2024-06-20 16:19
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.io;/**
 * 功能描述：
 *公众号：SteveCode
 * @author Songxianyang
 * @date 2024-06-20 16:19
 */



/*
总结
Java中的BIO、NIO和AIO都是处理输入/输出（I/O）操作的模型，但它们在处理方式和效率上有所不同。
BIO（Blocking I/O）：BIO是最传统的I/O模型，它的操作都是阻塞的。这意味着，当一个线程发起一个I/O操作后，必须等待操作完成才能进行其他任务。因此，BIO在处理大量并发连接时效率较低，但其编程模型简单。
NIO（Non-blocking I/O）：NIO是非阻塞的I/O模型，它允许线程在等待I/O操作完成时进行其他任务。NIO引入了Channel和Buffer的概念，以及Selector用于多路复用。NIO适合处理大量并发连接，但其编程模型相对复杂。
AIO（Asynchronous I/O）：AIO是真正的异步I/O模型，应用程序无需等待I/O操作的完成，当操作完成时，操作系统会通知应用程序。AIO使用回调函数或Future对象来获取操作结果，适合处理大量并发连接，其编程模型相对简单。
总之，BIO、NIO和AIO各有优缺点，适用的场景也不同。BIO适合连接数目较少且固定的架构，NIO适合连接数目多，但是并发读写操作相对较少的场景，AIO则适合连接数目多，且并发读写操作也多的场景。在选择使用哪种I/O模型时，需要根据具体的应用场景和需求进行权衡。
 */