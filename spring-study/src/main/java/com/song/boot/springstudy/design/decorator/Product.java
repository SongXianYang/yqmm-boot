package com.song.boot.springstudy.design.decorator;

// 抽象组件：商品接口
public interface Product {
    /**
     * 获取商品价格
     */
    double getPrice();

    /**
     * 获取商品描述
     */
    String getDescription();
}