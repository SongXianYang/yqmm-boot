package com.song.boot.springstudy.design.decorator;

// 具体装饰器：延长保修
class ExtendedWarrantyDecorator extends ProductDecorator {
    public ExtendedWarrantyDecorator(Product product) {
        super(product);
    }

    @Override
    public double getPrice() {
        return product.getPrice() + 200.0;
    }

    @Override
    public String getDescription() {
        return product.getDescription() + " + 延长保修";
    }
}