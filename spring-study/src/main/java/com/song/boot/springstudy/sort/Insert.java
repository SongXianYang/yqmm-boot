/*******************************************************************************
 * Package: com.song.boot.springstudy.sort
 * Type:    Insert
 * Date:    2022-05-03 15:37
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.sort;

/**
 学习地址：https://www.bilibili.com/video/BV1Ur4y1w7tv
 * @author Songxianyang
 * @date 2022-05-03 15:37
 */
@Deprecated
public class Insert {
    public static void main(String[] args) {
        int[] is = {8, 3, 9, 2, 1, 6, 5};
        //insertSort(is);
        System.out.println("-------------");
        System.out.println("冒泡");
        //bubbleSort(is);
        System.out.println("-------------");
        System.out.println("选择");
        select(is);
    }
    
    private static void insertSort(int[] ints) {
        // 轮数 i
        for (int i = 1; i < ints.length; i++) {
            int tmp;
            // 比较 并交换
            for (int j = 0; j < ints.length; j++) {
                // 每一轮要数据为 i   去比较 这个数组中的每个数据
                // i< j  比较并交换
                if (ints[i] < ints[j]) {
                    tmp = ints[i];
                    ints[i] = ints[j];
                    ints[j] = tmp;
                }
            }
        }
        for (int i = 0; i < ints.length; i++) {
            System.out.println(ints[i]);
        }
    }
    
    /**
     *
     * @param ints
     */
    private static void bubbleSort(int[] ints) {
        
        int tmp;
        // j 表示 轮数
        // ints.length-1 排序
        for (int j = 0; j < ints.length - 1; j++) {
            // 两两相比 并交换位置  前面>后面 交换
            // 为了防止 ints.length-1 数组下标越界
            for (int i = 0; i < ints.length - 1; i++) {
                if (ints[i] > ints[i + 1]) {
                    tmp = ints[i];
                    ints[i] = ints[i + 1];
                    ints[i + 1] = tmp;
                }
            }
        }
        
        for (int i = 0; i < ints.length; i++) {
            System.out.println(ints[i]);
        }
    }
    
    private static void select(int[] ints) {
        
        for (int j = 0; j < ints.length - 1; j++) {
            int k = j;
            int tmp;
            for (int i = j + 1; i < ints.length - 1; i++) {
                if (k > ints[i + 1]) {
                    k =ints[i + 1];
                }
                tmp = ints[j];
                ints[j] = ints[k];
                ints[k] = tmp;
            }
            
        }
        
        for (int i = 0; i < ints.length; i++) {
            System.out.println(ints[i]);
        }
    }
    
}
