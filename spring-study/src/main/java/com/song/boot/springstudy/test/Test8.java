/*******************************************************************************
 * Package: com.song.boot.springstudy.test
 * Type:    Test8
 * Date:    2023-07-22 15:34
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

/**
 * 功能描述：
 * ++在前先赋值
 * ++在后后赋值
 * @author Songxianyang
 * @date 2023-07-22 15:34
 */
public class Test8 {
    public static void main(String[] args) {
        int i = 5;
        System.out.println(++i);
        System.out.println(i++);
        System.out.println(i--);
        System.out.println(i);
    }
}
