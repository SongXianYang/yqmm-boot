/*******************************************************************************
 * Package: com.song.boot.springstudy.sort
 * Type:    ChaRu
 * Date:    2025-03-08 16:44
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.sort;

/**
 * 功能描述：插入排序  就是和纸牌游戏一样 拿到那一张 给牌放到指定位置,第一张牌得位置不动
 *
 * @author Songxianyang
 * @date 2025-03-08 16:44
 */
public class ChaRu {
    public static void main(String[] args) {
        int[] is = {8, 3, 9, 2, 1, 6, 5};

        // 循环到第几个
        for (int j = 0; j < is.length-1; j++) {
            // 往前排
            for (int i = j + 1; i > 0; i--) {

                int a1 = is[i - 1];
                int a2 = is[i];
                // 谁是最小
                if (a1 > a2) {
                    is[i - 1] = a2;
                    is[i] = a1;
                }
            }
        }

        // for (int i = 1; i >= 1; i--) {
        //     int a1 = is[0];
        //     int a2 = is[i];
        //     // 谁是最小
        //     if (a1 > a2) {
        //         is[0] = a2;
        //         is[i]=a1;
        //     }
        // }


        for (int i : is) {
            System.out.print(i);
        }
    }
}
