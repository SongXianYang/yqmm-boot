/*******************************************************************************
 * Package: com.example.design.adapter
 * Type:    AdapterMain
 * Date:    2025/3/10 15:07
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.adapter;

/**
 * 功能描述：
 *接口兼容：使不兼容的银行接口能够接入现有支付系统
 * 业务解耦：支付方式的变更不会影响客户端代码
 * 可扩展性：未来新增支付方式只需添加新适配器
 * 单一职责：将接口转换逻辑封装在独立类中
 * @author Songxianyang
 * @date 2025/3/10 15:07
 */
public class AdapterMain {
    public static void main(String[] args) {
        // 支付宝
        AlipayProcessor alipayProcessor = new AlipayProcessor();
        alipayProcessor.processPayment("user_123", 9993.2);

        BankPaymentAdapter adapter = new BankPaymentAdapter("sys_admin_account", new BankPaymentService());
        adapter.processPayment("user_123", 9993.2);
    }
}
