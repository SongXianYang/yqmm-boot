/*******************************************************************************
 * Package: com.song.boot.springstudy.bug
 * Type:    Cpu1_Serializable_1
 * Date:    2025/3/17 16:38
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.bug;

import com.song.boot.springstudy.entity.RoleEntity;
import com.song.boot.springstudy.thread.pool.TimeTool;
import org.apache.commons.lang3.SerializationUtils;


import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2025/3/17 16:38
 */
public class Cpu1_Serializable_1 {
    public static void main(String[] args) {
        int num = 1000000;

        List<RoleEntity> list = new ArrayList<>(num);

        TimeTool.check("", () -> {
            for (int i = 0; i < num; i++) {
                RoleEntity entity = new RoleEntity();
                entity.setRoleName("name"+i);
                entity.setUId(i);
                list.add(entity);
                try {
                    Object clone = entity.clone();
                } catch (CloneNotSupportedException e) {
                    throw new RuntimeException(e);
                }
            }
        });


        // TimeTool.check("", () -> {
        //     while (true){
        //         int i=0;
        //         RoleEntity entity = new RoleEntity();
        //         entity.setRoleName("name"+i);
        //         entity.setUId(i);
        //         list.add(entity);
        //         try {
        //                         Object clone = entity.clone();
        //                     } catch (CloneNotSupportedException e) {
        //                         throw new RuntimeException(e);
        //                     }
        //
        //         i++;
        //     }
        // });


    }
}
