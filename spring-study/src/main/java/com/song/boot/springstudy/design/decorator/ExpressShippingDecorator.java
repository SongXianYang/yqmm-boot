package com.song.boot.springstudy.design.decorator;

// 具体装饰器：加急配送
class ExpressShippingDecorator extends ProductDecorator {
    public ExpressShippingDecorator(Product product) {
        super(product);
    }

    @Override
    public double getPrice() {
        return product.getPrice() + 100.0;
    }

    @Override
    public String getDescription() {
        return product.getDescription() + " + 加急配送";
    }
}