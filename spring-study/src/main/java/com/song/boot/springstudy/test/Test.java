/*******************************************************************************
 * Package: com.song.boot.springstudy
 * Type:    Test
 * Date:    2022-01-20 18:15
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

import com.song.boot.springstudy.entity.ListUserRole;
import com.song.boot.springstudy.entity.RoleEntity;
import com.song.boot.springstudy.entity.UserEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-01-20 18:15
 */
public class Test {
    public static void main(String[] args) {
        List<ListUserRole> listUserRoles = new ArrayList<>();
    
        List<UserEntity> users = new ArrayList<>();
        UserEntity user = new UserEntity();
        user.setId(1);
        user.setName("张扬");
        user.setType("VIP");
        users.add(user);
        UserEntity user1 = new UserEntity();
        user1.setId(2);
        user1.setName("张三丰");
        user1.setType("SVIP");
        users.add(user1);
    
        Map<String, List<UserEntity>> map = new HashMap<>(2);
        
        map.put("one",users);
        
        map.forEach((s, userEntities) -> {
            userEntities.forEach(userEntity -> {
                System.out.println(userEntity);
            });
        });
    
        Set<Map.Entry<String, List<UserEntity>>> entries = map.entrySet();
        entries.forEach(stringListEntry -> {
            stringListEntry.getValue().forEach(userEntity -> {
                System.out.println(userEntity);
            });
        });
        
    
        System.out.println("------------");
        
        
        
        
        
        
    
        List<RoleEntity> roles = new ArrayList<>();
    
        RoleEntity role = new RoleEntity();
        role.setUId(1);
        role.setRoleName("管理员");
        roles.add(role);
        
        RoleEntity role1 = new RoleEntity();
        role1.setUId(1);
        role1.setRoleName("管理员2");
        roles.add(role1);
        RoleEntity role2 = new RoleEntity();
        role2.setUId(2);
        role2.setRoleName("管理员3");
        roles.add(role2);
    
        Map<Integer, String> userMap = users.stream().collect(Collectors.toMap(UserEntity::getId, UserEntity::getName));
    
        for (Integer integer : userMap.keySet()) {
            System.out.println(userMap.get(integer));
        }
    }
}
