package com.song.boot.springstudy.design.decorator;

// 具体装饰器：礼品包装
class GiftWrapDecorator extends ProductDecorator {
    public GiftWrapDecorator(Product product) {
        super(product);
    }

    @Override
    public double getPrice() {
        return product.getPrice() + 50.0;
    }

    @Override
    public String getDescription() {
        return product.getDescription() + " + 礼品包装";
    }
}