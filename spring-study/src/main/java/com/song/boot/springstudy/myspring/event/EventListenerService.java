/*******************************************************************************
 * Package: com.song.boot.springstudy.myspring.event
 * Type:    EventListenerService
 * Date:    2024-09-27 15:50
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.myspring.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-09-27 15:50
 */
@Slf4j
@Component
public class EventListenerService {
    @EventListener
    public void handlePersonEvent(PersonEvent personEvent) {
        log.info("监听到PersonEvent:   {}", personEvent);
    }
}
