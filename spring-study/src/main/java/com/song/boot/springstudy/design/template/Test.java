/*******************************************************************************
 * Package: com.song.boot.springstudy.design.template
 * Type:    Test
 * Date:    2023-01-02 16:52
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.template;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-01-02 16:52
 */

public class Test {
    public static void main(String[] args) {
        BusiTemplate cj = new Cj();
        // 钩子指定
        cj.setControl(true);
        BusiTemplate wz = new Wz();
        wz.setControl(false);
        cj.play();
        System.out.println("-----------------");
        wz.play();
    }
}
