/*******************************************************************************
 * Package: com.example.design.adapter
 * Type:    package-info
 * Date:    2025/3/10 14:56
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.adapter;/**
 * 功能描述：
 *假设我们有一个电商系统，最初只支持支付宝支付。现在需要接入新的银行转账支付方式，但银行支付的接口与现有系统不兼容。我们通过适配器模式统一支付接口。
 * @author Songxianyang
 * @date 2025/3/10 14:56
 */