/*******************************************************************************
 * Package: com.song.boot.springstudy.sort
 * Type:    XuanZe
 * Date:    2025-03-06 23:05
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.sort;

/**
 * 功能描述： 找到最小的放到前面  中途遇见小的先记录  再往后面找 找更小的
 *
 * @author Songxianyang
 * @date 2025-03-06 23:05
 */
public class XuanZe {
    public static void main(String[] args) {
        int[] is = {8, 3, 9, 2, 1, 6, 5};
        for (int j = 0; j < is.length-1; j++) {

            for (int i = j; i < is.length-1; i++) {
                int a1 = is[j];
                int a2 = is[i+1];

                // 谁是最小
                if (a1 > a2) {
                    is[j] = a2;
                    is[i+1]=a1;
                }

            }
        }

        // for (int i = 0; i < is.length-1; i++) {
        //     int a1 = is[0];
        //     int a2 = is[i+1];
        //
        //     // 谁是最小
        //     if (a1 > a2) {
        //         is[0] = a2;
        //         is[i+1]=a1;
        //     }
        //
        // }

        for (int i : is) {
            System.out.print(i);

        }
    }
}
