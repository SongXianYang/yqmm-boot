/*******************************************************************************
 * Package: com.song.boot.springstudy.myspring.aware
 * Type:    MyBeanNameAware
 * Date:    2024-01-31 17:01
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.myspring.aware;

import lombok.Data;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.stereotype.Component;

/**
 * 功能描述：通过实现该接口，Bean实例可以获取到其在Spring容器中的名称
 *
 * @author Songxianyang
 * @date 2024-01-31 17:01
 */
@Component
@Data
public class MyBeanNameAware implements BeanNameAware {

    private String name;

    @Override
    public void setBeanName(String name) {
        this.name =name;
    }
}
