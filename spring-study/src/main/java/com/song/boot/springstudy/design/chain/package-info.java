/*******************************************************************************
 * Package: com.example.design.chain
 * Type:    package-info
 * Date:    2025/3/10 16:29
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.chain;/**
 * 功能描述：
 *   责任链模式
 *   假设我们有一个OA系统需要处理费用报销审批流程，不同金额的报销需要不同级别的领导审批：
 * ≤1000元：部门经理审批
 * 1000~5000元：财务总监审批
 * ≥5000元：总经理审批
 * @author Songxianyang
 * @date 2025/3/10 16:29
 */