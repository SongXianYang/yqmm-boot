/*******************************************************************************
 * Package: com.song.boot.springstudy.netty.simple.handler
 * Type:    NettyClientHandler
 * Date:    2023-06-04 18:11
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.chat;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-06-04 18:11
 */
public class ChartNettyClientHandler extends SimpleChannelInboundHandler<String> {


    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, String s) throws Exception {
        System.out.println("------>"+s.trim()+"<--------");
    }
}
