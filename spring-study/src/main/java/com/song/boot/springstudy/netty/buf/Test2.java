/*******************************************************************************
 * Package: com.song.boot.springstudy.netty.buf
 * Type:    Test1
 * Date:    2023-06-22 15:44
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.buf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.util.CharsetUtil;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-06-22 15:44
 */
public class Test2 {
    public static void main(String[] args) {
        ByteBuf clientMsg = Unpooled.copiedBuffer("abcdefg!", CharsetUtil.UTF_8);
        System.out.println(clientMsg);
        byte[] array = clientMsg.array();
        System.out.println(new String(array,CharsetUtil.UTF_8));
        System.out.println((char) clientMsg.getByte(0));
        System.out.println(clientMsg.arrayOffset());
        System.out.println(clientMsg.readerIndex());
        System.out.println(clientMsg.readByte());
        System.out.println(clientMsg.readerIndex());
        System.out.println(clientMsg.getCharSequence(0, 4, CharsetUtil.UTF_8));
        System.out.println(clientMsg.getCharSequence(3, 2, CharsetUtil.UTF_8));
    }
}
