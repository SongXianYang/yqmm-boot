/*******************************************************************************
 * Package: com.example.design.chain
 * Type:    DepartmentManager
 * Date:    2025/3/10 16:36
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.chain;

/**
 * 功能描述：具体处理者：部门经理
 *
 * @author Songxianyang
 * @date 2025/3/10 16:36
 */
public class DepartmentManager extends ApproveHandler{
    /**
     * 具体的处理逻辑方法
     * @param name 那个用户
     * @param amount 报销金额
     */
    @Override
    public void result(String name, int amount) {
        if (amount <= 1000) {
            System.out.printf("[部门经理] 审批通过 %s 的 %d 元报销%n",
                    name,amount);
        } else if (next  != null) {
            next.result(name, amount);
        }
    }
}
