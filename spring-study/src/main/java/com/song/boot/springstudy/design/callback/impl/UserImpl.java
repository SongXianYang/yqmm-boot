/*******************************************************************************
 * Package: com.song.boot.springstudy.design.callback.impl
 * Type:    UserImpl
 * Date:    2024-04-18 11:49
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.callback.impl;

import com.song.boot.springstudy.design.builder.Cat;
import com.song.boot.springstudy.design.builder.CatBuilder;
import com.song.boot.springstudy.design.callback.MyCallback;
import com.song.boot.springstudy.design.callback.User;

import java.util.Objects;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-04-18 11:49
 */
public class UserImpl implements User {
    @Override
    public void insert(Cat cat, MyCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // 模拟异步操作
                if (Objects.nonNull(cat)) {
                    System.out.println("insert----success");
                    callback.onSuccess();
                    return;
                }
                System.out.println("insert----error");
                callback.onFailure();
            }
        }).start();

    }
}
