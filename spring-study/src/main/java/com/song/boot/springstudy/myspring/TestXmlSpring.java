/*******************************************************************************
 * Package: com.song.boot.springstudy.myspring
 * Type:    TestXmlSpring
 * Date:    2022-05-13 15:35
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.myspring;

import com.song.boot.springstudy.myspring.bean.CatBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 功能描述：学习  Spring 源码
 *
 * @author Songxianyang
 * @date 2022-05-13 15:35
 */
public class TestXmlSpring {
    
    public static void main(String[] args) {
        AbstractXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "spring-config.xml");
        
        CatBean cat = (CatBean) context.getBean("catBean");
        System.out.println(cat.getName());

    }
}
