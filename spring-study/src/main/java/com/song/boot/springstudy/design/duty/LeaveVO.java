/*******************************************************************************
 * Package: com.song.boot.springstudy.design.duty
 * Type:    Leave
 * Date:    2022-12-30 21:42
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.duty;

import lombok.Data;

/**
 * 功能描述： 请假对象
 *
 * @author Songxianyang
 * @date 2022-12-30 21:42
 */
@Data
public class LeaveVO {
    private String name;
    private Integer age;
    private Integer days;
    private String reason;

    public LeaveVO(String name, Integer age, Integer days, String reason) {
        this.name = name;
        this.age = age;
        this.days = days;
        this.reason = reason;
    }
}
