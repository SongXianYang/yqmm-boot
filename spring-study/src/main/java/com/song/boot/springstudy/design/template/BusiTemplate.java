/*******************************************************************************
 * Package: com.song.boot.springstudy.template
 * Type:    BusiTemplate
 * Date:    2023-01-02 16:35
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.template;

import lombok.Getter;
import lombok.Setter;

/**
 * 功能描述： 模板模式
 * 帮助文档：
 *      https://blog.csdn.net/qq_34162294/article/details/105324424
 *      https://juejin.cn/post/7030601365013397511#heading-6
 *      https://juejin.cn/post/6844903520844382215#heading-8
 *      https://juejin.cn/post/7023536216138055716
 * 设计规则：
 *  必须是一个抽象类
 *  改方法必须由final修饰，防止篡改。指定一些相同的行为逻辑
 *  其他方法为抽象方法，交给子类实现。
 *扩展：
 *  可以设置一个钩子方法来控制 抽象方法的使用
 *Spring 中 模板模式 的使用
 * AbstractPlatformTransactionManager  模板方法：commit、getTransaction
 *Jdk中
 *  java.io.InputStream，java.io.OutputStream，java.io.Reader，java.io.Writer中的非抽象方法。
 *  java.util.AbstractList，java.util.AbstractSet，java.util.AbstractMap中的非抽象方法。
 *
 * @author Songxianyang
 * @date 2023-01-02 16:35
 */
public abstract class BusiTemplate {
    @Getter
    @Setter
    // 钩子 是否登录
    private Boolean control = true;

    //抽象方法
    public abstract void initialize();
    abstract void startPlay();
    abstract void endPlay();

    //模板方法
    public final void play(){
        if (control) {
            //初始化游戏
            initialize();

            //开始游戏
            startPlay();

            //结束游戏
            endPlay();
        } else {
            System.out.println("用户未登录，无法开始游戏！");
        }
    }

}
