/*******************************************************************************
 * Package: com.song.boot.springstudy.design.builder
 * Type:    CatBuilder
 * Date:    2024-04-18 10:04
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.builder;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-04-18 10:04
 */
public class CatBuilder {
    private String name;
    private Integer age;
    private Long id;

    public CatBuilder nameBuild(String name) {
        this.name = name;
        return this;
    }

    public CatBuilder ageBuild(Integer age) {
        this.age = age;
        return this;
    }

    public CatBuilder idBuild(Long id) {
        this.id = id;
        return this;
    }

    public Cat build() {
        // 在构建返回对象的时候  提前校验
        if (id == null) {
            id=99L;
        }
        Cat cat = new Cat(name, age, id);
        return cat;
    }
}
