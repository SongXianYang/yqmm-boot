/*******************************************************************************
 * Package: com.song.boot.springstudy.design.observer
 * Type:    Observable
 * Date:    2023-01-05 21:41
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.observer;

/**
 * 功能描述： 抽象：被观察者--> 发布者
 *      理解成：微信公众号
 *
 * @author Songxianyang
 * @date 2023-01-05 21:41
 */
public interface MyObservable {
    // 添加观察者
    void add(MyObserver observer);
    // 删除观察者
    void delete(MyObserver observer);
    // 群发信息
    void send();

}
