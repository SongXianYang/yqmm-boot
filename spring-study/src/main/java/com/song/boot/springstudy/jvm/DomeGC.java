/*******************************************************************************
 * Package: com.song.boot.springstudy.jvm
 * Type:    DomeGC
 * Date:    2022-11-26 21:22
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.jvm;

import com.song.boot.springstudy.entity.UserEntity;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-11-26 21:22
 */
public class DomeGC {
    public static void main(String[] args) {
        System.out.println("打印GC详情：-XX:+PrintGCDetails");

    }
}
