package com.song.boot.springstudy.thread.pool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

/**
 * 数据量大的时候可以使用并行流
 * parallelStream
 *
 */
public class ParallelStreamTest {

    public static void main(String[] args) {
        int num = 10000000;
        List<Integer> list = new ArrayList<>(num);
        for (int i = 0; i < num; i++) {
            list.add(i);
        }

        TimeTool.check("", () -> {
            List<Integer> collect = list.stream()
                    .filter(item -> item > 100)
                    .collect(Collectors.toList());
        });


        /*
        使用并行流的时候：一定要自己创建一个：ForkJoinPool
        不然就会使用全局的公用一个：ForkJoinPool
         */
        ForkJoinPool forkJoinPool = new ForkJoinPool(20);

        TimeTool.check("", () -> {
            try {
                List<Integer> integers = forkJoinPool.submit(() ->
                        list
                                .parallelStream()
                                .filter(item -> item > 100)
                                .collect(Collectors.toList())
                ).get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        forkJoinPool.shutdown();
    }


}