package com.song.boot.springstudy.design.decorator;

// 具体组件：基础商品
class BaseProduct implements Product {
    private String name;
    private double price;

    public BaseProduct(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String getDescription() {
        return name;
    }
}