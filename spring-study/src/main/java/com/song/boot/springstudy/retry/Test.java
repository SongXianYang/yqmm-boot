/*******************************************************************************
 * Package: com.song.boot.springstudy.retry
 * Type:    Test
 * Date:    2022-05-24 21:17
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.retry;

import java.util.Scanner;
import java.util.jar.JarEntry;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-05-24 21:17
 */
public class Test {
    public static void main(String[] args) {
        double pow = Math.pow((1 + 0.05), 3);
        double i = 20 * pow;
        System.out.println("i="+i);
         int  a=6%3;
        System.out.println(a);
        
        int  b = 3/1;
        System.out.println(b);
        ////int integer = (int) b;
        ////System.out.println(integer);
        //Scanner scanner = new Scanner(System.in);
        //System.out.println("请输入一个数用于判断整数或小数");
        //double n = scanner.nextDouble();
        //String a1= n==(int)n?"是整数":"是小数";
        //System.out.println(n+a1);

    }
}
