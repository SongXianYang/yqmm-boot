/*******************************************************************************
 * Package: com.song.boot.springstudy.myspring.event
 * Type:    TestController
 * Date:    2024-09-27 14:33
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.myspring.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 功能描述：
 * 需求：业务代码解耦
 * 用户在新增修改的时候  记录他的操作
 * （发送短信  记录操作日志）等等
 * @author Songxianyang
 * @date 2024-09-27 14:33
 */
@Slf4j
@RestController
public class TestController {
        @Resource
        private ApplicationContext applicationContext;

        @GetMapping("/publishEvent")
        public  void  publishEvent()  {
                applicationContext.publishEvent(new  PersonEvent(new  Person("song"),  "edit"));
        }
}
