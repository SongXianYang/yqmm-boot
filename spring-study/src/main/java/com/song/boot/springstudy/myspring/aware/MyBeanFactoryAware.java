/*******************************************************************************
 * Package: com.song.boot.springstudy.myspring.aware
 * Type:    MyBeanFactoryAware
 * Date:    2024-01-31 17:01
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.myspring.aware;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

/**
 * 功能描述：
 *通过实现该接口，Bean实例可以获取到BeanFactory对象的引用，从而可以在运行时获取Bean的定义信息和其他Bean实例
 * @author Songxianyang
 * @date 2024-01-31 17:01
 */
public class MyBeanFactoryAware  implements BeanFactoryAware {
    /**
     * Callback that supplies the owning factory to a bean instance.
     * <p>Invoked after the population of normal bean properties
     * but before an initialization callback such as
     * {@link InitializingBean#afterPropertiesSet()} or a custom init-method.
     *
     * @param beanFactory owning BeanFactory (never {@code null}).
     *                    The bean can immediately call methods on the factory.
     * @throws BeansException in case of initialization errors
     * @see BeanInitializationException
     */
    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {

    }
}
