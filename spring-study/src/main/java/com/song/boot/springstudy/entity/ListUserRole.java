/*******************************************************************************
 * Package: com.song.boot.springstudy.entity
 * Type:    ListUserRole
 * Date:    2022-01-20 19:22
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-01-20 19:22
 */
@Data
public class ListUserRole implements Serializable {
    private static final long serialVersionUID = 5794644018863081137L;
    private Integer id;
}
