/*******************************************************************************
 * Package: com.song.boot.springstudy.thread.lock
 * Type:    ReentrantLockCinema
 * Date:    2022-06-04 21:47
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.thread.lock;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 功能描述：电影院 ReentrantLockCinema
 * 可以提高代码的封装性
 * @author Songxianyang
 * @date 2022-06-04 21:47
 */
@Slf4j
public class ReentrantLockCinema {
    private static ReentrantLock lock = new ReentrantLock();
    
    public static void main(String[] args) {
        
        new Thread(() -> new ReentrantLockCinema().cinema(1)).start();
        new Thread(() -> new ReentrantLockCinema().cinema(2)).start();
        new Thread(() -> new ReentrantLockCinema().cinema(3)).start();
        new Thread(() -> new ReentrantLockCinema().cinema(4)).start();
        
    }
    
    /**
     * 代码的封装业务
     */
    private void cinema(int i) {
        lock.lock();
        try {
            // 具体的业务
            System.out.println("具体的业务在try里面写-执行了哈哈哈" + i + "线程编号  " + Thread.currentThread().getName());
            Thread.sleep(1000);
            System.out.println("成功预定座位号：" + i +"祝您观看愉快！！");
            System.out.println("-------------");
        } catch (InterruptedException e) {
            e.printStackTrace();
            log.info("cinema: "+e);
        } finally {
            lock.unlock();
        }
    }
}
