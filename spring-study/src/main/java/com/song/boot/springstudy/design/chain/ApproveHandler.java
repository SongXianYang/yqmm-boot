/*******************************************************************************
 * Package: com.example.design.chain
 * Type:    ApproveHandler
 * Date:    2025/3/10 16:29
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.chain;

/**
 * 功能描述：
 *抽象审批处理器
 * @author Songxianyang
 * @date 2025/3/10 16:29
 */
public abstract class ApproveHandler {
    // 下一个处理器
    protected ApproveHandler next;

    // 设置下一个处理
    public ApproveHandler setNext(ApproveHandler handler) {
        next = handler;
        return next;
    }



    /**
     * 具体的处理逻辑方法
     * @param name 那个用户
     * @param amount 报销金额
     */
    public abstract void result(String name, int amount);
}
