package com.song.boot.springstudy.design.adapter;

// 现有支付宝实现（已兼容目标接口）
public class AlipayProcessor implements PaymentProcessor {
    @Override
    public boolean processPayment(String account, double amount) {
        System.out.printf("[支付宝] 向 %s 账户支付 %.2f 元成功%n", account, amount);
        return true;
    }
}