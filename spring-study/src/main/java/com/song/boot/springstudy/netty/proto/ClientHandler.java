/*******************************************************************************
 * Package: com.song.boot.springstudy.marshall
 * Type:    ClientHandler
 * Date:    2024-01-28 16:30
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.proto;

import com.song.boot.springstudy.netty.marshall.ReqData;
import com.song.boot.springstudy.netty.marshall.RespData;
import com.song.boot.springstudy.netty.proto.protoDoMain.CatInfo;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import io.netty.util.ReferenceCountUtil;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-01-28 16:30
 */
public class ClientHandler extends ChannelInboundHandlerAdapter {


    // 当通道就绪时，会触发  往服务器发送数据
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("client ctx=-------------"+ctx);
        // 往服务器发送数据
        for (int i = 0, size = 50; i < size; i++) {
            CatInfo.CatMsg.Builder rd = CatInfo.CatMsg.newBuilder();
            rd.setId(i);
            rd.setName("我是消息"+i);
            rd.setAge(i+1);
            ctx.writeAndFlush(rd);
        }
        System.out.println("消息发送完毕");
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        try{
            ByteBuf buf = (ByteBuf) msg;
            System.out.println("收到服务器 "+" 回复的ACK消息>>>>>>>>"+buf.toString(CharsetUtil.UTF_8));
        }finally {
            ReferenceCountUtil.release(msg);
        }
    }
}
