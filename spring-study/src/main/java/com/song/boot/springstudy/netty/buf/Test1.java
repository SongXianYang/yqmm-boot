/*******************************************************************************
 * Package: com.song.boot.springstudy.netty.buf
 * Type:    Test1
 * Date:    2023-06-22 15:44
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.buf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.util.CharsetUtil;

/**
 * 功能描述：   Unpooled.buffer(10)
 *
 * 已读完区域-----》待读区-----》写入区-----》已满区
 *
 * @author Songxianyang
 * @date 2023-06-22 15:44
 */
public class Test1 {
    public static void main(String[] args) {
        ByteBuf buffer = Unpooled.buffer(10);
        // write   会改变writerIndex
        for (int i = 0, size = 10; i < size; i++) {
            buffer.writeByte(i);
        }
        // read   readByte  会改变readerIndex
        for (int i = 0, size = buffer.capacity(); i < size; i++) {
            System.out.println(buffer.readByte());
        }
        System.out.println("不会改变readerIndex");
        // read   索引  不会改变readerIndex
        for (int i = 0, size = buffer.capacity(); i < size; i++) {
            System.out.println(buffer.getByte(i));
        }

    }
}
