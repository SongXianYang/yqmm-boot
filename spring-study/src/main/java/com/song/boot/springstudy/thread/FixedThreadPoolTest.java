/*******************************************************************************
 * Package: com.song.boot.springstudy.thread
 * Type:    FixedThreadPoolTest
 * Date:    2022-02-05 17:44
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.thread;

import org.springframework.stereotype.Service;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-02-05 17:44
 */
public class FixedThreadPoolTest {
    public static void main(String[] args) {
        //Executors.newSingleThreadExecutor();
        //Executors.newCachedThreadPool();
        //Executors.newScheduledThreadPool(10);
        //Executors.newFixedThreadPool(2);
        ExecutorService threadPool = Executors.newFixedThreadPool(4);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("需要一个runnable");
            }
        };
        for (int i = 0, size = Integer.MAX_VALUE; i < size; i++) {
            //向线程池种提交任务
            threadPool.execute(
                    //这个地方的代码等同与上面的runnable 实现类 Java8新特性
                    ()->{
                        try {
                            Thread.sleep(9999999);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.println("需要一个runnable");
                    });
        }
    }
}
