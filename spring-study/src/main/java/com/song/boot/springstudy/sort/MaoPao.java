/*******************************************************************************
 * Package: com.song.boot.springstudy.sort
 * Type:    MaoPao
 * Date:    2025-03-06 22:49
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.sort;

/**
 * 功能描述： 两两交换
 *
 * @author Songxianyang
 * @date 2025-03-06 22:49
 */
public class MaoPao {
    public static void main(String[] args) {
        int[] is = {8, 3, 9, 2, 1, 6, 5};


        for (int j = 0; j < is.length-1; j++) {

            for (int i = 0; i < is.length-1; i++) {
                int a1 = is[i];
                int a2 = is[i+1];
                if (a1 > a2) {
                    is[i]=a2;
                    is[i+1]=a1;
                }
            }
        }

        for (int i : is) {
            System.out.println(i);

        }
    }
}
