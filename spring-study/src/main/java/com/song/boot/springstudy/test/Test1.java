/*******************************************************************************
 * Package: com.song.boot.springstudy
 * Type:    Test1
 * Date:    2022-06-13 16:42
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

import com.song.boot.springstudy.entity.UserEntity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-06-13 16:42
 */
public class Test1 {
    public static void main(String[] args) {
        UserEntity user = new UserEntity();
        user.setId(1);
        user.setName("张扬");
        user.setType("VIP");
        for (String s : getFieldName(user)) {
            
            System.out.println(s);
        }
        System.out.println(Arrays.asList(getFieldName(user)));
    
        System.out.println(getFieldName1(user));
    }
    
    /**
     * 获取属性名数组
     *
     *  */
    private static String[] getFieldName(Object o) {
        Field[] fields = o.getClass().getDeclaredFields();
        String[] fieldNames = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            fieldNames[i] = fields[i].getName();
        }
        return fieldNames;
    }
    
    private static List<String> getFieldName1(Object o) {
        Field[] fields = o.getClass().getDeclaredFields();
        List<String> fieldsList = new ArrayList<>();
        for (int i = 0; i < fields.length; i++) {
            fieldsList.add(fields[i].getName());
        }
        return fieldsList;
    }
    
}
