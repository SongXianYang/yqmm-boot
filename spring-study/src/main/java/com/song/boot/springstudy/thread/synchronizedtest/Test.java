/*******************************************************************************
 * Package: com.song.boot.springstudy.thread.synchronizedtest
 * Type:    Test
 * Date:    2025-02-26 22:07
 *
 * Copyright (c) 2025 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.thread.synchronizedtest;

import org.openjdk.jol.info.ClassLayout;

/**
 * 功能描述：锁升级过程
 *锁升级不可逆：
 * 一旦锁升级为重量级锁，后续所有线程获取该锁时都会直接进入重量级状态。即使后续没有竞争，锁也不会降级。因此，主线程、t1和t2的同步块中打印的锁状态均为重量级。
 * @author Songxianyang
 * @date 2025-02-26 22:07
 */
public class Test {
    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(5000);
        Object o = new Object();
        // System.out.println(ClassLayout.parseInstance(o).toPrintable());

        // new Thread(() -> {
        //
        //     synchronized (o){
        //         //t1  - 重量级锁嘞
        //         System.out.println("t1:" + ClassLayout.parseInstance(o).toPrintable());
        //     }
        // }).start();

        new Thread(() -> {

            synchronized (o){
                //t1  - 重量级锁嘞
                System.out.println("t2:" + ClassLayout.parseInstance(o).toPrintable());
            }
        }).start();
        //main - 偏向锁 - 轻量级锁CAS - 重量级锁
        synchronized (o){
            System.out.println("main:" + ClassLayout.parseInstance(o).toPrintable());
        }
    }
}
