/*******************************************************************************
 * Package: com.song.boot.springstudy.marshall
 * Type:    Server
 * Date:    2024-01-28 16:09
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.marshall;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;


/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-01-28 16:09
 */
public class Server {
    public static void main(String[] args) throws Exception {
        EventLoopGroup bossEvent = new NioEventLoopGroup(1);
        EventLoopGroup workerEvent = new NioEventLoopGroup();

        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(bossEvent, workerEvent)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG,128) // 设置线程队列
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        socketChannel.pipeline().addLast(MarshallingCodec.buildMarshallingDecoder());
                        socketChannel.pipeline().addLast(MarshallingCodec.buildMarshallingEncoder());
                        socketChannel.pipeline().addLast(new ServerHandler()); // 自定义的server处理器
                    }
                });
        // 与Client 建立连接
        ChannelFuture sync = serverBootstrap.bind(9999).sync();
        sync.channel().closeFuture().sync();

        bossEvent.shutdownGracefully();  // 关闭
        workerEvent.shutdownGracefully();
    }
}
