package com.song.boot.springstudy.design.callback;

/**
 * 回调函数
 */
public interface MyCallback  {

	void onSuccess();
	
	void onFailure();
	
}