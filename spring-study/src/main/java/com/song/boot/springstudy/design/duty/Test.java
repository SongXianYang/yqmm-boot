/*******************************************************************************
 * Package: com.song.boot.springstudy.design.duty
 * Type:    Test
 * Date:    2022-12-30 22:39
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.duty;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-12-30 22:39
 */
@RestController
@RequestMapping("check")
public class Test {
    @Resource
    private PackCheck packCheck;

    @GetMapping("get")
    public String mainTest() {
        LeaveVO vo = new LeaveVO("sxy", 12, 7, "");
        packCheck.runCheck(vo);
        System.out.println("校验完后，再走主逻辑");
        System.out.println("-----复杂写法end-----");
        return "复杂写法";
    }

    @GetMapping("for")
    public String mainFor() {
        LeaveVO vo = new LeaveVO("sxy", 12, 3, "");
        packCheck.runCheckFor(vo);
        System.out.println("校验完后，再走主逻辑");
        System.out.println("-----优化写法end-----");
        return "优化写法";
    }

    @GetMapping("old")
    public String mainOld() {
        LeaveVO vo = new LeaveVO("sxy", 12, 11, "");
        packCheck.runCheckTest(vo);
        System.out.println("校验完后，再走主逻辑");
        System.out.println("-----老写法end-----");
        return "原始写法";
    }
}
