/*******************************************************************************
 * Package: com.song.boot.springstudy.thread
 * Type:    Join
 * Date:    2024-02-05 14:21
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.thread;


/**
 * 功能描述：join 等待线程终止 才会继续往下执行
 *https://blog.csdn.net/m0_55755339/article/details/131573485
 * https://blog.csdn.net/csdn_aiyang/article/details/126468579
 * @author Songxianyang
 * @date 2024-02-05 14:21
 */
public class Join {
    public static void main(String[] args) throws Exception {
        //Thread thread = new Thread(() -> {
        //    try {
        //        TimeUnit.SECONDS.sleep(10L);
        //    } catch (InterruptedException e) {
        //        e.printStackTrace();
        //    }
        //    System.out.println("song");
        //});
        //thread.start();
        //thread.join();

        Test a = new Test("A");
        Test b = new Test("B");
        Test c = new Test("C");
        Test d = new Test("D");
        a.start();
        a.join();
        System.out.println(a.getState());
        b.start();
        b.join();
        c.start();
        c.join();
        d.start();
        d.join();

        System.out.println("主线程！！");
    }
}

class Test extends Thread {
    public String nameThread;

    public Test(String name) {
        this.nameThread = name;
    }

    @Override
    public void run() {
        System.out.println("线程结果：：" + nameThread + "当前线程id为：：" + Thread.currentThread().getId());
    }
}
