package com.song.boot.springstudy.design.decorator;

// 客户端使用
public class EcommerceSystem {
    public static void main(String[] args) {
        // 基础商品
        Product smartphone = new BaseProduct("智能手机", 5000.0);
        
        // 添加附加服务
        Product decoratedProduct = new GiftWrapDecorator(
            new ExpressShippingDecorator(
                new ExtendedWarrantyDecorator(smartphone)
            )
        );

        System.out.println("商品描述: " + decoratedProduct.getDescription());
        System.out.println("总价格: ￥" + decoratedProduct.getPrice());
    }
}