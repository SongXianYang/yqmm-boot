# [【已满足】企业级开发的flowable工作流平台-学习借鉴-集成分布式项目都非常方便SpringBoot+swagger+mybatis plus+mysql](https://blog.csdn.net/weixin_48278764/article/details/122800320)
# [Flowable多实例会签功能来袭](https://blog.csdn.net/weixin_48278764/article/details/126920927)
# [Flowable多实例加签减签功能正式上线](https://blog.csdn.net/weixin_48278764/article/details/126920496)
# [退回](https://gitee.com/SongXianYang/yqmm-boot/blob/master/yqmm-flowable/src/main/resources/%E9%80%80%E5%9B%9E/Return.md)
# 流程模型
- [X] 列表分页
- [X] 获得模型
- [X] 修改模型部署的流程定义状态
- [X] 删除模型
- [X] 添加流程模型
- [X] 导出流程模型的XML
- [X] 修改流程模型
- [X] 导入模型
- [X] 部署模型模型
# 流程定义
- [X] 列表分页
- [X] 获得流程定义的xml
- [X] 启动流程
- [X] 查询所有启动的流程列表
- [X] 挂起流程定义(停止、暂停）或（唤醒、激活）被挂起的流程定义

# 流程实例
- [X] 获得指定流程实例
- [X] 终止流程(撤回)
- [X] 批量删除流程实例
- [X] 根据流程实例id查询当前历史流程记录
- [X] 挂起流程实例(停止、暂停）或（唤醒、激活）被挂起的流程实例
- [X] 判断传入流程实例在运行中是否存在
- [X] 查看流程实例列表

# 流程任务
- [X] 更新任务的负责人(转办或者转派负责人)
- [X] 查询可退回的节点列表
- [X] 委派
- [X] 单独拾取-领取-任务
- [X] 单独完成任务
- [X] 任务归还
- [X] 任务交接
- [X] 用户已办
- [X] 用户待办
- [X] 拾取任务并完成(通过)
- [X] 拒绝
- [X] 多实例加签
- [X] 多实例减签
- [X] 查询会签节点的审批人任务
- [X] 退回
- [X] 流程图
- [X] 获取指定用户组流程任务列表
- [X] 驳回流程-暂不使用
- [X] 获取当前任务的流程变量

# 监听器
- [X] 任务监听器
- [X] 执行监听器

##### 如果觉得对您有帮助，希望能请SteveCode喝杯奶茶丫！嘿嘿嘿，谢谢！
![img_3.png](img_3.png)
# 持续优化~~~~
![img.png](img_4.png)

![img_1.png](img_5.png)

![img_2.png](img_6.png)

![img_211.png](img_7.png)