/*******************************************************************************
 * Package: com.song.es.dto
 * Type:    DogDto
 * Date:    2024-01-10 21:42
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.es.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-01-10 21:42
 */
@Data
@AllArgsConstructor
public class DogDto {
    private String id;
    private String name;
}
