/*******************************************************************************
 * Package: com.song.rabbit.mq.web
 * Type:    MqController
 * Date:    2023-05-14 18:52
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.rabbit.mq.web;

import com.song.rabbit.mq.entity.Cat;
import com.song.rabbit.mq.entity.Dog;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Date;
import java.util.UUID;

/**
 * 功能描述： Mq管理
 *
 * @author Songxianyang
 * @date 2023-05-14 18:52
 */
@Slf4j
@RestController
@RequestMapping("mq-web")
public class MqController {
    @Autowired
    private  RabbitTemplate rabbitTemplate;
    @Autowired
    private  AmqpAdmin amqpAdmin;

    /**
     * 发送消息
     *
     * @return
     */
    @GetMapping("send")
    public String send() {
        Cat cat = new Cat(1, "大猫", new Date());
        Cat cat2 = new Cat(2, "大猫2", new Date());
        // 发送消息
        // String exchange, String routingKey, Object message, CorrelationData 确定这一条消息
        rabbitTemplate.convertAndSend("yqmm.rabbit.mq.java.exchange", "song.java", cat, new CorrelationData(UUID.randomUUID().toString()));
        rabbitTemplate.convertAndSend("yqmm.rabbit.mq.java.exchange", "song.java", cat2, new CorrelationData(UUID.randomUUID().toString()));
        Dog dog = new Dog(1, "大狗", new Date());
        rabbitTemplate.convertAndSend("yqmm.rabbit.mq.java.exchange", "song.java", dog, new CorrelationData(UUID.randomUUID().toString()));
        return "成功";
    }

    /**
     * 发送单条消息
     * @return
     */
    @GetMapping("send-1/{id}")
    public String send1(@PathVariable("id") Integer id) {
        Dog dog = new Dog(id, "大狗11", new Date());
        rabbitTemplate.convertAndSend("yqmm.rabbit.mq.java.exchange", "song.java", dog, new CorrelationData(String.valueOf(id)));
        return "成功";
    }

    /**
     * 创建交换机
     */
    @GetMapping("create-exchange")
    public String createExchange(@RequestParam String exchangeName) {
        DirectExchange directExchange = new DirectExchange(exchangeName, true, false);
        amqpAdmin.declareExchange(directExchange);
        return exchangeName + "创建交换机成功！";
    }
    /**
     * 创建队列
     */
    @GetMapping("create-queue")
    public String createQueue(@RequestParam String queueName) {
        Queue queue = new Queue(queueName, true, false, false);
        amqpAdmin.declareQueue(queue);
        return queueName + "创建队列成功！";
    }

    /**
     * 绑定队列和交换机
     * @param queueName
     * @return
     */
    @GetMapping("binding")
    public String binding(@RequestParam String exchangeName,@RequestParam String queueName) {
        //String 目的地, DestinationType 目标类型:队列, String 交换机名称, String 路由key,
        //@Nullable Map<String, Object> 各种参数
        // 简单的点对点
        Binding binding = new Binding(queueName, Binding.DestinationType.QUEUE,
                exchangeName, "song.java",
                null);
        amqpAdmin.declareBinding(binding);
        return queueName + "绑定成功！";
    }
}
