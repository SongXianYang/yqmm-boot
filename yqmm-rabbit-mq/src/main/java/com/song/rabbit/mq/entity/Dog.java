/*******************************************************************************
 * Package: com.song.rabbit.mq.entity
 * Type:    Cat
 * Date:    2023-05-14 17:59
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.rabbit.mq.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 功能描述： 实体对象
 *
 * @author Songxianyang
 * @date 2023-05-14 17:59
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Dog implements Serializable {
    private static final long serialVersionUID = 3859970005889783471L;
    private Integer id;
    private String name;
    private Date time;
}
