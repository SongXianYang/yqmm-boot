package com.song.rulesobj;


import lombok.Data;

@Data
public class Customer {
    /**
     * 客户类型
     */
    private String type;

    /**
     * 客户订单价格
     */
    private Double  originalPrice; // 订单原始价格，即优惠前的价格

    /**
     * 优惠后最终结算价格
     */
    private Double discount;
}
