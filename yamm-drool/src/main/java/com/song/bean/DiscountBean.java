/*******************************************************************************
 * Package: com.song.bean
 * Type:    DiscountService
 * Date:    2024-06-28 13:45
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.bean;

import com.song.rulesobj.Customer;
import org.kie.api.runtime.KieSession;

/**
 * 功能描述： 规则处理器封装
 *
 * @author Songxianyang
 * @date 2024-06-28 13:45
 */
public class DiscountBean {

    private KieSession kieSession;

    public DiscountBean(KieSession kieSession) {
        this.kieSession = kieSession;
    }

    public void applyDiscount(Customer customer) {
        kieSession.insert(customer); // 插入客户对象到规则引擎中
        kieSession.fireAllRules(); // 执行规则
        // 客户对象已经被更新，包含了计算出的折扣
        System.out.println("客户订单价格"+customer.getOriginalPrice()+"客户折扣类型: " + customer.getType() + ", 优惠后最终结算价格: " + customer.getDiscount());
    }


}
