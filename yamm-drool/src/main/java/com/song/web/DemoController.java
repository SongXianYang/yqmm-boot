package com.song.web;

import com.song.bean.DiscountBean;
import com.song.common.annotation.ResponseInfoSkin;
import com.song.rulesobj.Customer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "规则引擎")
@ResponseInfoSkin
public class DemoController {

    @Autowired
    private DiscountBean discountBean;

    @PostMapping("/discount")
    @ApiOperation("打折")
    public Double discount(@RequestBody Customer customer) {
        // 规则处理器封装
        discountBean.applyDiscount(customer);
        return customer.getDiscount();
    }
}
