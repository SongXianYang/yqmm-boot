package com.song;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 规则引擎
 * https://blog.csdn.net/baidu_35468322/article/details/120936410
 */
@SpringBootApplication
public class YqmmDroolsApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(YqmmDroolsApplication.class, args);
    }
    
}