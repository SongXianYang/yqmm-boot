package com.song.bigdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * https://developer.aliyun.com/article/952933
 */
@SpringBootApplication
public class SteveBigDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(SteveBigDataApplication.class, args);
    }

}
