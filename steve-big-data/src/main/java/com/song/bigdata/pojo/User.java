/*******************************************************************************
 * Package: com.song.bigdata.stream
 * Type:    User
 * Date:    2022-10-26 22:48
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.bigdata.pojo;

/**
 * 功能描述：  必须都是：public的
 *
 * @author Songxianyang
 * @date 2022-10-26 22:48
 */
public class User {
    public String name;
    public Integer age;

    public User() {
    }

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
