package com.song.bigdata.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Order {
    private String userId;      // 用户ID
    private String orderId;     // 订单ID
    private double amount;      // 订单金额
    private String timestamp;    // 订单时间戳
}
