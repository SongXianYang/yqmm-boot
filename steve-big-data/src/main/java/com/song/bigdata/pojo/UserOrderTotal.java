package com.song.bigdata.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserOrderTotal {
    private String userId;        // 用户ID
    private double totalAmount;   // 累计订单金额
    private String lastOrderTime;  // 最近订单时间
}
