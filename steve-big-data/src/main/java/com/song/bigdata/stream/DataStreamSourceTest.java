/*******************************************************************************
 * Package: com.song.bigdata.stream
 * Type:    DataStreamSourceTest
 * Date:    2022-10-26 22:52
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.bigdata.stream;

import com.song.bigdata.pojo.User;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-10-26 22:52
 */
public class DataStreamSourceTest {
    public static void main(String[] args) throws Exception{
        // 创建环境
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        environment.setRestartStrategy(RestartStrategies.fixedDelayRestart(
                3, // 尝试重启的次数
                Time.of(10, TimeUnit.SECONDS) // 间隔
        ));
        // 并行度
        environment.setParallelism(1);
        //DataStreamSource<String> streamSource = environment.readTextFile("\\file\\user.txt");
        //streamSource.print();
        List<User> users = new ArrayList<>();
        users.add(new User("song", 19));
        users.add(new User("xian",19));
        DataStreamSource<User> listUserStream = environment.fromCollection(
                users
        );
        DataStreamSource<User> userDataStreamSource = environment.fromElements(
                new User("song", 19),
                new User("xian", 19)
        );
        userDataStreamSource.print("--->2");
        listUserStream.print("-->1");
        environment.execute();
    }
}
