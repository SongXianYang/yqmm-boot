/*******************************************************************************
 * Package: com.song.common.util
 * Type:    MD5Utils
 * Date:    2023-12-09 15:36
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.common.util;

import org.apache.tomcat.util.codec.binary.Base64;

import java.security.MessageDigest;

/**
 * 功能描述：MD5加密
 * 注册：密码加密
 * 登录：密码加密后与数据库对比！
 *
 * @author Songxianyang
 * @date 2023-12-09 15:36
 */
public class MD5Utils {

    /**
     *
     * @Title: MD5Utils.java
     * @Package com.study.utils
     * @Description: 对字符串进行md5加密
     */
    public static String getMD5Str(String strValue) throws Exception {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        String newstr = Base64.encodeBase64String(md5.digest(strValue.getBytes()));
        return newstr;
    }

    public static void main(String[] args) {
        try {
            String md5 = getMD5Str("root");
            System.out.println(md5);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
