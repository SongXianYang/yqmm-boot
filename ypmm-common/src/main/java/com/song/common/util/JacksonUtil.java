/*******************************************************************************
 * Package: com.hngtrust.activiti.commons
 * Type:    JacksonUtil
 * Date:    2022/1/6 11:23
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.common.util;


import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * TODO your comment
 *
 * @author Wujingtao
 * @date 2022/1/6 11:23
 */
public class JacksonUtil {
    private static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    public JacksonUtil() {
    }
    
    public static <T> List<T> jsonToListBean(String jsonStr, Class<T> cls) {
        List<Object> list = (List<Object>) JsonUtils.toObjectList(jsonStr,Object.class);
        List<T> lstRet = new ArrayList();
        Iterator var5 = list.iterator();
        
        while(var5.hasNext()) {
            Object obj = var5.next();
            if (obj instanceof Map) {
                lstRet.add(mapToBean((Map<String, Object>) obj, cls));
            }
        }
        
        return lstRet;
    }
    
    public static String[] jsonToStringArray(String jsonStr) {
        List<Object> lstTemp = jsonToList(jsonStr);
        return (String[])lstTemp.toArray(new String[lstTemp.size()]);
    }
    
    public static List<Object> jsonToList(String jsonStr) {
        List<Object> list = (List<Object>) JsonUtils.toObjectList(jsonStr,Object.class);
        return list;
    }
    
    public static <T> List<T> jsonToList(String jsonStr, Class<T> clazz) {
        return (List<T>) JsonUtils.toObjectList(jsonStr,Object.class);
    }
    
    public static List<Map<String, Object>> jsonToListMap(String jsonStr) {
        List<Map<String, Object>> retList = (List<Map<String, Object>>) JsonUtils.toObjectList(jsonStr,Map.class);
        return retList;
    }
    
    public static <T> T jsonToBean(String jsonStr, Class<T> cls) {
        Map<String, Object> obj = JsonUtils.toObject(jsonStr,Map.class);
        return mapToBean(obj, cls);
    }
    
    public static String mapToJson(Map<String, Object> mapParam) {
        return JsonUtils.toJson(mapParam);
    }
    
    public static String listToJson(Object mapParam) {
        return JsonUtils.toJson(mapParam);
    }
    
    public static Map<String, Object> jsonToMap(String jsonStr) {
        return JsonUtils.toObject(jsonStr,Map.class);
    }
    
    public static List<Map<String, Object>> jsonToMapArray(String[] jsonStrArray) {
        List<Map<String, Object>> list = new ArrayList();
        if (jsonStrArray != null && 0 < jsonStrArray.length) {
            String[] var2 = jsonStrArray;
            int var3 = jsonStrArray.length;
            
            for(int var4 = 0; var4 < var3; ++var4) {
                String jsonStr = var2[var4];
                list.add(jsonToMap(jsonStr));
            }
        }
        
        return list;
    }
    
    public static Object convertParam(Object inputObj, Class<?> paramType) {
        if (paramType.equals(inputObj.getClass())) {
            return inputObj;
        } else {
            String value = String.valueOf(inputObj);
            if (null == value || 0 == value.trim().length()) {
                return null;
            } else if (Date.class.equals(paramType)) {
                try {
                    return DATE_FORMATTER.parse(value);
                } catch (ParseException var4) {
                    var4.printStackTrace();
                    throw new RuntimeException("");
                }
            } else if (BigDecimal.class.equals(paramType)) {
                return new BigDecimal(value);
            } else if (!Integer.class.equals(paramType) && !"int".equals(paramType.getName())) {
                if (!Long.class.equals(paramType) && !"long".equals(paramType.getName())) {
                    if (!Double.class.equals(paramType) && !"double".equals(paramType.getName())) {
                        if (!Boolean.class.equals(paramType) && !"boolean".equals(paramType.getName())) {
                            return String.class.equals(paramType) ? value : value;
                        } else {
                            return Boolean.valueOf(value);
                        }
                    } else {
                        return Double.valueOf(value);
                    }
                } else {
                    return Long.valueOf(value);
                }
            } else {
                return Integer.valueOf(value);
            }
        }
    }
    
    public static List<Field> getAllFields(Class<?> inputClass) {
        List<Field> lstField = new ArrayList();
        
        for(Class tempClass = inputClass; tempClass != null; tempClass = tempClass.getSuperclass()) {
            Field[] fields = tempClass.getDeclaredFields();
            if (fields != null) {
                Field[] var4 = fields;
                int var5 = fields.length;
                
                for(int var6 = 0; var6 < var5; ++var6) {
                    Field field = var4[var6];
                    lstField.add(field);
                }
            }
        }
        
        return lstField;
    }
    
    private static <T> T mapToBean(Map<String, Object> map, Class<T> cls) {
        List<Field> fields = getAllFields(cls);
        Object obj = null;
        
        try {
            obj = cls.newInstance();
            Iterator var4 = fields.iterator();
            
            while(var4.hasNext()) {
                Field field = (Field)var4.next();
                if (map.get(field.getName()) != null) {
                    field.setAccessible(true);
                    field.set(obj, convertParam(map.get(field.getName()), field.getType()));
                }
            }
            
            return (T) obj;
        } catch (Exception var6) {
            throw new RuntimeException("");
        }
    }
}
