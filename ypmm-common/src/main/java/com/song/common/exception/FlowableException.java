/*******************************************************************************
 * Package: com.song.flowable.exception
 * Type:    FlowableException
 * Date:    2022-10-20 18:19
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.common.exception;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-10-20 18:19
 */
public class FlowableException extends ServiceException {

    public FlowableException(String message) {
        super(message);
    }
}
