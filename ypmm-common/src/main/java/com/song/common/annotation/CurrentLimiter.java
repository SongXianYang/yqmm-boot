/*******************************************************************************
 * Package: com.song.common.annotation
 * Type:    CurrentLimiterAspect
 * Date:    2024-01-07 14:39
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.common.annotation;

import java.lang.annotation.*;

/**
 * 功能描述： 基于redis的Lua限流注解
 *
 * @author Songxianyang
 * @date 2024-01-07 14:39
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CurrentLimiter {

    /**
     * 限流次数
     * @return
     */
    int limit();

    /**
     * 方法签名
     * @return
     */
    String methodKey() default "";

}
