/*******************************************************************************
 * Package: com.song.fafka.doMain
 * Type:    ProducerMsg
 * Date:    2023-12-28 22:01
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.fafka.doMain;

import lombok.Data;

import java.util.Date;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-12-28 22:01
 */
@Data
public class ProducerMsg {
    private Long id;    //id

    private String msg; //消息

    private Date sendTime;  //时间戳


}
