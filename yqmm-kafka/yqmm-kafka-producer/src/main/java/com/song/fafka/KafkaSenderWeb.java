/*******************************************************************************
 * Package: com.song.fafka
 * Type:    KafkaSender
 * Date:    2023-12-28 22:08
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.fafka;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.song.common.annotation.ResponseInfoSkin;
import com.song.fafka.doMain.ProducerMsg;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.UUID;

/**
 * 功能描述：kafka 消息生产者
 *
 * @author Songxianyang
 * @date 2023-12-28 22:08
 */
@Api(tags = "临时")
@Slf4j
@RestController
@ResponseInfoSkin
@RequestMapping("temp")
public class KafkaSenderWeb {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    private Gson gson = new GsonBuilder().create();

    //发送消息方法
    @ApiOperation("发送消息方法")
    @GetMapping("send")
    @ResponseBody
    public String send() {
        ProducerMsg message = new ProducerMsg();
        message.setId(System.currentTimeMillis());
        message.setMsg("老王头"+UUID.randomUUID().toString());
        message.setSendTime(new Date());
        log.info(">>>>>>>>>>>>>>>>发送消息数据 = {}<<<<<<<<<<<<<<<<<<<<<<", gson.toJson(message));
        kafkaTemplate.send("test1", gson.toJson(message));
        return "success";
    }
}
