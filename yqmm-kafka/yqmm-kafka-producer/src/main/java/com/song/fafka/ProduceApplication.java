package com.song.fafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * kafka 消息生产者
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.song.common","com.song.fafka"})
public class ProduceApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(ProduceApplication.class, args);
    }
    
}
