package com.song.flowable.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mybatis.core.base.ParentDO;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;



/**
 * 学生请假
 * @author SongXianYang
 */


@Data
@TableName("student_leave")
@ApiModel("学生请假")
public class StudentLeaveDO extends ParentDO {
	@TableId(type = IdType.ASSIGN_UUID)
	private String  id;
	/*
	 * 请假人
	 */
	@ApiModelProperty("请假人")
	private String name;
	/*
	 * 请假时间
	 */
	@ApiModelProperty("请假时间")
	private Date time;
	/*
	 * 天数
	 */
	@ApiModelProperty("天数")
	private Long days;
	/*
	 * 请假原因
	 */
	@ApiModelProperty("请假原因")
	private String reason;

}