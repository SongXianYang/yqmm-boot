/*******************************************************************************
 * Package: com.song.flowable.vo
 * Type:    ModelVO
 * Date:    2023-11-29 17:26
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 功能描述：流程模型VO
 * ModelConvert
 *
 * @author Songxianyang
 * @date 2023-11-29 17:26
 */
@ApiModel("流程模型VO")
@Data
public class ModelVO {

    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "名")
    private String name;
    @ApiModelProperty(value = "key")
    private String key;
    @ApiModelProperty(value = "版本")
    private int version;
    @ApiModelProperty(value = "创建人")
    private String createName;
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    @ApiModelProperty(value = "最后修改时间")
    private Date lastUpdateTime;
    @ApiModelProperty(value = "最后修改人")
    private String lastUpdatedName;
    @ApiModelProperty(value = "发布状态")
    private Integer publishState;
    /**
     * 查询当前xml
     */
    @ApiModelProperty(value = "查询当前xml")
    private String flowXml;
}
