/*******************************************************************************
 * Package: cn.surveyking.module.flowable.vo
 * Type:    DoneVO
 * Date:    2023-12-22 16:23
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 功能描述： 已办得返回参数
 *
 * @author Songxianyang
 * @date 2023-12-22 16:23
 */
@Data
@ApiModel("已办返参")
public class DoneVO {
    /**
     * 任务编号
     */
    @ApiModelProperty("任务编号")
    private String taskId;
    /**
     * 任务执行编号
     */
    @ApiModelProperty("任务执行编号")
    private String executionId;
    /**
     * 任务名称
     */
    @ApiModelProperty("任务名称")
    private String taskName;
    /**
     * 任务Key
     */
    @ApiModelProperty("任务Key")
    private String taskDefKey;
    /**
     * 任务执行人Id
     */
    @ApiModelProperty("任务执行人Id")
    private Long assigneeId;
    /**
     * 部门名称
     */
    @ApiModelProperty("部门名称")
    private String deptName;
    /**
     * 流程发起人部门名称
     */
    @ApiModelProperty("流程发起人部门名称")
    private String startDeptName;
    /**
     * 任务执行人名称
     */
    @ApiModelProperty("任务执行人名称")
    private String assigneeName;
    /**
     * 任务执行人部门
     */
    @ApiModelProperty("任务执行人部门")
    private String assigneeDeptName;
    ;
    /**
     * 流程发起人Id
     */
    @ApiModelProperty("流程发起人Id")
    private String startUserId;
    /**
     * 流程发起人名称
     */
    @ApiModelProperty("流程发起人名称")
    private String startUserName;
    /**
     * 流程类型
     */
    @ApiModelProperty("流程类型")
    private String category;
    /**
     * 流程变量信息
     */
    @ApiModelProperty("流程变量信息")
    private Object procVars;
    /**
     * 局部变量信息
     */
    @ApiModelProperty("局部变量信息")
    private Object taskLocalVars;
    /**
     * deployId
     */
    @ApiModelProperty("流程部署编号")
    private String deployId;
    /**
     * procDefId
     */
    @ApiModelProperty("流程ID")
    private String procDefId;
    /**
     * 流程key
     */
    @ApiModelProperty("流程key")
    private String procDefKey;
    /**
     * 流程定义名称
     */
    @ApiModelProperty("流程定义名称")
    private String procDefName;
    /**
     * 流程定义内置使用版本
     */
    @ApiModelProperty("流程定义内置使用版本")
    private int procDefVersion;
    /**
     * 流程实例ID
     */
    @ApiModelProperty("流程实例ID")
    private String procInsId;
    /**
     * 历史流程实例ID
     */
    @ApiModelProperty("历史流程实例ID")
    private String hisProcInsId;
    /**
     * 任务耗时
     */
    @ApiModelProperty("任务耗时")
    private String duration;
    /**
     * 任务意见
     */
    @ApiModelProperty("任务意见")
    private String comment;
    /**
     * 候选执行人
     */
    @ApiModelProperty("候选执行人")
    private String candidate;
    /**
     * 任务创建时间
     */
    @ApiModelProperty("任务创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 任务完成时间
     */
    @ApiModelProperty("任务完成时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date finishTime;


}
