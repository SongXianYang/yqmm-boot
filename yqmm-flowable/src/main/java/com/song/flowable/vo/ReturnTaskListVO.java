/*******************************************************************************
 * Package: com.song.flowable.vo
 * Type:    ReturnTaskListVO
 * Date:    2023-11-30 22:58
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.vo;

import io.swagger.annotations.ApiModel;
import liquibase.pro.packaged.S;
import lombok.Data;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-30 22:58
 */
@ApiModel("返回Task")
@Data
public class ReturnTaskListVO {
    private String nodeId;
    /**
     * 节点name
     */
    private String nodeName;
}
