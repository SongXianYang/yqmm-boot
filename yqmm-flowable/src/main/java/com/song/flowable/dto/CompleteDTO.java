/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    CompleteDTO
 * Date:    2023-12-15 10:50
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-12-15 10:50
 */
@Data
@ApiModel("单独完成任务")
public class CompleteDTO {
    /**
     * 任务id
     */
    @ApiModelProperty("任务id")
    private String taskId;

    /**
     * 流程中的变量 会签时也是这个变量
     */
    @ApiModelProperty("流程中的变量")
    private Map<String, Object> map;

    /**
     * 审批意见
     */
    @ApiModelProperty("审批意见")
    private String  message;
    /**
     * 流程实例id
     */
    @ApiModelProperty("流程实例id")
    private String  processInstanceId;
}
