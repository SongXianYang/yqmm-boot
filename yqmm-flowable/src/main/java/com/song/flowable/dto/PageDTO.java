/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    PageDTO
 * Date:    2022-09-18 16:38
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-09-18 16:38
 */
@Data
@ApiModel("分页对象")
public class PageDTO {
    /**
     * 那一页开始
     */
    @ApiModelProperty("那一页开始-例如1、2、3")
    private Integer page;

    /**
     * 每页显示条数
     */
    @ApiModelProperty("每页显示条数 例如10，100")
    private Integer pageSize;
}
