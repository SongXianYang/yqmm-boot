/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    DotoDTO
 * Date:    2022-02-05 23:38
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 功能描述：待办入参
 *
 * @author Songxianyang
 * @date 2022-02-05 23:38
 */

@ApiModel("待办入参")
@Data
public class TodoDTO extends PageDTO{

    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private String userId;

}
