/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    RejectDTO
 * Date:    2023-12-19 14:56
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-12-19 14:56
 */
@Data
@ApiModel("驳回入参")
public class RejectDTO {
    // 任务id
    @ApiModelProperty("任务id")
    private String taskId;
    @ApiModelProperty("任务意见")
    private String comment;

}
