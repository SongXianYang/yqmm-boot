/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    Done
 * Date:    2022-02-05 23:42
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 功能描述：已办对象
 *
 * @author Songxianyang
 * @date 2022-02-05 23:42
 */

@ApiModel("已办入参")
@Data
public class DoneDTO {
    /**
     * 那一页开始
     */
    @ApiModelProperty("那一页开始")
    private Integer page;
    
    /**
     * 每页显示条数
     */
    @ApiModelProperty("每页显示条数")

    private Integer pageSize;
    
    /**
     * 用户id
     */
    @ApiModelProperty("用户id")

    private String userId;

    /**
     * 是否终止：1终止0为终止
     */
    @ApiModelProperty("是否终止：1终止0为终止")
    private Integer ifEnd = 0;

}
