package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 流程定义分页入参
 */
@ApiModel("流程定义分页入参")
@Data
public class ProcessDefinitionPageDTO extends PageDTO {

    @ApiModelProperty("标识-精准匹配。定义流程的key")
    private String key;
}
