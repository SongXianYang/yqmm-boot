/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    FlowModelDTO
 * Date:    2023-12-10 15:26
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-12-10 15:26
 */
@Data
@ApiModel(value = "流程模型保存实体")
public class FlowModelDTO {
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "key")
    private String key;
    @ApiModelProperty(value = "描述")
    private String description;
}
