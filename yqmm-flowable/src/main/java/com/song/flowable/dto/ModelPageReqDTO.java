/*******************************************************************************
 * Package: com.song.flowable.vo
 * Type:    ModelPageReqVO
 * Date:    2023-11-27 21:03
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述：流程模型分页vo
 *
 * @author Songxianyang
 * @date 2023-11-27 21:03
 */
@ApiModel("查询历史活动信息")
@Data
public class ModelPageReqDTO extends PageDTO {
    @ApiModelProperty("标识-精准匹配")
    private String key;

    @ApiModelProperty("名字-模糊匹配")
    private String likeName;
}
