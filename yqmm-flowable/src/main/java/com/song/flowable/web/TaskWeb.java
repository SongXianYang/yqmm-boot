/*******************************************************************************
 * Package: com.song.flowable.web
 * Type:    TaskWeb
 * Date:    2023-11-28 22:38
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.web;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.song.flowable.convert.TaskConvert;
import com.song.flowable.dto.*;
import com.song.flowable.service.FlowTaskService;
import com.song.flowable.vo.DoneVO;
import com.song.flowable.vo.ReturnTaskListVO;
import com.song.flowable.vo.ReturnTaskVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-28 22:38
 */
@Api(tags = "流程任务")
@RestController
@RequestMapping("/flow/task")
@RequiredArgsConstructor
public class TaskWeb {
    private final FlowTaskService flowTaskService;

    /**
     * **转签\转办\转派负责人:**就是将任务的负责人直接设置为别人。即本来由自己办理，改为别人办理。
     *
     * @param taskId
     * @param targetUserId
     * @return
     */
    @GetMapping("/update-assignee")
    @ApiOperation("更新任务的负责人(转办或者转派负责人)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", value = "当前任务ID")
            ,
            @ApiImplicitParam(name = "targetUserId", value = "转办目标人")
    })
    public Boolean updateTaskAssignee(@RequestParam("taskId") String taskId, @RequestParam("targetUserId") String targetUserId) {
        return flowTaskService.updateTaskAssignee(taskId, targetUserId);

    }

    @GetMapping("/get-return-task-list")
    @ApiOperation("查询可退回的节点列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", value = "当前任务ID")
    })
    public List<ReturnTaskListVO> getReturnTaskList(@RequestParam("taskId") String taskId) {
        return flowTaskService.getReturnTaskList(taskId);
    }


    /**
     * **委派:**A由于某些原因不能处理该任务可以把任务委派给用户B代理，当B决绝完之后再次回到用户A这里，然后由A去完成任务，在这个过程中A是任务的所有者OWNER_，B是该任务的办理人Assignee。A->B->A
     *
     * @param taskId
     * @param assignUserId
     * @return
     */
    @GetMapping("/assign")
    @ApiOperation("委派")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", value = "当前任务ID"),
            @ApiImplicitParam(name = "assignUserId", value = "要委派的用户")
    })
    public Boolean assign(@RequestParam("taskId") String taskId, @RequestParam("assignUserId") String assignUserId) {
        return flowTaskService.assign(taskId, assignUserId);
    }


    /**
     * 拾取任务
     * 当一个任务被拾取后，其他用户是无法拾取该任务的。任务拾取成功后，就会变成个人任务。
     * 注意：即使不是任务的候选人也可以进行拾取任务，所以需要根据任务候选人去查询任务再去进行拾取
     * <p>
     * claim
     */
    @GetMapping("/claim")
    @ApiOperation("单独拾取-领取-任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", value = "当前任务ID"),
            @ApiImplicitParam(name = "assignUserId", value = "当前需要拾取的用户id")
    })
    public Boolean claim(@RequestParam("taskId") String taskId, @RequestParam("assignUserId") String claimUserId) {
        return flowTaskService.claim(taskId, claimUserId);
    }

    /**
     * 任务归还
     * 如果任务 拾取之后不想操作 或者误拾取任务也可以进行归还任务。
     *
     * @param taskId
     * @return
     */
    @GetMapping("/un-claim")
    @ApiOperation("任务归还")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", value = "当前任务ID")
    })
    public Boolean unClaim(@RequestParam("taskId") String taskId) {
        return flowTaskService.unClaim(taskId);
    }

    /**
     * handover
     * 任务交接
     * 如果任务拾取后，不想操作也不想归还，可以交接给他人进行处理 （转办、转派）
     */
    @GetMapping("/handover")
    @ApiOperation("任务交接")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", value = "当前任务ID"),
            @ApiImplicitParam(name = "handoverUserId", value = "需要交接给那个用户")
    })
    public Boolean handover(@RequestParam("taskId") String taskId, @RequestParam("handoverUserId") String handoverUserId) {
        return flowTaskService.handover(taskId, handoverUserId);
    }

    /**
     * complete
     * 单独完成任务
     */
    @PostMapping("/complete")
    @ApiOperation("单独完成任务")
    public Boolean complete(@RequestBody CompleteDTO dto) {
        return flowTaskService.complete(dto);
    }


    @ApiOperation("用户已办")
    @PostMapping("done-list")
    Page<DoneVO> doneList(@RequestBody DoneDTO dto) {
        return flowTaskService.doneList(dto);
    }

    @ApiOperation("用户待办")
    @PostMapping("todo-list")
    Page<ReturnTaskVo> todo(@RequestBody TodoDTO dto) {
        return flowTaskService.todo(dto);
    }


    /**
     * 拾取任务并完成
     *
     * @param dto
     * @return
     */
    @ApiOperation("拾取任务并完成(通过)")
    @PostMapping("accept")
    String accept(@RequestBody AcceptDTO dto) {
        return flowTaskService.accept(dto);
    }


    /**
     * 拒绝
     *
     * @param dto
     * @return
     */
    @ApiOperation("拒绝")
    @PostMapping("reject")
    String reject(@RequestBody RejectDTO dto) {
        return flowTaskService.reject(dto);
    }


    /**
     * 多实例加签
     *
     * @param addMultiInstance
     * @return
     */
    @ApiOperation("多实例加签")
    @PostMapping("add-multi-instance")
    String addMultiInstanceExecution(@RequestBody AddMultiInstanceDTO addMultiInstance) {
        return flowTaskService.addMultiInstanceExecution(addMultiInstance);
    }

    /**
     * 多实例减签
     *
     * @param executionId
     * @return String
     */
    @ApiOperation("多实例减签")
    @GetMapping("delete-multi-instance/{executionId}")
    @ApiImplicitParam(value = "运行时实例id", name = "executionId")
    String deleteMultiInstanceExecution(@PathVariable String executionId) {
        return flowTaskService.deleteMultiInstanceExecution(executionId);
    }

    /**
     * 查询会签节点的审批人任务
     */
    @ApiOperation("查询会签节点的审批人任务")
    @PostMapping("sign-task-users")
    List<ReturnTaskVo> signTaskUsers(@RequestBody SignTaskUserDTO dto) {
        return flowTaskService.signTaskUsers(dto);
    }

    @ApiOperation("退回")
    // 例如：在节点4直接退到节点1
    @PostMapping("flow-return")
    String flowReturn(@RequestBody FlowReturnDTO dto) {
        return flowTaskService.flowReturn(dto);
    }

    // 任务
    @GetMapping("get-flow-chart")
    @ApiOperation("流程图")
    public void getFlowChart(HttpServletResponse response, String processId) throws Exception {
        flowTaskService.getFlowChart(response, processId);
    }

    /**
     * 获取指定用户组流程任务列表
     *
     * @param group
     * @return List<Task>
     */
    @ApiOperation("获取指定用户组流程任务列表")
    @GetMapping("user-group-tasks/{group}")
    List<ReturnTaskVo> tasks(@PathVariable("group") String group) {
        return TaskConvert.INSTANCE.toConvertTaskVoList(flowTaskService.userGroupTasks(group));
    }

    /**
     * 驳回流程
     *
     * @param targetTaskKey
     * @param taskId
     * @return Task
     */
    @ApiOperation("驳回流程-暂不使用")
    @GetMapping("disallow-task/{taskId}/{targetTaskKey}")
    String currentTask(@PathVariable String taskId, @PathVariable String targetTaskKey) {
        return flowTaskService.disallowTask(taskId, targetTaskKey);
    }

    @ApiOperation("获取当前任务的流程变量")
    @GetMapping("get-task-var")
    Map<String, Object> getTaskVar(@RequestParam  String taskId) {
        return flowTaskService.getTaskVar(taskId);
    }

}
