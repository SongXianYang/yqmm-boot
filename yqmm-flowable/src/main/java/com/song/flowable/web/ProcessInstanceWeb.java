/*******************************************************************************
 * Package: com.song.flowable.web
 * Type:    ProcessInstanceWeb
 * Date:    2023-11-28 22:13
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.web;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.song.flowable.dto.BatchDeleteProcessInstanceDTO;
import com.song.flowable.dto.DoneDTO;
import com.song.flowable.service.ProcessInstanceService;
import com.song.flowable.vo.ProcessInstancesVO;
import com.song.flowable.vo.QueryHistoryActivityVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.flowable.engine.history.HistoricProcessInstance;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 功能描述：流程实例
 *
 * @author Songxianyang
 * @date 2023-11-28 22:13
 */
@Api(tags = "流程实例")
@RestController
@RequestMapping("/flow/instance")
@RequiredArgsConstructor
public class ProcessInstanceWeb {

    private final ProcessInstanceService processInstanceService;

    @GetMapping("/get")
    @ApiOperation("获得指定流程实例")
    // id:流程实例的编号
    public HistoricProcessInstance getProcessInstance(@RequestParam("id") String id) {
        return processInstanceService.getProcessInstance(id);
    }

    /**
     * 终止流程
     *
     * @param processInstanceId
     * @param reason            原因
     * @return string
     */
    @ApiOperation("终止流程(撤回)")
    @GetMapping("delete-process-instance-by-id/{processInstanceId}/{reason}")
    Boolean deleteProcessInstanceById(@PathVariable String processInstanceId, @PathVariable String reason) {
        return processInstanceService.deleteProcessInstanceById(processInstanceId, reason);
    }

    /**
     * 批量删除流程实例
     *
     * @param dto
     * @return string
     */
    @ApiOperation("批量删除流程实例")
    @PostMapping("batch-delete-process-instance-by-id")
    Boolean batchDeleteProcessInstanceById(@RequestBody BatchDeleteProcessInstanceDTO dto) {
        return processInstanceService.batchDeleteProcessInstanceById(dto);
    }

    /**
     * 查看历史流程记录
     *
     * @param processInstanceId
     * @return List
     */
    @ApiOperation("根据流程实例id查询当前历史流程记录")
    @GetMapping("historic-activity-instances/{processInstanceId}")
    List<QueryHistoryActivityVO> historicActivityInstances(@PathVariable String processInstanceId) {
        return processInstanceService.historicActivityInstances(processInstanceId);
    }

    /**
     * 挂起流程实例(停止、暂停）或（唤醒、激活）被挂起的流程实例
     *  说明：
     *  挂起是指将流程实例暂停 ：停止该实例的任务执行
     *  激活是指恢复被挂起的流程实例的执行 ：可以继续执行后续
     *
     * @param processInstanceId
     * @param state
     * @return
     */
    @ApiOperation("挂起流程实例(停止、暂停）或（唤醒、激活）被挂起的流程实例")
    @PostMapping("stop-or-activate")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "processInstanceId", value = "流程实例id"),
            @ApiImplicitParam(name = "state", value = "1:激活、2：挂起")
    })

    Boolean stopOrActivate(@RequestParam String processInstanceId ,@RequestParam String state) {
        return processInstanceService.stopOrActivate(processInstanceId,state);
    }


    /**
     * 判断传入流程实例在运行中是否存在
     *
     * @param processInstanceId
     * @return 布尔
     */
    @ApiOperation("判断传入流程实例在运行中是否存在")
    @GetMapping("is-exist-proc-int-running/{processInstanceId}")
    Boolean isExistProcIntRunning(@PathVariable String processInstanceId) {
        return processInstanceService.isExistProcIntRunning(processInstanceId);
    }

    /**
     * 我发起的流程实例列表
     *
     * @param
     * @return list
     */
    @ApiOperation("查看流程实例列表")
    @PostMapping("list-by-user-id")
    Page<ProcessInstancesVO> listByUserId(@RequestBody DoneDTO dto) {
        return processInstanceService.listByUserId(dto);
    }

}
