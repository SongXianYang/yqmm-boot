/*******************************************************************************
 * Package: com.song.flowable.web
 * Type:    FlowableWeb
 * Date:    2022-02-05 22:22
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.web;

import com.song.common.annotation.ResponseInfoSkin;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-02-05 22:22
 */
@Api(tags = "临时")
@Slf4j
@RestController
@ResponseInfoSkin
@RequestMapping("temp")
@RequiredArgsConstructor
public class TempWeb {

    private final ServletWebServerApplicationContext servletWebServerApplicationContext;
    @ApiOperation("测试")
    @GetMapping("/ok")
    String ok() {
        return "ok";
    }

    @ApiOperation("返回id")
    @GetMapping("get-uuid")
    String getUuid() {
        //throw new FlowableException("返回id");
        String replace = UUID.randomUUID().toString().replace("-", "");
        int port = servletWebServerApplicationContext.getWebServer().getPort();
        String string = new Integer(port).toString();
        log.info(">>>>>>>>>>replace{}<<<<<<<<<<<", replace);
        log.info(">>>>>>>>>>replace1{}<<<<<<<<<<<", replace);
        log.info(">>>>>>>>>>replace2{}<<<<<<<<<<<", replace);
        log.info(">>>>>>>>>>replace3{}<<<<<<<<<<<", replace);
        log.info(">>>>>>>>>>replace4{}<<<<<<<<<<<", replace);
        return string + "<----->" + replace;
    }

    @ApiOperation("返回端口")
    @GetMapping("get-port")
    String getPort() {
        int port = servletWebServerApplicationContext.getWebServer().getPort();
        String string = new Integer(port).toString();
        string = string + ">>>>>返回端口";
        return string;
    }
}
