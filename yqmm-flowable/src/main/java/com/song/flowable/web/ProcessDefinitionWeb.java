/*******************************************************************************
 * Package: com.song.flowable.web
 * Type:    ProcessDefinitionWeb
 * Date:    2023-11-27 22:13
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.web;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.song.flowable.dto.ProcessDefinitionPageDTO;
import com.song.flowable.dto.StartFlowableDTO;
import com.song.flowable.service.ProcessDefinitionService;
import com.song.flowable.vo.ProcessDefinitionVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.flowable.engine.runtime.Execution;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-27 22:13
 */
@Api(tags = "流程定义")
@RestController
@RequestMapping("/flow/definition")
@RequiredArgsConstructor
public class ProcessDefinitionWeb {
    private final ProcessDefinitionService processDefinitionService;

    @PostMapping("/page")
    @ApiOperation("列表分页")
    public Page<ProcessDefinitionVO> getProcessDefinitionPage(
           @RequestBody ProcessDefinitionPageDTO dto) {
        return processDefinitionService.getProcessDefinitionPage(dto);
    }

    @GetMapping ("/get-xml")
    @ApiOperation("获得流程定义的xml")
    // 流程定义id
    public String getXML(@RequestParam("id") String id) {
        String bpmnXML = processDefinitionService.getXML(id);
        return bpmnXML;
    }

    /**
     * 启动流程
     * @param dto  启动流程参数
     * @return
     */
    @ApiOperation("启动流程")
    @PostMapping("start-flowable")
    String startFlowable(@RequestBody StartFlowableDTO dto){
        return processDefinitionService.startFlowable(dto);
    }


    /**
     * 查询所有启动的流程列表
     * @param dto
     * @return
     */
    @ApiOperation("查询所有启动的流程列表")
    @PostMapping("executions")
    List<Execution> executions(@RequestBody StartFlowableDTO dto) {
        return processDefinitionService.executions(dto);
    }


    /**
     * 挂起流程定义(停止、暂停）或（唤醒、激活）被挂起的流程定义
     *  说明：
     *  部署的流程其默认状态为激活状态，若暂时不想使用该定义的流程，则可以进行挂起操作。
     *  流程定义为挂起状态时，该流程定义将不允许启动新的流程实例。
     *
     * @param definitionId
     * @param state
     * @return
     */
    @ApiOperation("挂起流程定义(停止、暂停）或（唤醒、激活）被挂起的流程定义")
    @PostMapping("stop-or-activate")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "definitionId", value = "流程定义id"),
            @ApiImplicitParam(name = "state", value = "1:激活、2：挂起")
    })

    Boolean stopOrActivate(@RequestParam String definitionId ,@RequestParam String state) {
        return processDefinitionService.stopOrActivate(definitionId,state);
    }


}
