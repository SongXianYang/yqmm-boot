package com.song.flowable.web;

import com.song.common.util.JsonUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import lombok.RequiredArgsConstructor;


import com.song.flowable.entity.StudentLeaveDO;
import com.song.flowable.service.StudentLeaveService;

import java.util.List;

/**
 * @author SongXianYang
 * 学生请假控制层
 */
@Slf4j
@Api(tags =  "学生请假")
@RestController
@RequestMapping("/student_leave")
@RequiredArgsConstructor
public class StudentLeaveController {



    private final StudentLeaveService studentLeaveService;

	@GetMapping("ok")
    @ApiOperation("测试")
    public String test() {
        return "ok";
    }

    @GetMapping("insert")
    public String insert() {
        StudentLeaveDO studentLeaveDO = new StudentLeaveDO();
        studentLeaveDO.setDays(8L);
        studentLeaveDO.setName("开开心");
        studentLeaveService.save(studentLeaveDO);
        return "ok";
    }
    @GetMapping("list")
    public String list() {
        List<StudentLeaveDO> list = studentLeaveService.list();
        log.info("data:----->{}", JsonUtils.toJson(list));

        return "ok";
    }


}