package com.song.flowable.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.song.flowable.entity.StudentLeaveDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author SongXianYang
 * 学生请假 mapper接口
 */
@Mapper
public interface StudentLeaveMapper extends BaseMapper<StudentLeaveDO> {

}