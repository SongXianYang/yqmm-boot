package com.song.flowable.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.song.flowable.entity.StudentLeaveDO;

/**
 * @author SongXianYang
 * 学生请假业务层
 */
public interface StudentLeaveService extends IService<StudentLeaveDO> {
}