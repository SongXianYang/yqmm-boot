package com.song.flowable.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.song.flowable.mapper.StudentLeaveMapper;
import com.song.flowable.service.StudentLeaveService;
import org.springframework.stereotype.Service;
import com.song.flowable.entity.StudentLeaveDO;
import lombok.RequiredArgsConstructor;

/**
 * @author SongXianYang
 * 学生请假业务实现层
 */
@Service
@RequiredArgsConstructor
public class StudentLeaveServiceImpl extends ServiceImpl<StudentLeaveMapper,
StudentLeaveDO> implements
        StudentLeaveService {
	// 例子
	// private final AdminUserApi adminUserApi; 
}