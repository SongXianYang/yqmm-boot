/*******************************************************************************
 * Package: com.song.flowable.service
 * Type:    FlowTaskService
 * Date:    2023-11-28 22:53
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.song.flowable.dto.*;
import com.song.flowable.vo.DoneVO;
import com.song.flowable.vo.ReturnTaskListVO;
import com.song.flowable.vo.ReturnTaskVo;
import org.flowable.task.api.Task;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 功能描述：流程任务
 *
 * @author Songxianyang
 * @date 2023-11-28 22:53
 */
public interface FlowTaskService {
    /**
     * 更新任务的负责人(转办或者转派负责人)
     * @param taskId
     * @param targetUserId
     * @return
     */
    Boolean updateTaskAssignee(String taskId, String targetUserId);

    /**
     * 查询可退回的节点列表
     * @param taskId
     * @return
     */
    List<ReturnTaskListVO> getReturnTaskList(String taskId);

    /**
     * 委派
     * @param taskId
     * @param assignUserId 要委派的用户
     * @return
     */
    Boolean assign(String taskId , String assignUserId);

    /**
     * 拾取任务
     * @param taskId
     * @param claimUserId
     * @return
     */
    Boolean claim(String taskId, String claimUserId);

    /**
     * 任务归还
     * @param taskId
     * @return
     */
    Boolean unClaim(String taskId);

    /**
     * 如果任务拾取后，不想操作也不想归还，可以交接给他人进行处理
     * @param taskId
     * @param handoverUserId
     * @return
     */
    Boolean handover(String taskId, String handoverUserId);

    /**
     * 单独完成任务
     * @param dto
     * @return
     */
    Boolean complete(CompleteDTO dto);
    /**
     * 已办数据
     * @param dto
     * @return
     */
    Page<DoneVO> doneList(DoneDTO dto);

    /**
     * 用户待办
     * @param dto
     * @return
     */
    Page<ReturnTaskVo> todo(TodoDTO dto);

    /**
     * 拾取任务并完成
     * @param dto
     * @return
     */
    String accept(AcceptDTO dto);
    /**
     * 拒绝
     * 参考文献：
     * FLOWABLE 节点 驳回/退回 后，节点变量的处理 https://www.freesion.com/article/41401448672/
     * 驳回：https://www.jianshu.com/p/99df24e4da44
     * FLOWABLE实战（五）FLOWABLE驳回/退回上一步/退回到 https://www.freesion.com/article/90381289458/#google_vignette
     * @param dto
     * @return
     */
    String reject(RejectDTO dto);

    /**
     * 多实例加签
     * @param addMultiInstance
     * @return
     */
    String addMultiInstanceExecution(AddMultiInstanceDTO addMultiInstance);

    /**
     * 多实例减签
     * @param executionId
     * @return
     */
    String deleteMultiInstanceExecution(String executionId);

    /**
     * 查询会签节点的审批人任务
     * @param dto
     * @return
     */
    List<ReturnTaskVo> signTaskUsers(SignTaskUserDTO dto);

    /**
     * 退回
     * @param dto
     * @return
     */
    String flowReturn(FlowReturnDTO dto);

    /**
     * 流程图
     * @param response
     * @param processId
     */
    void getFlowChart(HttpServletResponse response, String processId);

    /**
     * 获取指定用户组流程任务列表
     * @param group
     * @return
     */
    List<Task> userGroupTasks(String group);

    /**
     *驳回流程
     * @param taskId
     * @param targetTaskKey
     * @return
     */
    public String disallowTask(String taskId, String targetTaskKey);

    /**
     * 获取当前任务的流程变量
     * @param taskId
     * @return
     */
    Map<String, Object> getTaskVar(String taskId);


}
