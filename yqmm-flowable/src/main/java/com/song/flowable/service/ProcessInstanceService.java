/*******************************************************************************
 * Package: com.song.flowable.service
 * Type:    ProcessInstanceService
 * Date:    2023-11-28 22:19
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.song.flowable.dto.BatchDeleteProcessInstanceDTO;
import com.song.flowable.dto.DoneDTO;
import com.song.flowable.vo.ProcessInstancesVO;
import com.song.flowable.vo.QueryHistoryActivityVO;
import org.flowable.engine.history.HistoricProcessInstance;

import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-28 22:19
 */
public interface ProcessInstanceService {
    /**
     * 获得指定流程实例
     * @param id
     * @return ProcessDefinition
     */
    HistoricProcessInstance getProcessInstance(String id);

    /**
     * 终止流程(撤回)
     * @param processInstanceId
     * @param reason
     * @return
     */
    Boolean deleteProcessInstanceById(String processInstanceId, String reason);

    /**
     * 批量终止、撤回流程实例
     * @param dto
     * @return
     */
    Boolean batchDeleteProcessInstanceById(BatchDeleteProcessInstanceDTO dto);

    /**
     * 根据流程实例id查询当前历史流程记录
     * @param processInstanceId
     * @return
     */
    List<QueryHistoryActivityVO> historicActivityInstances(String processInstanceId);

    /**
     * 判断传入流程实例在运行中是否存在
     * @param processInstanceId
     * @return
     */
    Boolean isExistProcIntRunning(String processInstanceId);

    /**
     * 我发起的流程实例列表
     * @param
     * @return
     */
    Page<ProcessInstancesVO> listByUserId(DoneDTO dto);

    /**
     * 挂起流程实例(停止、暂停）或（唤醒、激活）被挂起的流程实例
     * @param processInstanceId
     * @param state   1:激活、2：挂起
     * @return
     */
    Boolean stopOrActivate(String processInstanceId,String state);
}
