package com.song.flowable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@ComponentScan(basePackages = {"com.song.common","com.song.flowable"})
public class YqmmFlowableApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(YqmmFlowableApplication.class, args);
    }
    
}
