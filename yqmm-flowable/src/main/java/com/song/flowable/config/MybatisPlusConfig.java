/*******************************************************************************
 * Package: com.song.flowable.config
 * Type:    MybatisPlusConfig
 * Date:    2022-09-18 13:04
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
// package com.song.flowable.config;
//
// import com.baomidou.mybatisplus.annotation.DbType;
// import com.baomidou.mybatisplus.core.MybatisConfiguration;
// import com.baomidou.mybatisplus.core.MybatisXMLLanguageDriver;
// import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
// import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
// import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
// import org.apache.ibatis.session.SqlSessionFactory;
// import org.apache.ibatis.type.JdbcType;
// import org.mybatis.spring.annotation.MapperScan;
// import org.springframework.boot.SpringBootConfiguration;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Primary;
//
// import javax.sql.DataSource;
//
// /**
//  * 功能描述：
//  * @author Songxianyang
//  * @date 2022-09-18 13:04
//  */
// @SpringBootConfiguration
// @MapperScan(basePackages ="com.song.flowable.mapper*", sqlSessionFactoryRef = "sqlSessionFactory", sqlSessionTemplateRef = "sqlSessionTemplate")
// public class MybatisPlusConfig {
//     @Bean
//     @Primary
//     public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
//         MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
//         sqlSessionFactory.setDataSource(dataSource);
//         sqlSessionFactory.setTypeAliasesPackage("com.song.flowable.entity");
//         MybatisConfiguration configuration = new MybatisConfiguration();
//         configuration.setDefaultScriptingLanguage(MybatisXMLLanguageDriver.class);
//         configuration.setJdbcTypeForNull(JdbcType.NULL);
//         configuration.setMapUnderscoreToCamelCase(true);
//         sqlSessionFactory.setConfiguration(configuration);
//         return sqlSessionFactory.getObject();
//     }
//
//     /**
//      * @Description 最新版
//      * @author: ybwei
//      * @Date: 2021/5/21 18:45
//      */
//     @Bean
//     public MybatisPlusInterceptor mybatisPlusInterceptor() {
//         MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
//         interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
//         return interceptor;
//     }
//
// }
