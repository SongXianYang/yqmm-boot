

# 需求
>公司两个人事，一个比较忙（大HR）另一位比较闲（工资少的HR啥啥不会干就会审批假条）。有一天狗蛋发起了请假审批，本来下一个节点审批人就是大HR 刚好大hr 那一天没看系统。

>但是审批时间就两个小时，如果超过两个小时 我们就直接跳到下个节点审批（比较闲HR）。并且大hr干大事情，也不会每天每小时盯着系统看。只能小hr来帮忙了。

>比较闲HR审批后再抄送一份邮件给大hr

# application.yml配置
```yaml
flowable:
  async-executor-activate: false
  database-schema-update: true
  mail:
    server:
      default-charset: UTF-8
#      默认的发件人
      default-from: s15738993185@163.com
      host: smtp.163.com
      username: s15738993185@163.com

      password: 163邮箱授权码
      s-s-l-port: 465
      use-ssl: true
      use-tls: true
```
# 流程图![img_2.png](img_2.png)
![img_1.png](img_1.png)
# 文件
> bpmn/邮件服务1.bpmn20.xml

json 文件审批
```json
启动流程
{
	"map": {
"startUserId":"admin"
},
	"processDefinitionKey": "key-mail"
}
用户代办
{
  "page": 0,
  "pageSize": 1000,
  "userId": "admin"
}
通过或者拒绝
{
  "taskId":"59e99247-8a6a-11ee-aece-dc41a90b0909",
  "userId":"admin"
}

```

# 报错 ：Could not connect to SMTP host: smtp.163.com, port: 25
```
解决方法：yml当中
s-s-l-port: 465
```
![img.png](img.png)
# 报错 Sending the email to the following server failed : smtp.163.com
```
解决方法：yml当中
password: 163邮箱授权码
```
![img_3.png](img_3.png)
