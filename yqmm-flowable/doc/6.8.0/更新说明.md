# 为什么会由原先的6.6.0升级到6.8.0 （重磅升级优化bug）
##  问题描述
### 流程图
![img.png](img.png)
> 发现问题1：流程发起后 弟弟节点一直等待到 时间持续时间过完 ，再次审批  ，直接进入最后一个节点
> 
> 经过时间推移~~~~~~
> 
> 发现问题2：act_ru_timer_job  我实时看了一下数据   这张表里面没有数据 不知道为啥   ？
> 
> 接着找问题~~~~~~~
> 
> 误判断：前提已经过了持续事件并且是从发起流程开始算起。就直接办理第一个任务的时候就开始算时间了（是错误的。但也是问题的分析一步一步来）
> 
> 接着~~~~
> 
> 发现问题3：不管启动几个流程（事件跨度挺大）act_ru_timer_job+DUEDATE_ +CREATE_TIME_这两个字段 的值都是相同的 .DUEDATE_  如果说 过了这个字段的时间   以后启动的流程都不会进入act_ru_timer_job 这张表
> ![img_1.png](img_1.png)
> 接着~~~ 
> 
> 深入Activiti流程引擎》作者贺波提点debug源码：org.flowable.engine.impl.agenda.ContinueProcessOperation的executeBoundaryEvents方法！我发现CREATE_TIME创建时间总是都是我发起流程后到达哥哥节点的时间。
> 
> 再后来~~~~~~
> 
> 我发现网络上没有人发现这个问题的！非常头大！难受！头疼！领导还催，必须要这个功能！该怎么办。。。。。。。
> 
> 突然想到ruoyi系统也是用的是flowable工作流引擎！我就去看了一下发现是6.8.0 想了想就升级了   发现问题因升而解！开心不行！
> 
# 如何升级
## 下载
官网下载地址：https://github.com/flowable/flowable-engine/releases/tag/flowable-6.8.0 比较慢不建议
## 解压flowable-6.8.0.zip
## 运行sql文件到自己的服务器
> 文件目录：\flowable-6.8.0\database\create\all\flowable.mysql.all.create.sql  （官方）
> 这是我的最新sql脚本：yqmm-flowable/sql/flowable_new.sql
## 找到war包
> 目录 flowable-6.8.0\wars\flowable-ui.war 。将flowable-ui.war拷贝到自己tomcat的webapps下。然后再去执行bin目录下的startup.bat文件
> 
## 发现启动失败（因为数据绑定的问题）
> 如何修复：打开这4个地方
> \webapps\flowable-ui\WEB-INF\classes\flowable-default.properties
> ![img_2.png](img_2.png)
## 重启tomcat

# 项目yqmm-flowable
> 升级maven依赖版本
```xml
    <dependency>
      <groupId>org.flowable</groupId>
      <artifactId>flowable-spring-boot-starter-process</artifactId>
      <version>${flowable.version}</version>
    </dependency>
    <dependency>
      <groupId>org.flowable</groupId>
      <artifactId>flowable-spring-boot-starter-actuator</artifactId>
      <version>${flowable.version}</version>
    </dependency>

<flowable.version>6.8.0</flowable.version>

```
##  重启项目：yqmm-flowable

#  此刻就升级成功！~~~~~~~~~~~ 持续优化
