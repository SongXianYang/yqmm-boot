## 一、执行监听器

### 1、可以监听的节点

+   开始、结束节点
+   连线节点
+   节点的开始和结束
+   网关的开始和结束
+   中间事件的开始和结束
+   开始时间结束或结束事件开始

### 2、添加事件监听器配置

+   Event事件
    +   start 开始
    +   take 启用
    +   end 结束
+   类型
    +   类：Class：com.sgp.StartListeners
        +   类名全限定
        +   监听器类上无论是否添加注入@C/S，也只能注入Flowable中的Bean
    +   表达式：Expression
    +   委托表达式：Delegate expression：${startListeners}
        +   监听器类的bean name
        +   监听器类上添加@C/S，既能注入Flowable中的Bean，也能注入Spring管理的Bean  
            参数

### 3、具体实现

```java
@Component
public class ProcessEndLister implements ExecutionListener {
    @Resource
    private RuntimeService runtimeService;

    @Override
    public void notify(DelegateExecution delegateExecution) {
        //delegateExecution.getProcessInstanceId();
        //delegateExecution.getProcessInstanceBusinessKey();
        //Object delegateExecution.getVariable(String variableName);
    }
}

```

## 二、任务监听器

### 1、可以监听的节点

+   用户任务UserTask节点

### 2、添加任务监听器配置

+   Event事件
    +   assignment：任务被委派给某人后触发（create之前触发）
    +   create：任务创建时，并且所有的任务属性设置完成后 触发
    +   complete：任务完成后，从运行时数据（runtime data）中删除前触发
    +   delete：在任务将要被删除之前发生（当任务通过completeTask完成任务时，它也会被执  
        行）
+   类型
    +   类：Class：com.sgp.StartListeners
        +   类名全限定
        +   监听器类上无论是否添加注入@C/S，监听器中只能注入Flowable中的Bean
    +   表达式：Expression
    +   委托表达式：Delegate expression： ${startListeners}
        +   监听器类的bean name
        +   监听器类上添加@C/S，既能注入Flowable中的Bean，也能注入Spring管理的Bean
+   参数

### 3、具体实现

```java
@Component
public class ProcessTaskNoticeLister implements TaskListener {

    //注入参数
    //添加属性FixedValue，param对应<flowable:field name="param"
    //获取参数值Sting param.getExpressionText();
    private FixedValue param;
    
    @Autowired
    private RuntimeService runtimeService;

    @Override
    public void notify(DelegateTask delegateTask) {
        //1、环境信息
        //流程id
        String delegateTask.getProcessInstanceId();
        
        //任务指定人信息
        String delegateTask.getAssignee();
        
        //该任务xml id 
        String  delegateTask.getTaskDefinitionKey();
        
        //该任务名称
        String  delegateTask.getName();
        
        //该任务的人员信息
        Set<IdentityLink>  delegateTask.getCandidates();

        //添加、删除候选人
        delegateTask.deleteCandidateUser(String userId);
        delegateTask.deleteUserIdentityLink(String userId, String identityLinkType);

        //不设置identityLinkType时，默认=candidate
        delegateTask.addCandidateUser(String userId);
        delegateTask.addCandidateUsers(Collection<String> candidateUsers);
        delegateTask.addUserIdentityLink(String userId, String identityLinkType);
        
        //添加、删除候选组
        delegateTask.deleteCandidateGroup(String groupId);
        delegateTask.deleteGroupIdentityLink(String groupId, String identityLinkType);

        //不设置identityLinkType时，默认=candidate
        delegateTask.addCandidateGroup(String groupId);
        delegateTask.addCandidateGroups(Collection<String> candidateGroups);
        delegateTask.addGroupIdentityLink(String groupId, String identityLinkType);
        
        //2、流程变量
        //单个变量获取
        Object varValue=delegateTask.getVariable(String key);

        //所有变量key、value
        Map<String, Object>variables= delegateTask.getVariables();
        
        //所有变量key
        Set<String> setNames=delegateTask.getVariableNames();
        
        //修改变量值
        delegateTask.setVariable("name","kimi");
    }
}
```