/*******************************************************************************
 * Package: com.song.web
 * Type:    StompController
 * Date:    2024-08-05 17:24
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.web;

import com.song.comm.ChatMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-08-05 17:24
 */
@Controller
@Slf4j
@RequiredArgsConstructor
public class StompController {

    private final SimpMessagingTemplate messagingTemplate;

    @GetMapping("stomp")
    public ModelAndView stomp() {
        return new ModelAndView("stomp");
    }

    @GetMapping("point")
    public ModelAndView point() {
        return new ModelAndView("stompCopy");
    }


    /**
     * 数据通过前端发送到后端，处理完数据后在响应给前端的订阅者“/topic/public”
     * @param chatMessage
     * @return
     */

    // 这个注解的作用是映射客户端发送到服务器的消息路径
    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(ChatMessage chatMessage) {
        // 处理消息并返回
        return chatMessage;
    }

    /**
     * 点对点消息通信
     * @param chatMessage
     * @return
     */
    // @MessageMapping("/chat.one.sendMessage")
    // @SendToUser("/queue/reply")
    // public ChatMessage processMessageFromClient(ChatMessage chatMessage, @Header("userId") String userId) {
    //     chatMessage.setTargetUser(userId);
    //     return chatMessage;
    // }


    /**
     * 点对点消息通信
     * @param chatMessage
     * @return
     */
    @MessageMapping("/chat.one.sendMessage")
    public void processMessageFromClient(ChatMessage chatMessage) {
        messagingTemplate.convertAndSendToUser(chatMessage.getTargetUser(), "/queue/reply", chatMessage);
    }

    /**
     *  用户加入的消息
     * @param chatMessage
     * @param headerAccessor
     * @return
     */
    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public ChatMessage addUser(ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        // 将用户名存储在WebSocket会话中
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        return chatMessage;
    }


    /**
     * 用户离开
     * @param chatMessage
     * @param headerAccessor
     * @return
     */
    @MessageMapping("/chat.leave")
    @SendTo("/topic/public")
    public ChatMessage leaveChat(ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        // 从会话属性中移除用户名
        String username = (String) headerAccessor.getSessionAttributes().remove("username");
        chatMessage.setSender(username);
        chatMessage.setContent(username + "~~已离开聊天");
        chatMessage.setType(ChatMessage.MessageType.LEAVE);
        return chatMessage;
    }

    /**
     * 服务端主动给客户端发送消息
     * @return
     */
    @GetMapping("s-to-c")
    @SendTo("/topic/public")
    @ResponseBody
    public ChatMessage sToC() {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setContent("西瓜视频");
        chatMessage.setSender("Server");
        chatMessage.setType(ChatMessage.MessageType.CHAT);
        // 发送消息
        messagingTemplate.convertAndSend("/topic/public", chatMessage);
        return chatMessage;
    }
}
