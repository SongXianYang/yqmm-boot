package com.song;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * https://blog.csdn.net/moshowgame/article/details/80275084
 * https://github.com/moshowgame/spring-cloud-study/tree/master/
 * WebSocket的那些事（3-STOMP实操篇）
 * https://blog.csdn.net/lingbomanbu_lyl/article/details/130542995
 * WebSocket+RabbitMQ实现消息推送系统
 * https://blog.csdn.net/CSDN2497242041/article/details/120359947
 */
@SpringBootApplication
public class YqmmWebsocketApplication {

    public static void main(String[] args) {
        SpringApplication.run(YqmmWebsocketApplication.class, args);
    }

}