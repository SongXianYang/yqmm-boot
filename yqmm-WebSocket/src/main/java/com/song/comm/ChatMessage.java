package com.song.comm;

import lombok.Data;

/**
 * 聊天消息对象
 */
@Data
public class ChatMessage {
    /**
     * 消息的类型
     */
    private MessageType type;
    /**
     * 消息的内容
     */
    private String content;
    /**
     * 发送者
     */
    private String sender;
    /**
     * 接收者
     */
    private String targetUser;

    public enum MessageType {
        //普通聊天消息
        CHAT,
        // 加入聊天
        JOIN,
        // 离开聊天
        LEAVE
    }
}
