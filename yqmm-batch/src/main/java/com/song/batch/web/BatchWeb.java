/*******************************************************************************
 * Package: com.song.es
 * Type:    ESTestWeb
 * Date:    2024-01-08 21:48
 *
 * Copyright (c) 2024 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.batch.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2024-01-08 21:48
 */
@Slf4j
@Api(tags = "批处理-简单操作")
@RestController
@RequestMapping("/batch")
public class BatchWeb {
    @Resource
    private JobLauncher jobLauncher;

    @Resource
    private Job importJob;

    @SneakyThrows
    @GetMapping("execute-jod")
    @ApiOperation("执行我们定义的jop")
    public void create() {
        // theNameOfTheSpecificBusiness 具体的业务名称
        // 表batch_job_execution_params
        JobParameters jobParameters = new JobParametersBuilder().addLong("theNameOfTheSpecificBusiness", System.currentTimeMillis()).toJobParameters();
        jobLauncher.run(importJob, jobParameters);

    }




}
