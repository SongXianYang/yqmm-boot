package com.song.batch;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.song.common","com.song.batch"})
@EnableBatchProcessing // 开启批处理的支持
public class YqmmBatchApplication {
    //https://www.cnblogs.com/jian0110/p/10838744.html
    //https://blog.csdn.net/fooelliot/article/details/86570216
    //https://www.bilibili.com/video/BV1yb411975P/?spm_id_from=333.337.search-card.all.click&vd_source=7edba4d7e4f510db6dfdcf1ff1081847
    public static void main(String[] args) {
        SpringApplication.run(YqmmBatchApplication.class, args);
    }
    
}
